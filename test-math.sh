#!/bin/sh

set -x

cargo test $@ --manifest-path utils-math/Cargo.toml

exit
