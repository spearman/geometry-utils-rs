//! Types and functions for coordinate spaces

use cgmath;
#[cfg(feature = "derive_serdes")]
use serde::{Deserialize, Serialize};

use crate::{Scalar, Unit3};

////////////////////////////////////////////////////////////////////////////////
//  consts                                                                    //
////////////////////////////////////////////////////////////////////////////////

pub const COMPONENT_INDEX_X : usize = 0;
pub const COMPONENT_INDEX_Y : usize = 1;
pub const COMPONENT_INDEX_Z : usize = 2;

////////////////////////////////////////////////////////////////////////////////
//  enums                                                                     //
////////////////////////////////////////////////////////////////////////////////

#[derive(Clone,Copy,Debug,Eq,PartialEq)]
pub enum Sign {
  Negative,
  Zero,
  Positive
}

#[derive(Clone,Copy,Debug,Eq,PartialEq)]
pub enum Octant {
  /// +X, +Y, +Z
  PosPosPos,
  /// -X, +Y, +Z
  NegPosPos,
  /// +X, -Y, +Z
  PosNegPos,
  /// -X, -Y, +Z
  NegNegPos,
  /// +X, +Y, -Z
  PosPosNeg,
  /// -X, +Y, -Z
  NegPosNeg,
  /// +X, -Y, -Z
  PosNegNeg,
  /// -X, -Y, -Z
  NegNegNeg
}

#[derive(Copy,Clone,Debug,Eq,PartialEq)]
pub enum Axis2 {
  X=0,
  Y
}

#[derive(Copy,Clone,Debug,Eq,PartialEq)]
pub enum Axis3 {
  X=0,
  Y,
  Z
}

#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Copy,Clone,Debug,Eq,PartialEq)]
pub enum SignedAxis1 {
  NegX, PosX
}

#[derive(Copy,Clone,Debug,Eq,PartialEq)]
pub enum SignedAxis2 {
  NegX, PosX,
  NegY, PosY
}

#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Copy,Clone,Debug,Eq,PartialEq)]
pub enum SignedAxis3 {
  NegX, PosX,
  NegY, PosY,
  NegZ, PosZ
}

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

impl Axis2 {
  #[inline]
  pub fn component (self) -> usize {
    self as usize
  }
}

impl Axis3 {
  #[inline]
  pub fn component (self) -> usize {
    self as usize
  }
}

impl SignedAxis3 {
  pub fn try_from <S : Scalar> (vector : &cgmath::Vector3 <S>) -> Option <Self> {
    return if *vector == [ S::one(),   S::zero(),  S::zero()].into() {
      Some (SignedAxis3::PosX)
    } else if *vector == [-S::one(),   S::zero(),  S::zero()].into() {
      Some (SignedAxis3::NegX)
    } else if *vector == [ S::zero(),  S::one(),   S::zero()].into() {
      Some (SignedAxis3::PosY)
    } else if *vector == [ S::zero(), -S::one(),   S::zero()].into() {
      Some (SignedAxis3::NegY)
    } else if *vector == [ S::zero(),  S::zero(),  S::one() ].into() {
      Some (SignedAxis3::PosZ)
    } else if *vector == [ S::zero(),  S::zero(), -S::one() ].into() {
      Some (SignedAxis3::NegZ)
    } else {
      None
    }
  }

  pub fn to_vec <S : Scalar> (self) -> cgmath::Vector3 <S> {
    match self {
      SignedAxis3::NegX => [-S::one(),   S::zero(),  S::zero()].into(),
      SignedAxis3::PosX => [ S::one(),   S::zero(),  S::zero()].into(),
      SignedAxis3::NegY => [ S::zero(), -S::one(),   S::zero()].into(),
      SignedAxis3::PosY => [ S::zero(),  S::one(),   S::zero()].into(),
      SignedAxis3::NegZ => [ S::zero(),  S::zero(), -S::one() ].into(),
      SignedAxis3::PosZ => [ S::zero(),  S::zero(),  S::one() ].into()
    }
  }
  #[inline]
  pub fn to_unit <S : Scalar> (self) -> Unit3 <S> {
    Unit3::unchecked (self.to_vec())
  }
}
