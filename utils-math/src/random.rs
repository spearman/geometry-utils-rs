use rand;
use cgmath::num_traits as num;

/// Adds a random offset to the given value.
///
/// The min/max bounds will be enforced and the magnitude of the offset is
/// determined by the given absolute value.
pub fn brown_update <N, R> (
  rng       : &mut R,
  n         : N,
  min       : N,
  max       : N,
  brown_abs : N
) -> N where
  N : Copy + PartialEq + PartialOrd + std::fmt::Debug
    + std::ops::Neg <Output=N> + std::ops::Add <Output=N>
    + std::ops::Sub <Output=N> + std::cmp::Ord + num::Zero + num::One
    + rand::distributions::uniform::SampleUniform,
  R : rand::Rng
{
  debug_assert!(n >= min);
  debug_assert!(n <= max);
  let brown = rng.gen_range (-brown_abs..brown_abs+N::one());
  let x = n + brown;
  let out = if brown > N::zero() {
    let over = max - n - brown;
    if over < N::zero() {
      (max + over).max (min)
    } else {
      x
    }
  } else if brown < N::zero() {
    let under = min - n - brown;
    if under > N::zero() {
      (min + under).min (max)
    } else {
      x
    }
  } else {
    x
  };
  debug_assert!(out >= min);
  debug_assert!(out <= max);
  out
}
