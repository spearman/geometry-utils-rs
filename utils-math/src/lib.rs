//! Math utilities

#![warn(unused_extern_crates)]

use cgmath::{assert_ulps_eq, relative_eq, ulps_eq};
#[cfg(feature = "derive_serdes")]
use serde::{Deserialize, Serialize};

pub mod consts;
pub mod coordinate;
pub mod random;

pub use coordinate::*;

////////////////////////////////////////////////////////////////////////////////
//  traits                                                                    //
////////////////////////////////////////////////////////////////////////////////

/// Marker trait for integer types
pub trait Integer :
  cgmath::BaseNum +
  cgmath::num_traits::Signed +
  cgmath::num_traits::PrimInt
{ }
impl <INT> Integer for INT where
  INT : cgmath::BaseNum +
    cgmath::num_traits::Signed + cgmath::num_traits::PrimInt
{ }

/// Marker trait for scalar values in some base field
pub trait Scalar :
  cgmath::BaseFloat +
  cgmath::BaseNum +
  cgmath::num_traits::FloatConst +
  cgmath::num_traits::Signed
{
  // some generic constants
  fn two() -> Self {
    Self::one() + Self::one()
  }
  fn three() -> Self {
    Self::one() + Self::one() + Self::one()
  }
  fn four() -> Self {
    Self::two() * Self::two()
  }
  fn five() -> Self {
    Self::two() + Self::three()
  }
  fn six() -> Self {
    Self::two() * Self::three()
  }
  fn seven() -> Self {
    Self::three() + Self::four()
  }
  fn eight() -> Self {
    Self::two() * Self::two() * Self::two()
  }
  fn nine() -> Self {
    Self::three() * Self::three()
  }
  fn ten() -> Self {
    Self::two() * Self::five()
  }
}

impl <FLD> Scalar for FLD where
  FLD : cgmath::BaseFloat + cgmath::num_traits::FloatConst +
    cgmath::num_traits::Signed
{ }

////////////////////////////////////////////////////////////////////////////////
//  structs                                                                   //
////////////////////////////////////////////////////////////////////////////////

/// Non-negative scalars (may be zero)
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Copy,Debug,PartialEq,PartialOrd)]
pub struct NonNegative <S : Scalar> {
  value : S
}

/// Scalars in the closed unit interval [0,1]
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Copy,Debug,PartialEq,PartialOrd)]
pub struct Normalized <S : Scalar> {
  value : S
}

/// Scalars in the closed unit interval [-1,1]
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Copy,Debug,PartialEq,PartialOrd)]
pub struct NormalSigned <S : Scalar> {
  value : S
}

/// 2D non-zero vectors
#[derive(Clone,Copy,Debug,Eq,PartialEq)]
pub struct NonZero2 <S : Scalar> {
  vector : cgmath::Vector2 <S>
}

/// 3D non-zero vectors
#[derive(Clone,Copy,Debug,Eq,PartialEq)]
pub struct NonZero3 <S : Scalar> {
  vector : cgmath::Vector3 <S>
}

/// Strictly positive scalars
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Copy,Debug,PartialEq,PartialOrd)]
pub struct Positive <S : Scalar> {
  value : S
}

/// 2D unit vector
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Copy,Debug,Eq,PartialEq)]
pub struct Unit2 <S : Scalar> {
  vector : cgmath::Vector2 <S>
}

/// 3D unit vector
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Copy,Debug,Eq,PartialEq)]
pub struct Unit3 <S : Scalar> {
  vector : cgmath::Vector3 <S>
}

/// An unordered pair of unique identifiers.
#[derive(Clone,Debug,Eq,PartialEq)]
pub struct UnorderedPair {
  a : usize,
  b : usize
}

/// A unit quaternion representing an orientation in $\mathbb{R}^3$
#[derive(Clone,Debug)]
pub struct Versor <S : Scalar> (cgmath::Quaternion <S>);

////////////////////////////////////////////////////////////////////////////////
//  functions                                                                 //
////////////////////////////////////////////////////////////////////////////////

/// Maps `0.0` to `0.0`, otherwise equal to `S::signum` (which would otherwise
/// map `+0.0 -> 1.0` and `-0.0 -> -1.0`)
#[inline]
pub fn signum_or_zero <S : Scalar> (scalar : S) -> S {
  if scalar.is_zero() {
    S::zero()
  } else {
    scalar.signum()
  }
}

/// Maps `signum_or_zero` over each element of the given vector
#[inline]
pub fn sigvec <S : Scalar> (vector : cgmath::Vector3 <S>)
  -> cgmath::Vector3 <S>
{
  vector.map (signum_or_zero)
}

/// Maps `signum_or_zero` over each element of the given vector and normalizes
/// the resulting vector, unless called with the zero vector in which case the
/// zero vector is returned.
///
/// # Examples
///
/// Returns the zero vector if called with the zero vector:
///
/// ```
/// # use math_utils::sigvec_unit_f32;
/// assert_eq!(sigvec_unit_f32 ([0.0, 0.0, 0.0].into()), [0.0, 0.0, 0.0].into());
/// ```
#[inline]
pub fn sigvec_unit_f32 (vector : cgmath::Vector3 <f32>)
  -> cgmath::Vector3 <f32>
{
  use consts::f32::*;
  let sigvec = sigvec (vector);
  let x = (sigvec.x + 1.0) as usize;
  let y = (sigvec.y + 1.0) as usize;
  let z = (sigvec.z + 1.0) as usize;
  UNIT_SIGVEC_ARRAY[x][y][z].into()
}

/// Maps `signum_or_zero` over each element of the given vector and normalizes
/// the resulting vector, unless called with the zero vector in which case the
/// zero vector is returned.
///
/// # Examples
///
/// Returns the zero vector if called with the zero vector:
///
/// ```
/// # use math_utils::sigvec_unit_f64;
/// assert_eq!(sigvec_unit_f64 ([0.0, 0.0, 0.0].into()), [0.0, 0.0, 0.0].into());
/// ```
#[inline]
pub fn sigvec_unit_f64 (vector : cgmath::Vector3 <f64>)
  -> cgmath::Vector3 <f64>
{
  use consts::f64::*;
  let sigvec = sigvec (vector);
  let x = (sigvec.x + 1.0) as usize;
  let y = (sigvec.y + 1.0) as usize;
  let z = (sigvec.z + 1.0) as usize;
  UNIT_SIGVEC_ARRAY[x][y][z].into()
}


////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

//
//  impl Sign
//
impl Sign {
  #[inline]
  pub fn from_scalar <S : Scalar> (scalar : S) -> Self {
    if scalar.is_zero() {
      Sign::Zero
    } else if scalar.is_positive() {
      Sign::Positive
    } else {
      debug_assert!(scalar.is_negative());
      Sign::Negative
    }
  }
} // end impl Sign

//
//  impl Octant
//
impl Octant {
  /// Returns some 'Some (octant)' if the vector is strictly contained with an
  /// octant and returns 'None' otherwise
  pub fn from_vec_strict <S : Scalar> (vector : &cgmath::Vector3 <S>)
    -> Option <Octant>
  {
    let sign_x = Sign::from_scalar (vector.x);
    let sign_y = Sign::from_scalar (vector.y);
    let sign_z = Sign::from_scalar (vector.z);
    let octant = match (sign_x, sign_y, sign_z) {
      (Sign::Zero,          _,          _) |
      (         _, Sign::Zero,          _) |
      (         _,          _, Sign::Zero) => return None,
      (Sign::Positive, Sign::Positive, Sign::Positive) => Octant::PosPosPos,
      (Sign::Negative, Sign::Positive, Sign::Positive) => Octant::NegPosPos,
      (Sign::Positive, Sign::Negative, Sign::Positive) => Octant::PosNegPos,
      (Sign::Negative, Sign::Negative, Sign::Positive) => Octant::NegNegPos,
      (Sign::Positive, Sign::Positive, Sign::Negative) => Octant::PosPosNeg,
      (Sign::Negative, Sign::Positive, Sign::Negative) => Octant::NegPosNeg,
      (Sign::Positive, Sign::Negative, Sign::Negative) => Octant::PosNegNeg,
      (Sign::Negative, Sign::Negative, Sign::Negative) => Octant::NegNegNeg
    };
    Some (octant)
  }
  /// Same as `from_vec_strict` for a point
  #[inline]
  pub fn from_point_strict <S : Scalar> (point : &cgmath::Point3 <S>)
    -> Option <Octant>
  {
    use cgmath::EuclideanSpace;
    Octant::from_vec_strict (&point.to_vec())
  }
}
// end impl Octant

//
//  impl NonNegative
//
impl <S : Scalar> NonNegative <S> {
  pub fn zero() -> Self {
    NonNegative { value: S::zero() }
  }
  pub fn one() -> Self {
    NonNegative { value: S::one() }
  }
  pub fn infinity() -> Self {
    NonNegative { value: S::infinity() }
  }
  /// Returns 'None' when called with a negative value.
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::NonNegative;
  /// assert!(NonNegative::new (1.0).is_some());
  /// assert!(NonNegative::new (0.0).is_some());
  /// assert!(NonNegative::new (-1.0).is_none());
  /// ```
  pub fn new (value : S) -> Option <Self> {
    if value >= S::zero() {
      Some (NonNegative { value })
    } else {
      None
    }
  }
  /// Converts negative input to absolute value.
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::NonNegative;
  /// assert_eq!(*NonNegative::abs (1.0), 1.0);
  /// assert_eq!(*NonNegative::abs (-1.0), 1.0);
  /// ```
  pub fn abs (value : S) -> Self {
    NonNegative { value: value.abs() }
  }
  /// Panics if negative.
  ///
  /// # Panics
  ///
  /// ```should_panic
  /// # use math_utils::NonNegative;
  /// let x = NonNegative::noisy (-1.0);  // panic!
  /// ```
  pub fn noisy (value : S) -> Self {
    assert!(S::zero() <= value);
    NonNegative { value }
  }
  /// Create a new non-negative number without checking the value.
  ///
  /// This method is completely unchecked for release builds.  Debug builds will
  /// panic if the value is negative:
  ///
  /// ```should_panic
  /// # use math_utils::NonNegative;
  /// let negative = NonNegative::unchecked (-1.0);   // panic!
  /// ```
  pub fn unchecked (value : S) -> Self {
    debug_assert!(value >= S::zero());
    NonNegative { value }
  }
  /// Map an operation on the underlying scalar, converting negative result to
  /// an absolute value.
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::NonNegative;
  /// assert_eq!(*NonNegative::abs (1.0).map_abs (|x| -2.0 * x), 2.0);
  /// ```
  pub fn map_abs (self, fun : fn (S) -> S) -> Self {
    Self::abs (fun (self.value))
  }
  /// Map an operation on the underlying scalar, panicking if the result is
  /// negative
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::NonNegative;
  /// assert_eq!(*NonNegative::abs (1.0).map_noisy (|x| 2.0 * x), 2.0);
  /// ```
  ///
  /// # Panics
  ///
  /// Panics of the result is negative:
  ///
  /// ```should_panic
  /// # use math_utils::NonNegative;
  /// let v = NonNegative::abs (1.0).map_noisy (|x| -1.0 * x);  // panic!
  /// ```
  pub fn map_noisy (self, fun : fn (S) -> S) -> Self {
    Self::noisy (fun (self.value))
  }
}
impl <S : Scalar> std::ops::Deref for NonNegative <S> {
  type Target = S;
  fn deref (&self) -> &S {
    &self.value
  }
}
impl <S : Scalar> std::ops::Mul for NonNegative <S> {
  type Output = Self;
  fn mul (self, rhs : Self) -> Self {
    NonNegative { value: self.value * rhs.value }
  }
}
impl <S : Scalar> std::ops::Mul <Positive <S>> for NonNegative <S> {
  type Output = Self;
  fn mul (self, rhs : Positive <S>) -> Self {
    NonNegative { value: self.value * rhs.value }
  }
}
impl <S : Scalar> std::ops::Div for NonNegative <S> {
  type Output = Self;
  fn div (self, rhs : Self) -> Self {
    NonNegative { value: self.value / rhs.value }
  }
}
impl <S : Scalar> std::ops::Add for NonNegative <S> {
  type Output = Self;
  fn add (self, rhs : Self) -> Self {
    NonNegative { value: self.value + rhs.value }
  }
}
impl <S : Scalar> From <Positive <S>> for NonNegative <S> {
  fn from (positive : Positive <S>) -> Self {
    NonNegative { value: *positive }
  }
}
//  end impl NonNegative

//
//  impl Positive
//
impl <S : Scalar> Positive <S> {
  pub fn one() -> Self {
    Positive { value: S::one() }
  }
  pub fn infinity() -> Self {
    Positive { value: S::infinity() }
  }
  /// Returns 'None' when called with a negative or zero value.
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::Positive;
  /// assert!(Positive::new (1.0).is_some());
  /// assert!(Positive::new (0.0).is_none());
  /// assert!(Positive::new (-1.0).is_none());
  /// ```
  pub fn new (value : S) -> Option <Self> {
    if value > S::zero() {
      Some (Positive { value })
    } else {
      None
    }
  }
  /// Panics if negative or zero.
  ///
  /// # Panics
  ///
  /// ```should_panic
  /// # use math_utils::Positive;
  /// let x = Positive::noisy (0.0);  // panic!
  /// ```
  pub fn noisy (value : S) -> Self {
    assert!(value > S::zero());
    Positive { value }
  }
  /// Create a new positive number without checking the value.
  ///
  /// In debug builds this will fail with a debug assertion if the value is
  /// negative:
  ///
  /// ```should_panic
  /// # use math_utils::Positive;
  /// let negative = Positive::unchecked (-1.0);  // panic!
  /// ```
  pub fn unchecked (value : S) -> Self {
    debug_assert!(value > S::zero());
    Positive { value }
  }
  /// Map an operation on the underlying scalar, panicking if the result is
  /// zero or negative.
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::Positive;
  /// assert_eq!(*Positive::<f64>::one().map_noisy (|x| 2.0 * x), 2.0);
  /// ```
  ///
  /// # Panics
  ///
  /// Panics of the result is negative or zero:
  ///
  /// ```should_panic
  /// # use math_utils::Positive;
  /// let v = Positive::<f64>::one().map_noisy (|_| 0.0);  // panic!
  /// ```
  pub fn map_noisy (self, fun : fn (S) -> S) -> Self {
    Self::noisy (fun (self.value))
  }
}
impl <S : Scalar> std::ops::Deref for Positive <S> {
  type Target = S;
  fn deref (&self) -> &S {
    &self.value
  }
}
impl <S : Scalar> std::ops::Mul for Positive <S> {
  type Output = Self;
  fn mul (self, rhs : Self) -> Self {
    Positive { value: self.value * rhs.value }
  }
}
impl <S : Scalar> std::ops::Mul <NonNegative <S>> for Positive <S> {
  type Output = NonNegative <S>;
  fn mul (self, rhs : NonNegative <S>) -> NonNegative <S> {
    NonNegative { value: self.value * *rhs }
  }
}
impl <S : Scalar> std::ops::Div for Positive <S> {
  type Output = Self;
  fn div (self, rhs : Self) -> Self {
    Positive { value: self.value / rhs.value }
  }
}
impl <S : Scalar> std::ops::Add for Positive <S> {
  type Output = Self;
  fn add (self, rhs : Self) -> Self {
    Positive { value: self.value + rhs.value }
  }
}
impl <S : Scalar> std::ops::Add <NonNegative <S>> for Positive <S> {
  type Output = Self;
  fn add (self, rhs : NonNegative <S>) -> Self {
    Positive { value: self.value + rhs.value }
  }
}
//  end impl Positive

//
//  impl Normalized
//
impl <S : Scalar> Normalized <S> {
  #[inline]
  pub fn zero() -> Self {
    Normalized { value: S::zero() }
  }
  #[inline]
  pub fn one() -> Self {
    Normalized { value: S::one() }
  }
  /// Returns 'None' when called with a value outside of the
  /// closed unit interval [0,1].
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::Normalized;
  /// assert!(Normalized::new (2.0).is_none());
  /// assert!(Normalized::new (1.0).is_some());
  /// assert!(Normalized::new (0.5).is_some());
  /// assert!(Normalized::new (0.0).is_some());
  /// assert!(Normalized::new (-1.0).is_none());
  /// ```
  #[inline]
  pub fn new (value : S) -> Option <Self> {
    if S::zero() <= value && value <= S::one() {
      Some (Normalized { value })
    } else {
      None
    }
  }
  /// Clamps to the closed unit interval [0,1]
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::Normalized;
  /// assert_eq!(*Normalized::clamp (2.0), 1.0);
  /// assert_eq!(*Normalized::clamp (-1.0), 0.0);
  /// ```
  #[inline]
  pub fn clamp (value : S) -> Self {
    let value = S::max (S::zero(), S::min (value, S::one()));
    Normalized { value }
  }
  /// Panics if outside the closed unit interval [0,1]
  ///
  /// # Panics
  ///
  /// ```should_panic
  /// # use math_utils::Normalized;
  /// let x = Normalized::noisy (-1.0);  // panic!
  /// ```
  ///
  /// ```should_panic
  /// # use math_utils::Normalized;
  /// let x = Normalized::noisy (2.0);  // panic!
  /// ```
  #[inline]
  pub fn noisy (value : S) -> Self {
    assert!(value <= S::one());
    assert!(S::zero() <= value);
    Normalized { value }
  }
  /// Map an operation on the underlying scalar, clamping results to the closed
  /// unit interval [0,1].
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::Normalized;
  /// assert_eq!(*Normalized::<f64>::one().map_clamp (|x|  2.0 * x), 1.0);
  /// assert_eq!(*Normalized::<f64>::one().map_clamp (|x| -2.0 * x), 0.0);
  /// ```
  #[inline]
  pub fn map_clamp (self, fun : fn (S) -> S) -> Self {
    Self::clamp (fun (self.value))
  }
  /// Map an operation on the underlying scalar, panicking if the result is
  /// outside the unit interval [0,1]
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::Normalized;
  /// assert_eq!(*Normalized::<f64>::one().map_noisy (|x| 0.5 * x), 0.5);
  /// ```
  ///
  /// # Panics
  ///
  /// Panics of the result is outside [0,1]:
  ///
  /// ```should_panic
  /// # use math_utils::Normalized;
  /// let v = Normalized::<f64>::one().map_noisy (|x| -1.0 * x);  // panic!
  /// ```
  ///
  /// ```should_panic
  /// # use math_utils::Normalized;
  /// let v = Normalized::<f64>::one().map_noisy (|x|  2.0 * x);  // panic!
  /// ```
  #[inline]
  pub fn map_noisy (self, fun : fn (S) -> S) -> Self {
    Self::noisy (fun (self.value))
  }
}
impl <S : Scalar> std::ops::Deref for Normalized <S> {
  type Target = S;
  fn deref (&self) -> &S {
    &self.value
  }
}
impl <S : Scalar> std::ops::Mul for Normalized <S> {
  type Output = Self;
  fn mul (self, rhs : Self) -> Self {
    Normalized { value: self.value * rhs.value }
  }
}
impl <S : Scalar> Eq  for Normalized <S> { }
impl <S : Scalar> Ord for Normalized <S> {
  fn cmp (&self, rhs : &Self) -> std::cmp::Ordering {
    // safe to unwrap: normalized values are never NaN
    self.partial_cmp (rhs).unwrap()
  }
}
//  end impl Normalized

//
//  impl NormalSigned
//
impl <S : Scalar> NormalSigned <S> {
  #[inline]
  pub fn zero() -> Self {
    NormalSigned { value: S::zero() }
  }
  #[inline]
  pub fn one() -> Self {
    NormalSigned { value: S::one() }
  }
  /// Returns 'None' when called with a value outside of the
  /// closed interval [-1,1].
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::NormalSigned;
  /// assert!(NormalSigned::new (2.0).is_none());
  /// assert!(NormalSigned::new (1.0).is_some());
  /// assert!(NormalSigned::new (0.5).is_some());
  /// assert!(NormalSigned::new (0.0).is_some());
  /// assert!(NormalSigned::new (-1.0).is_some());
  /// assert!(NormalSigned::new (-2.0).is_none());
  /// ```
  #[inline]
  pub fn new (value : S) -> Option <Self> {
    if -S::one() <= value && value <= S::one() {
      Some (NormalSigned { value })
    } else {
      None
    }
  }
  /// Clamps to the closed interval [-1,1]
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::NormalSigned;
  /// assert_eq!(*NormalSigned::clamp (2.0), 1.0);
  /// assert_eq!(*NormalSigned::clamp (-2.0), -1.0);
  /// ```
  #[inline]
  pub fn clamp (value : S) -> Self {
    let value = S::max (-S::one(), S::min (value, S::one()));
    NormalSigned { value }
  }
  /// Panics if outside the closed interval [-1,1]
  ///
  /// # Panics
  ///
  /// ```should_panic
  /// # use math_utils::NormalSigned;
  /// let x = NormalSigned::noisy (-2.0);   // panic!
  /// ```
  ///
  /// ```should_panic
  /// # use math_utils::NormalSigned;
  /// let x = NormalSigned::noisy (2.0);    // panic!
  /// ```
  #[inline]
  pub fn noisy (value : S) -> Self {
    assert!(value <= S::one());
    assert!(-S::one() <= value);
    NormalSigned { value }
  }
  /// Map an operation on the underlying scalar, clamping results to the closed
  /// interval [-1,1].
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::NormalSigned;
  /// assert_eq!(*NormalSigned::<f64>::one().map_clamp (|x|  2.0 * x),  1.0);
  /// assert_eq!(*NormalSigned::<f64>::one().map_clamp (|x| -2.0 * x), -1.0);
  /// ```
  #[inline]
  pub fn map_clamp (self, fun : fn (S) -> S) -> Self {
    Self::clamp (fun (self.value))
  }
  /// Map an operation on the underlying scalar, panicking if the result
  /// is outside the interval [-1, 1]
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::NormalSigned;
  /// assert_eq!(*NormalSigned::<f64>::one().map_noisy (|x| 0.5 * x), 0.5);
  /// ```
  ///
  /// # Panics
  ///
  /// Panics of the result is outside [-1, 1]:
  ///
  /// ```should_panic
  /// # use math_utils::NormalSigned;
  /// let v = NormalSigned::<f64>::one().map_noisy (|x| -2.0 * x);  // panic!
  /// ```
  ///
  /// ```should_panic
  /// # use math_utils::NormalSigned;
  /// let v = NormalSigned::<f64>::one().map_noisy (|x|  2.0 * x);  // panic!
  /// ```
  #[inline]
  pub fn map_noisy (self, fun : fn (S) -> S) -> Self {
    Self::noisy (fun (self.value))
  }
}
impl <S : Scalar> std::ops::Deref for NormalSigned <S> {
  type Target = S;
  fn deref (&self) -> &S {
    &self.value
  }
}
impl <S : Scalar> std::ops::Mul for NormalSigned <S> {
  type Output = Self;
  fn mul (self, rhs : Self) -> Self {
    NormalSigned { value: self.value * rhs.value }
  }
}
impl <S : Scalar> Eq  for NormalSigned <S> { }
impl <S : Scalar> Ord for NormalSigned <S> {
  fn cmp (&self, rhs : &Self) -> std::cmp::Ordering {
    // safe to unwrap: normalized values are never NaN
    self.partial_cmp (rhs).unwrap()
  }
}
impl <S : Scalar> std::ops::Neg for NormalSigned <S> {
  type Output = Self;
  fn neg (self) -> Self {
    NormalSigned { value: -self.value }
  }
}
impl <S : Scalar> From <Normalized <S>> for NormalSigned <S> {
  fn from (Normalized { value } : Normalized <S>) -> Self {
    debug_assert!(S::zero() <= value);
    debug_assert!(value <= S::one());
    NormalSigned { value }
  }
}
//  end impl NormalSigned

//
//  impl NonZero2
//
impl <S : Scalar> NonZero2 <S> {
  /// Returns 'None' if called with the zero vector
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::NonZero2;
  /// assert!(NonZero2::new ([1.0, 0.0].into()).is_some());
  /// assert!(NonZero2::new ([0.0, 0.0].into()).is_none());
  /// ```
  pub fn new (vector : cgmath::Vector2 <S>) -> Option <Self> {
    use cgmath::Zero;
    if !vector.is_zero() {
      Some (NonZero2 { vector })
    } else {
      None
    }
  }
  /// Panics if zero vector is given.
  ///
  /// # Panics
  ///
  /// ```should_panic
  /// # use math_utils::NonZero2;
  /// let x = NonZero2::noisy ([0.0, 0.0].into());  // panic!
  /// ```
  pub fn noisy (vector : cgmath::Vector2 <S>) -> Self {
    use cgmath::Zero;
    assert!(!vector.is_zero());
    NonZero2 { vector }
  }
  /// Map an operation on the underlying vector, panicking if the result is
  /// zero
  ///
  /// # Panics
  ///
  /// Panics of the result is zero:
  ///
  /// ```should_panic
  /// # use math_utils::NonZero2;
  /// let v = NonZero2::noisy ([1.0, 1.0].into())
  ///   .map_noisy (|x| 0.0 * x);  // panic!
  /// ```
  pub fn map_noisy (self, fun : fn (cgmath::Vector2 <S>) -> cgmath::Vector2 <S>) -> Self {
    Self::noisy (fun (self.vector))
  }
}
impl <S : Scalar> std::ops::Deref for NonZero2 <S> {
  type Target = cgmath::Vector2 <S>;
  fn deref (&self) -> &cgmath::Vector2 <S> {
    &self.vector
  }
}
//  end impl NonZero2

//
//  impl NonZero3
//
impl <S : Scalar> NonZero3 <S> {
  /// Returns 'None' if called with the zero vector
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::NonZero3;
  /// assert!(NonZero3::new ([1.0, 0.0, 0.0].into()).is_some());
  /// assert!(NonZero3::new ([0.0, 0.0, 0.0].into()).is_none());
  /// ```
  pub fn new (vector : cgmath::Vector3 <S>) -> Option <Self> {
    use cgmath::Zero;
    if !vector.is_zero() {
      Some (NonZero3 { vector })
    } else {
      None
    }
  }
  /// Panics if zero vector is given.
  ///
  /// # Panics
  ///
  /// ```should_panic
  /// # use math_utils::NonZero3;
  /// let x = NonZero3::noisy ([0.0, 0.0, 0.0].into());  // panic!
  /// ```
  pub fn noisy (vector : cgmath::Vector3 <S>) -> Self {
    use cgmath::Zero;
    assert!(!vector.is_zero());
    NonZero3 { vector }
  }
  /// Map an operation on the underlying vector, panicking if the result is
  /// zero
  ///
  /// # Panics
  ///
  /// Panics of the result is zero:
  ///
  /// ```should_panic
  /// # use math_utils::NonZero3;
  /// let v = NonZero3::noisy ([1.0, 1.0, 1.0].into())
  ///   .map_noisy (|x| 0.0 * x);  // panic!
  /// ```
  pub fn map_noisy (self, fun : fn (cgmath::Vector3 <S>) -> cgmath::Vector3 <S>) -> Self {
    Self::noisy (fun (self.vector))
  }
}
impl <S : Scalar> std::ops::Deref for NonZero3 <S> {
  type Target = cgmath::Vector3 <S>;
  fn deref (&self) -> &cgmath::Vector3 <S> {
    &self.vector
  }
}
// end impl NonZero3

//
//  impl Unit2
//
impl <S : Scalar> Unit2 <S> {
  /// ```
  /// # use math_utils::Unit2;
  /// assert_eq!(Unit2::axis_x(), Unit2::new ([1.0, 0.0].into()).unwrap());
  /// ```
  #[inline]
  pub fn axis_x() -> Self {
    Unit2 { vector: cgmath::Vector2::unit_x() }
  }
  /// ```
  /// # use math_utils::Unit2;
  /// assert_eq!(Unit2::axis_y(), Unit2::new ([0.0, 1.0].into()).unwrap());
  /// ```
  #[inline]
  pub fn axis_y() -> Self {
    Unit2 { vector: cgmath::Vector2::unit_y() }
  }
  /// Returns 'None' if called with a non-normalized vector.
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::Unit2;
  /// assert!(Unit2::new ([1.0, 0.0].into()).is_some());
  /// assert!(Unit2::new ([2.0, 0.0].into()).is_none());
  /// ```
  pub fn new (vector : cgmath::Vector2 <S>) -> Option <Self> {
    use cgmath::InnerSpace;
    if relative_eq!(vector.magnitude2(), S::one()) {
      Some (Unit2 { vector })
    } else {
      None
    }
  }
  /// Normalizes a given non-zero vector.
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::Unit2;
  /// assert_eq!(
  ///   *Unit2::normalize ([2.0, 0.0].into()),
  ///   [1.0, 0.0].into()
  /// );
  /// ```
  ///
  /// # Panics
  ///
  /// Panics if the zero vector is given:
  ///
  /// ```should_panic
  /// # use math_utils::Unit2;
  /// let x = Unit2::normalize ([0.0, 0.0].into());  // panic!
  /// ```
  pub fn normalize (vector : cgmath::Vector2 <S>) -> Self {
    use cgmath::{InnerSpace, Zero};
    assert!(!vector.is_zero());
    let vector = if !relative_eq!(vector.magnitude2(), S::one()) {
      vector.normalize()
    } else {
      vector
    };
    Unit2 { vector }
  }
  /// Panics if a non-normalized vector is given
  ///
  /// # Panics
  ///
  /// Panics if a non-normalized vector is given:
  ///
  /// ```should_panic
  /// # use math_utils::Unit2;
  /// let x = Unit2::noisy ([2.0, 0.0].into());  // panic!
  /// ```
  pub fn noisy (vector : cgmath::Vector2 <S>) -> Self {
    use cgmath::InnerSpace;
    assert!(relative_eq!(vector.magnitude2(), S::one()));
    Unit2 { vector }
  }
  /// It is a debug assertion if the given vector is not normalized:
  ///
  /// ```should_panic
  /// # use math_utils::Unit2;
  /// let n = Unit2::unchecked ([2.0, 0.0].into());  // panic!
  /// ```
  #[inline]
  pub fn unchecked (vector : cgmath::Vector2 <S>) -> Self {
    use cgmath::InnerSpace;
    // TODO: is this a reasonable check? max_ulps=4 is too low
    debug_assert!(ulps_eq!(vector.magnitude2(), S::one(), max_ulps = 6));
    Unit2 { vector }
  }
  /// Return the unit vector pointing in the opposite direction
  pub fn invert (mut self) -> Self {
    self.vector = -self.vector;
    self
  }
  /// Map an operation on the underlying vector, panicking if the result is
  /// zero
  ///
  /// # Panics
  ///
  /// Panics of the result is not normalized:
  ///
  /// ```should_panic
  /// # use math_utils::Unit2;
  /// // panic!
  /// let v = Unit2::noisy ([1.0, 0.0].into()).map_noisy (|x| 2.0 * x);
  /// ```
  pub fn map_noisy (self, fun : fn (cgmath::Vector2 <S>) -> cgmath::Vector2 <S>)
    -> Self
  {
    Self::noisy (fun (self.vector))
  }
}
impl <S : Scalar> std::ops::Deref for Unit2 <S> {
  type Target = cgmath::Vector2 <S>;
  fn deref (&self) -> &cgmath::Vector2 <S> {
    &self.vector
  }
}
//  end impl Unit2

//
//  impl Unit3
//
impl <S : Scalar> Unit3 <S> {
  /// ```
  /// # use math_utils::Unit3;
  /// assert_eq!(Unit3::axis_x(), Unit3::new ([1.0, 0.0, 0.0].into()).unwrap());
  /// ```
  #[inline]
  pub fn axis_x() -> Self {
    Unit3 { vector: cgmath::Vector3::unit_x() }
  }
  /// ```
  /// # use math_utils::Unit3;
  /// assert_eq!(Unit3::axis_y(), Unit3::new ([0.0, 1.0, 0.0].into()).unwrap());
  /// ```
  #[inline]
  pub fn axis_y() -> Self {
    Unit3 { vector: cgmath::Vector3::unit_y() }
  }
  /// ```
  /// # use math_utils::Unit3;
  /// assert_eq!(Unit3::axis_z(), Unit3::new ([0.0, 0.0, 1.0].into()).unwrap());
  /// ```
  #[inline]
  pub fn axis_z() -> Self {
    Unit3 { vector: cgmath::Vector3::unit_z() }
  }
  /// Returns 'None' if called with a non-normalized vector.
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::Unit3;
  /// assert!(Unit3::new ([1.0, 0.0, 0.0].into()).is_some());
  /// assert!(Unit3::new ([2.0, 0.0, 0.0].into()).is_none());
  /// ```
  pub fn new (vector : cgmath::Vector3 <S>) -> Option <Self> {
    use cgmath::InnerSpace;
    if relative_eq!(vector.magnitude2(), S::one()) {
      Some (Unit3 { vector })
    } else {
      None
    }
  }
  /// Normalizes a given non-zero vector.
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::Unit3;
  /// assert_eq!(
  ///   *Unit3::normalize ([2.0, 0.0, 0.0].into()),
  ///   [1.0, 0.0, 0.0].into()
  /// );
  /// ```
  ///
  /// # Panics
  ///
  /// Panics if the zero vector is given:
  ///
  /// ```should_panic
  /// # use math_utils::Unit3;
  /// let x = Unit3::normalize ([0.0, 0.0, 0.0].into());  // panic!
  /// ```
  pub fn normalize (vector : cgmath::Vector3 <S>) -> Self {
    use cgmath::{InnerSpace, Zero};
    assert!(!vector.is_zero());
    let vector = if !relative_eq!(vector.magnitude2(), S::one()) {
      vector.normalize()
    } else {
      vector
    };
    Unit3 { vector }
  }
  /// Panics if a non-normalized vector is given
  ///
  /// # Panics
  ///
  /// Panics if a non-normalized vector is given:
  ///
  /// ```should_panic
  /// # use math_utils::Unit3;
  /// let x = Unit3::noisy ([2.0, 0.0, 0.0].into());  // panic!
  /// ```
  pub fn noisy (vector : cgmath::Vector3 <S>) -> Self {
    use cgmath::InnerSpace;
    assert!(relative_eq!(vector.magnitude2(), S::one()));
    Unit3 { vector }
  }
  /// It is a debug assertion if the given vector is not normalized:
  ///
  /// ```should_panic
  /// # use math_utils::Unit3;
  /// let n = Unit3::unchecked ([2.0, 0.0, 0.0].into());  // panic!
  /// ```
  #[inline]
  pub fn unchecked (vector : cgmath::Vector3 <S>) -> Self {
    use cgmath::InnerSpace;
    // TODO: is this a reasonable check? max_ulps=4 is too low
    debug_assert!(ulps_eq!(vector.magnitude2(), S::one(), max_ulps = 6));
    Unit3 { vector }
  }
  /// Return the unit vector pointing in the opposite direction
  pub fn invert (mut self) -> Self {
    self.vector = -self.vector;
    self
  }
  /// Compute cross product (preserves length).
  ///
  /// # Example
  ///
  /// ```
  /// # #[macro_use] extern crate cgmath;
  /// # use cgmath::InnerSpace;
  /// # extern crate math_utils;
  /// # use math_utils::Unit3;
  /// # fn main() {
  /// let x = Unit3::new ([1.0, 0.0, 0.0].into()).unwrap();
  /// let y = Unit3::new ([0.0, 1.0, 0.0].into()).unwrap();
  /// assert_relative_eq!(x.cross (y).magnitude2(), 1.0);
  /// # }
  /// ```
  pub fn cross (self, other : Unit3 <S>) -> Self {
    use cgmath::InnerSpace;
    let vector = self.vector.cross (other.vector);
    debug_assert!(relative_eq!(vector.magnitude2(), S::one()));
    Unit3 { vector }
  }
  /// Map an operation on the underlying vector, panicking if the result is
  /// zero
  ///
  /// # Panics
  ///
  /// Panics of the result is not normalized:
  ///
  /// ```should_panic
  /// # use math_utils::Unit3;
  /// let v = Unit3::noisy ([1.0, 0.0, 0.0].into())
  ///   .map_noisy (|x| 2.0 * x);  // panic!
  /// ```
  pub fn map_noisy (self, fun : fn (cgmath::Vector3 <S>) -> cgmath::Vector3 <S>)
    -> Self
  {
    Self::noisy (fun (self.vector))
  }
}
impl <S : Scalar> std::ops::Deref for Unit3 <S> {
  type Target = cgmath::Vector3 <S>;
  fn deref (&self) -> &cgmath::Vector3 <S> {
    &self.vector
  }
}
impl Unit3 <f32> {
  /// Returns a unit sigvec based on the given input vector.
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::Unit3;
  /// # use std::f32::consts::*;
  /// assert_eq!(
  ///   *Unit3::<f32>::sigvec ([0.1, -3.0, 0.0].into()),
  ///   [FRAC_1_SQRT_2, -FRAC_1_SQRT_2, 0.0].into()
  /// );
  /// ```
  ///
  /// # Panics
  ///
  /// Panics if the zero vector is given:
  ///
  /// ```should_panic
  /// # use math_utils::Unit3;
  /// let x = Unit3::<f32>::sigvec ([0.0, 0.0, 0.0].into());  // panic!
  /// ```
  #[inline]
  pub fn sigvec (vector : cgmath::Vector3 <f32>) -> Self {
    use cgmath::Zero;
    assert!(!vector.is_zero());
    Unit3::unchecked (sigvec_unit_f32 (vector))
  }
}
impl Unit3 <f64> {
  /// Returns a unit sigvec based on the given input vector.
  ///
  /// # Example
  ///
  /// ```
  /// # use math_utils::Unit3;
  /// # use std::f64::consts::*;
  /// assert_eq!(
  ///   *Unit3::<f64>::sigvec ([0.1, -3.0, 0.0].into()),
  ///   [FRAC_1_SQRT_2, -FRAC_1_SQRT_2, 0.0].into()
  /// );
  /// ```
  ///
  /// # Panics
  ///
  /// Panics if the zero vector is given:
  ///
  /// ```should_panic
  /// # use math_utils::Unit3;
  /// let x = Unit3::<f64>::sigvec ([0.0, 0.0, 0.0].into());  // panic!
  /// ```
  #[inline]
  pub fn sigvec (vector : cgmath::Vector3 <f64>) -> Self {
    use cgmath::Zero;
    assert!(!vector.is_zero());
    Unit3::unchecked (sigvec_unit_f64 (vector))
  }
}
//  end impl Unit3

//
//  impl UnorderedPair
//
impl UnorderedPair {
  /// Create a new un-ordered pair.
  ///
  /// It is a debug panic if `a == b`.
  ///
  /// Note the pair will be stored in order such that `a < b`. Since this
  /// invariant is maintained, a simple element-wise equality test is sufficient
  /// to tell if two pairs are equivalent.
  pub fn new (a : usize, b : usize) -> Self {
    debug_assert!(a != b);
    if a < b {
      UnorderedPair { a, b }
    } else {
      UnorderedPair { b, a }
    }
  }
  pub fn fst (&self) -> usize {
    self.a
  }
  pub fn snd (&self) -> usize {
    self.b
  }
}
//  end impl UnorderedPair

//
//  impl Versor
//
impl <S : Scalar> Versor <S> {
  /// Normalizes the given quaternion.
  ///
  /// Panics if the zero quaternion is given.
  pub fn normalize (quaternion : cgmath::Quaternion <S>) -> Self {
    use cgmath::{InnerSpace, Zero};
    assert_ne!(quaternion, cgmath::Quaternion::zero());
    Versor (quaternion.normalize())
  }
  /// Panic if the given quaternion is not a unit quaternion
  pub fn noisy (unit_quaternion : cgmath::Quaternion <S>) -> Self {
    use cgmath::InnerSpace;
    assert_ulps_eq!(unit_quaternion.magnitude(), S::one(), max_ulps = 6);
    Versor (unit_quaternion)
  }
  /// It is a debug panic if the given quaternion is not a unit quaternion
  pub fn unchecked (unit_quaternion : cgmath::Quaternion <S>) -> Self {
    use cgmath::InnerSpace;
    if cfg!(debug_assertions) {
      assert_ulps_eq!(unit_quaternion.magnitude(), S::one(), max_ulps = 6);
    }
    Versor (unit_quaternion)
  }
  pub fn from_scaled_axis (scaled_axis : &cgmath::Vector3 <S>) -> Self {
    use cgmath::{InnerSpace, Rotation3, Zero};
    if *scaled_axis == cgmath::Vector3::zero() {
      debug_assert_eq!(
        cgmath::Quaternion::from_axis_angle (
          *scaled_axis, cgmath::Rad (S::zero())
        ).magnitude(),
        S::one());
      Versor (
        cgmath::Quaternion::from_axis_angle (
          *scaled_axis, cgmath::Rad (S::zero()))
      )
    } else {
      let angle = scaled_axis.magnitude();
      debug_assert!(S::zero() < angle);
      let axis  = *scaled_axis / angle;
      Versor (cgmath::Quaternion::from_axis_angle (axis, cgmath::Rad (angle)))
    }
  }
}
impl <S : Scalar> std::ops::Deref for Versor <S> {
  type Target = cgmath::Quaternion <S>;
  fn deref (&self) -> &cgmath::Quaternion <S> {
    &self.0
  }
}
//  end impl Versor
