//! Scalar and vector constants

pub mod f32 {

  use std::f32::consts::*;

  pub const SQRT_3        : f32 = 1.732050807568877293527446341505872366942805253810380628055f32;
  pub const FRAC_1_SQRT_3 : f32 = (1.0f64 / 1.732050807568877293527446341505872366942805253810380628055f64) as f32;

  //
  // f32 unit sigvecs
  //
  /// An array of unit vectors in 26 principal (+-X, +-Y, +-Z) and semiprincipal
  /// (permutations thereof) directions, from least `normalize(-1,-1,-1)` to
  /// greatest `normalize(1,1,1)`, with the zero vector in the center (index
  /// `[1][1][1]`).
  pub const UNIT_SIGVEC_ARRAY : [[[[f32; 3]; 3]; 3]; 3] = [
    // x == -1
    [
      // y == -1
      [
        [-FRAC_1_SQRT_3, -FRAC_1_SQRT_3, -FRAC_1_SQRT_3 ],
        [-FRAC_1_SQRT_2, -FRAC_1_SQRT_2,            0.0 ],
        [-FRAC_1_SQRT_3, -FRAC_1_SQRT_3,  FRAC_1_SQRT_3 ]
      ],
      // y == 0
      [
        [-FRAC_1_SQRT_2, 0.0, -FRAC_1_SQRT_2 ],
        [-1.0,           0.0,            0.0 ],
        [-FRAC_1_SQRT_2, 0.0,  FRAC_1_SQRT_2 ]
      ],
      // y == 1
      [
        [-FRAC_1_SQRT_3,  FRAC_1_SQRT_3, -FRAC_1_SQRT_3 ],
        [-FRAC_1_SQRT_2,  FRAC_1_SQRT_2,            0.0 ],
        [-FRAC_1_SQRT_3,  FRAC_1_SQRT_3,  FRAC_1_SQRT_3 ]
      ]
    ],
    // x == 0
    [
      // y == -1
      [
        [0.0, -FRAC_1_SQRT_2, -FRAC_1_SQRT_2 ],
        [0.0,           -1.0,            0.0 ],
        [0.0, -FRAC_1_SQRT_2,  FRAC_1_SQRT_2 ]
      ],
      // y == 0
      [
        [0.0,            0.0,           -1.0 ],
        [0.0,            0.0,            0.0 ],
        [0.0,            0.0,            1.0 ]
      ],
      // y == 1
      [
        [0.0,  FRAC_1_SQRT_2, -FRAC_1_SQRT_2 ],
        [0.0,            1.0,            0.0 ],
        [0.0,  FRAC_1_SQRT_2,  FRAC_1_SQRT_2 ]
      ]
    ],
    // x == 1
    [
      // y == -1
      [
        [FRAC_1_SQRT_3, -FRAC_1_SQRT_3, -FRAC_1_SQRT_3 ],
        [FRAC_1_SQRT_2, -FRAC_1_SQRT_2,            0.0 ],
        [FRAC_1_SQRT_3, -FRAC_1_SQRT_3,  FRAC_1_SQRT_3 ]
      ],
      // y == 0
      [
        [FRAC_1_SQRT_2, 0.0, -FRAC_1_SQRT_2 ],
        [1.0,           0.0,            0.0 ],
        [FRAC_1_SQRT_2, 0.0,  FRAC_1_SQRT_2 ]
      ],
      // y == 1
      [
        [FRAC_1_SQRT_3,  FRAC_1_SQRT_3, -FRAC_1_SQRT_3 ],
        [FRAC_1_SQRT_2,  FRAC_1_SQRT_2,            0.0 ],
        [FRAC_1_SQRT_3,  FRAC_1_SQRT_3,  FRAC_1_SQRT_3 ]
      ]
    ]
  ];

} // end f32

pub mod f64 {

  use std::f64::consts::*;

  pub const SQRT_3        : f64 = 1.732050807568877293527446341505872366942805253810380628055f64;
  pub const FRAC_1_SQRT_3 : f64 = 1.0f64 / 1.732050807568877293527446341505872366942805253810380628055f64;

  //
  // f64 unit sigvecs
  //
  /// An array of unit vectors in 26 principal (+-X, +-Y, +-Z) and semiprincipal
  /// (permutations thereof) directions, from least `normalize(-1,-1,-1)` to
  /// greatest `normalize(1,1,1)`, with the zero vector in the center (index
  /// `[1][1][1]`).
  pub const UNIT_SIGVEC_ARRAY : [[[[f64; 3]; 3]; 3]; 3] = [
    // x == -1
    [
      // y == -1
      [
        [-FRAC_1_SQRT_3, -FRAC_1_SQRT_3, -FRAC_1_SQRT_3 ],
        [-FRAC_1_SQRT_2, -FRAC_1_SQRT_2,            0.0 ],
        [-FRAC_1_SQRT_3, -FRAC_1_SQRT_3,  FRAC_1_SQRT_3 ]
      ],
      // y == 0
      [
        [-FRAC_1_SQRT_2, 0.0, -FRAC_1_SQRT_2 ],
        [-1.0,           0.0,            0.0 ],
        [-FRAC_1_SQRT_2, 0.0,  FRAC_1_SQRT_2 ]
      ],
      // y == 1
      [
        [-FRAC_1_SQRT_3,  FRAC_1_SQRT_3, -FRAC_1_SQRT_3 ],
        [-FRAC_1_SQRT_2,  FRAC_1_SQRT_2,            0.0 ],
        [-FRAC_1_SQRT_3,  FRAC_1_SQRT_3,  FRAC_1_SQRT_3 ]
      ]
    ],
    // x == 0
    [
      // y == -1
      [
        [0.0, -FRAC_1_SQRT_2, -FRAC_1_SQRT_2 ],
        [0.0,           -1.0,            0.0 ],
        [0.0, -FRAC_1_SQRT_2,  FRAC_1_SQRT_2 ]
      ],
      // y == 0
      [
        [0.0,            0.0,           -1.0 ],
        [0.0,            0.0,            0.0 ],
        [0.0,            0.0,            1.0 ]
      ],
      // y == 1
      [
        [0.0,  FRAC_1_SQRT_2, -FRAC_1_SQRT_2 ],
        [0.0,            1.0,            0.0 ],
        [0.0,  FRAC_1_SQRT_2,  FRAC_1_SQRT_2 ]
      ]
    ],
    // x == 1
    [
      // y == -1
      [
        [FRAC_1_SQRT_3, -FRAC_1_SQRT_3, -FRAC_1_SQRT_3 ],
        [FRAC_1_SQRT_2, -FRAC_1_SQRT_2,            0.0 ],
        [FRAC_1_SQRT_3, -FRAC_1_SQRT_3,  FRAC_1_SQRT_3 ]
      ],
      // y == 0
      [
        [FRAC_1_SQRT_2, 0.0, -FRAC_1_SQRT_2 ],
        [1.0,           0.0,            0.0 ],
        [FRAC_1_SQRT_2, 0.0,  FRAC_1_SQRT_2 ]
      ],
      // y == 1
      [
        [FRAC_1_SQRT_3,  FRAC_1_SQRT_3, -FRAC_1_SQRT_3 ],
        [FRAC_1_SQRT_2,  FRAC_1_SQRT_2,            0.0 ],
        [FRAC_1_SQRT_3,  FRAC_1_SQRT_3,  FRAC_1_SQRT_3 ]
      ]
    ]
  ];

} // end f64
