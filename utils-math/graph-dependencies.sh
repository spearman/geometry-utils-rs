#!/usr/bin/env bash
set -x

cargo graph > dependencies.dot && make -f MakefileDot dependencies \
  && feh dependencies.png

exit
