//! 3D volumetric forms

use std::convert::TryFrom;
use cgmath;
use derive_more::From;
#[cfg(feature = "derive_serdes")]
use serde::{Deserialize, Serialize};

use crate::{math::{self, Scalar}, Aabb3, Capsule3, Cylinder3, Sphere3};

////////////////////////////////////////////////////////////////////////////////
//  traits                                                                    //
////////////////////////////////////////////////////////////////////////////////

/// A trait for bounded and unbounded shapes.
///
/// A 'Shape' implements the following traits:
///
/// - 'Aabb' -- a shape can compute the axis-aligned bounding volume that
///   encloses it
/// - 'Bsphere' -- TODO: a shape can compute its bounding sphere
///
/// Both of these traits carry the constraint that a shape implements the
/// 'Stereometric' trait which allows a shape to compute its volume (non-zero,
/// possibly infinite).
pub trait Shape <S : Scalar> : Aabb <S> /*+ Bsphere <S>*/ { } // TODO: bspheres

/// Trait for computing the axis-aligned bounding volume of a given shape.
///
/// Note for 'Unbounded' shapes some of the components will contain positive or
/// negative infinity.
pub trait Aabb <S : Scalar> : Stereometric <S> {
  fn aabb (&self) -> Aabb3 <S>;
}

// TODO: implement bounding spheres
/// A trait for computing bounding spheres
pub trait Bsphere <S : Scalar> : Stereometric <S> {
  fn sphere (&self) -> Sphere3 <S>;
}

/// Trait for computing volumes of solid figures.
///
/// Note for 'Unbounded' shapes this will be positive infinitiy.
pub trait Stereometric <S : Scalar> {
  fn volume (&self) -> math::Positive <S>;
}

////////////////////////////////////////////////////////////////////////////////
//  enums                                                                     //
////////////////////////////////////////////////////////////////////////////////

/// A shape that is either bounded (encloses a finite volume) or unbounded
/// (delimits an infinite volume)
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(From, Clone, Debug, PartialEq)]
pub enum Variant <S : Scalar> {
  Bounded   (Bounded   <S>),
  Unbounded (Unbounded <S>)
}

/// A (totally) bounded shape
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(From, Clone, Debug, PartialEq)]
pub enum Bounded <S : Scalar> {
  Sphere   (Sphere   <S>),
  Capsule  (Capsule  <S>),
  Cylinder (Cylinder <S>),
  Cone     (Cone     <S>),
  Cube     (Cube     <S>),
  Cuboid   (Cuboid   <S>)
}

/// A shape that is only partly bounded
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(From, Clone, Debug, PartialEq)]
pub enum Unbounded <S : Scalar> {
  Orthant   (Orthant),
  Halfspace (Halfspace <S>)
}

////////////////////////////////////////////////////////////////////////////////
//  structs                                                                   //
////////////////////////////////////////////////////////////////////////////////

/// An axis-aligned halfspace
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Orthant {
  pub normal_axis : math::SignedAxis3
}

/// A halfspace defined by a normal vector
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Halfspace <S : Scalar> {
  pub normal_vector : math::Unit3 <S>
}

/// A sphere defined by a positive radius
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Sphere <S : Scalar> {
  pub radius : math::Positive <S>
}

/// A capsule defined by a strictly positive radius and a non-negative
/// half-height.
///
/// Note that a capsule with a zero half-height encloses a volume equivalent to
/// a sphere of the same radius.
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Capsule <S : Scalar> {
  pub radius      : math::Positive <S>,
  pub half_height : math::NonNegative <S>
}

/// A cylinder defined by strictly positive radius and half-height
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Cylinder <S : Scalar> {
  pub radius      : math::Positive <S>,
  pub half_height : math::Positive <S>
}

/// A cone defined by strictly positive radius and half-height
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Cone <S : Scalar> {
  pub radius      : math::Positive <S>,
  pub half_height : math::Positive <S>
}

/// A cube defined by a strictly positive half-extent
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Cube <S : Scalar> {
  pub half_extent : math::Positive <S>
}

/// A box defined by three strictly positive half extents
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Cuboid <S : Scalar> {
  pub half_extent_x : math::Positive <S>,
  pub half_extent_y : math::Positive <S>,
  pub half_extent_z : math::Positive <S>
}

////////////////////////////////////////////////////////////////////////////////
//  functions                                                                 //
////////////////////////////////////////////////////////////////////////////////

pub fn report_sizes() {
  use std::mem::size_of;
  println!("shape report sizes...");

  macro_rules! show {
    ($e:expr) => { println!("{}: {:?}", stringify!($e), $e); }
  }

  show!(size_of::<Variant   <f32>>());
  show!(size_of::<Variant   <f64>>());

  show!(size_of::<Bounded   <f32>>());
  show!(size_of::<Bounded   <f64>>());

  show!(size_of::<Unbounded <f32>>());
  show!(size_of::<Unbounded <f64>>());

  show!(size_of::<Sphere    <f32>>());
  show!(size_of::<Capsule   <f32>>());
  show!(size_of::<Cylinder  <f32>>());
  show!(size_of::<Cone      <f32>>());
  show!(size_of::<Cube      <f32>>());
  show!(size_of::<Cuboid    <f32>>());

  show!(size_of::<Orthant>());
  show!(size_of::<Halfspace <f32>>());

  show!(size_of::<Sphere    <f64>>());
  show!(size_of::<Capsule   <f64>>());
  show!(size_of::<Cylinder  <f64>>());
  show!(size_of::<Cone      <f64>>());
  show!(size_of::<Cube      <f64>>());
  show!(size_of::<Cuboid    <f64>>());

  show!(size_of::<Orthant>());
  show!(size_of::<Halfspace <f64>>());

  println!("...shape report sizes");
}

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

//
//  impl Variant
//
impl <S : Scalar> Shape <S> for Variant <S> { }
impl <S : Scalar> Aabb <S> for Variant <S> {
  /// Note that unbounded shapes will return a Cuboid with some infinite half
  /// extents
  fn aabb (&self) -> Aabb3 <S> {
    match self {
      Variant::Bounded   (ref bounded)   => bounded.aabb(),
      Variant::Unbounded (ref unbounded) => unbounded.aabb()
    }
  }
}
impl <S : Scalar> Stereometric <S> for Variant <S> {
  /// Note that unbounded shapes will return a Cuboid with some infinite half
  /// extents
  fn volume (&self) -> math::Positive <S> {
    match self {
      Variant::Bounded   (ref bounded)   => bounded.volume(),
      Variant::Unbounded (ref unbounded) => unbounded.volume()
    }
  }
}

//
//  impl Bounded
//
impl <S : Scalar> Shape <S> for Bounded <S> { }
impl <S : Scalar> TryFrom <Variant <S>> for Bounded <S> {
  type Error = Variant <S>;
  fn try_from (variant : Variant <S>) -> Result <Self, Self::Error> {
    match variant {
      Variant::Bounded (bounded) => Ok (bounded),
      _ => Err (variant)
    }
  }
}
impl <S : Scalar> Stereometric <S> for Bounded <S> {
  /// Volume of a bounded solid is always finite
  fn volume (&self) -> math::Positive <S> {
    match self {
      Bounded::Sphere   (ref sphere)   => sphere.volume(),
      Bounded::Capsule  (ref capsule)  => capsule.volume(),
      Bounded::Cylinder (ref cylinder) => cylinder.volume(),
      Bounded::Cone     (ref cone)     => cone.volume(),
      Bounded::Cube     (ref cube)     => cube.volume(),
      Bounded::Cuboid   (ref cuboid)   => cuboid.volume()
    }
  }
}
impl <S : Scalar> Aabb <S> for Bounded <S> {
  fn aabb (&self) -> Aabb3 <S> {
    match self {
      Bounded::Sphere   (ref sphere)   => sphere.aabb(),
      Bounded::Capsule  (ref capsule)  => capsule.aabb(),
      Bounded::Cylinder (ref cylinder) => cylinder.aabb(),
      Bounded::Cone     (ref cone)     => cone.aabb(),
      Bounded::Cube     (ref cube)     => cube.aabb(),
      Bounded::Cuboid   (ref cuboid)   => cuboid.aabb()
    }
  }
}

//
//  impl Unbounded
//
impl <S : Scalar> Shape <S> for Unbounded <S> { }
impl <S : Scalar> TryFrom <Variant <S>> for Unbounded <S> {
  type Error = Variant <S>;
  fn try_from (variant : Variant <S>) -> Result <Self, Self::Error> {
    match variant {
      Variant::Unbounded (unbounded) => Ok (unbounded),
      _ => Err (variant)
    }
  }
}
impl <S : Scalar> Aabb <S> for Unbounded <S> {
  fn aabb (&self) -> Aabb3 <S> {
    match self {
      Unbounded::Orthant   (ref orthant)   => orthant.aabb(),
      Unbounded::Halfspace (ref halfspace) => halfspace.aabb()
    }
  }
}
impl <S : Scalar> Stereometric <S> for Unbounded <S> {
  /// Volume of an unbounded solid is always infinite
  fn volume (&self) -> math::Positive <S> {
    if cfg!(debug_assertions) {
      let volume = match self {
        Unbounded::Orthant   (ref orthant)   => orthant.volume(),
        Unbounded::Halfspace (ref halfspace) => halfspace.volume()
      };
      debug_assert_eq!(*volume, S::infinity());
    }
    math::Positive::infinity()
  }
}

//
//  impl Sphere
//
impl <S : Scalar> Shape <S> for Sphere <S> { }
impl <S : Scalar> Sphere <S> {
  #[inline]
  /// Sphere with radius 1.0
  pub fn unit() -> Self {
    Sphere { radius: math::Positive::one() }
  }
  /// Create a new sphere with the absolute value of the given radius.
  ///
  /// Panics if radius is zero:
  ///
  /// ```should_panic
  /// # use geometry_utils::shape::Sphere;
  /// let s = Sphere::noisy (0.0);
  /// ```
  #[inline]
  pub fn noisy (radius : S) -> Self {
    assert_ne!(radius, S::zero());
    Sphere { radius: math::Positive::unchecked (radius.abs()) }
  }
  /// Create a new sphere with the absolute value of the given radius.
  ///
  /// Debug panic if radius is zero:
  ///
  /// ```should_panic
  /// # use geometry_utils::shape::Sphere;
  /// let s = Sphere::noisy (0.0);
  /// ```
  #[inline]
  pub fn unchecked (radius : S) -> Self {
    debug_assert_ne!(radius, S::zero());
    Sphere { radius: math::Positive::unchecked (radius.abs()) }
  }
  #[inline]
  pub fn sphere3 (&self, center : cgmath::Point3 <S>) -> Sphere3 <S> {
    Sphere3 { center, radius: self.radius }
  }
}
impl <S : Scalar> TryFrom <Bounded <S>> for Sphere <S> {
  type Error = Bounded <S>;
  fn try_from (bounded : Bounded <S>) -> Result <Self, Self::Error> {
    match bounded {
      Bounded::Sphere (sphere) => Ok (sphere),
      _ => Err (bounded)
    }
  }
}
impl <S : Scalar> Stereometric <S> for Sphere <S> {
  fn volume (&self) -> math::Positive <S> {
    let four      = math::Positive::unchecked (S::four());
    let frac_pi_3 = math::Positive::unchecked (S::FRAC_PI_3());
    four * frac_pi_3 * self.radius * self.radius * self.radius
  }
}
impl <S : Scalar> Aabb <S> for Sphere <S> {
  fn aabb (&self) -> Aabb3 <S> {
    Aabb3::with_minmax ([-*self.radius; 3].into(), [ *self.radius; 3].into())
  }
}

//
//  impl Capsule
//
impl <S : Scalar> Shape <S> for Capsule <S> { }
impl <S : Scalar> Capsule <S> {
  /// Create a new capsule with the absolute values of the given radius and
  /// half-height.
  ///
  /// Panics if radius is zero:
  ///
  /// ```should_panic
  /// # use geometry_utils::shape::Capsule;
  /// let s = Capsule::noisy (0.0, 2.0);
  /// ```
  #[inline]
  pub fn noisy (radius : S, half_height : S) -> Self {
    assert_ne!(radius, S::zero());
    let radius      = math::Positive::unchecked    (radius.abs());
    let half_height = math::NonNegative::unchecked (half_height.abs());
    Capsule { radius, half_height }
  }
  /// Create a new capsule with the absolute values of the given radius and
  /// half-height.
  ///
  /// Debug panic if radius is zero:
  ///
  /// ```should_panic
  /// # use geometry_utils::shape::Capsule;
  /// let s = Capsule::unchecked (0.0, 2.0);
  /// ```
  #[inline]
  pub fn unchecked (radius : S, half_height : S) -> Self {
    debug_assert_ne!(radius, S::zero());
    let radius      = math::Positive::unchecked    (radius.abs());
    let half_height = math::NonNegative::unchecked (half_height.abs());
    Capsule { radius, half_height }
  }
  /// Height of the cylinder portion
  #[inline]
  pub fn height (&self) -> math::NonNegative <S> {
    self.half_height * math::NonNegative::unchecked (S::two())
  }
  #[inline]
  pub fn capsule3 (&self, center : cgmath::Point3 <S>) -> Capsule3 <S> {
    Capsule3 { center, radius: self.radius, half_height: self.half_height }
  }
}
impl <S : Scalar> Stereometric <S> for Capsule <S> {
  fn volume (&self) -> math::Positive <S> {
    let r               = self.radius;
    let h               = self.height();
    let r2              = r * r;
    let r3              = r2 * r;
    let pi              = math::Positive::unchecked (S::PI());
    let four            = math::Positive::unchecked (S::four());
    let frac_pi_3       = math::Positive::unchecked (S::FRAC_PI_3());
    let cylinder_volume = pi * r2 * h;
    let sphere_volume   = four * frac_pi_3 * r3;
    sphere_volume + cylinder_volume
  }
}
impl <S : Scalar> Aabb <S> for Capsule <S> {
  fn aabb (&self) -> Aabb3 <S> {
    let r  = *self.radius;
    let hh = *self.half_height;
    Aabb3::with_minmax ([-r, -r, -r - hh].into(), [ r,  r,  r + hh].into())
  }
}

//
//  impl Cylinder
//
impl <S : Scalar> Shape <S> for Cylinder <S> { }
impl <S : Scalar> Cylinder <S> {
  #[inline]
  /// Cylinder with radius 1.0 and half-height 1.0
  pub fn unit() -> Self {
    Cylinder {
      radius: math::Positive::one(), half_height: math::Positive::one()
    }
  }
  /// Create a new cylinder with the absolute values of the given radius and
  /// half-height.
  ///
  /// Panics if radius or half-height are zero:
  ///
  /// ```should_panic
  /// # use geometry_utils::shape::Cylinder;
  /// let s = Cylinder::noisy (0.0, 0.0);
  /// ```
  #[inline]
  pub fn noisy (radius : S, half_height : S) -> Self {
    assert_ne!(radius,      S::zero());
    assert_ne!(half_height, S::zero());
    let radius      = math::Positive::unchecked (radius.abs());
    let half_height = math::Positive::unchecked (half_height.abs());
    Cylinder { radius, half_height }
  }
  /// Create a new cylinder with the absolute values of the given radius and
  /// half-height.
  ///
  /// Debug panic if radius or half-height are zero:
  ///
  /// ```should_panic
  /// # use geometry_utils::shape::Cylinder;
  /// let s = Cylinder::unchecked (0.0, 0.0);
  /// ```
  #[inline]
  pub fn unchecked (radius : S, half_height : S) -> Self {
    debug_assert_ne!(radius,      S::zero());
    debug_assert_ne!(half_height, S::zero());
    let radius      = math::Positive::unchecked (radius.abs());
    let half_height = math::Positive::unchecked (half_height.abs());
    Cylinder { radius, half_height }
  }
  #[inline]
  pub fn cylinder3 (&self, center : cgmath::Point3 <S>) -> Cylinder3 <S> {
    Cylinder3 { center, radius: self.radius, half_height: self.half_height }
  }
  #[inline]
  pub fn height (&self) -> math::Positive <S> {
    self.half_height * math::Positive::unchecked (S::two())
  }
}
impl <S : Scalar> Stereometric <S> for Cylinder <S> {
  fn volume (&self) -> math::Positive <S> {
    let pi = math::Positive::unchecked (S::PI());
    pi * self.radius * self.radius * self.height()
  }
}
impl <S : Scalar> Aabb <S> for Cylinder <S> {
  fn aabb (&self) -> Aabb3 <S> {
    let r  = *self.radius;
    let hh = *self.half_height;
    Aabb3::with_minmax ([-r, -r, -hh].into(), [ r,  r,  hh].into())
  }
}

//
//  impl Cone
//
impl <S : Scalar> Shape <S> for Cone <S> { }
impl <S : Scalar> Cone <S> {
  /// Create a new cone with the absolute values of the given radius and
  /// half-height.
  ///
  /// Panics if radius or half-height are zero:
  ///
  /// ```should_panic
  /// # use geometry_utils::shape::Cone;
  /// let s = Cone::noisy (0.0, 0.0);
  /// ```
  #[inline]
  pub fn noisy (radius : S, half_height : S) -> Self {
    assert_ne!(radius,      S::zero());
    assert_ne!(half_height, S::zero());
    let radius      = math::Positive::unchecked (radius.abs());
    let half_height = math::Positive::unchecked (half_height.abs());
    Cone { radius, half_height }
  }
}
impl <S : Scalar> Stereometric <S> for Cone <S> {
  fn volume (&self) -> math::Positive <S> {
    let frac_pi_3 = math::Positive::unchecked (S::FRAC_PI_3());
    let two       = math::Positive::unchecked (S::two());
    frac_pi_3 * self.radius * self.radius * two * self.half_height
  }
}
impl <S : Scalar> Aabb <S> for Cone <S> {
  fn aabb (&self) -> Aabb3 <S> {
    let r  = *self.radius;
    let hh = *self.half_height;
    Aabb3::with_minmax ([-r, -r, -hh].into(), [ r,  r,  hh].into())
  }
}

//
//  impl Cube
//
impl <S : Scalar> Shape <S> for Cube <S> { }
impl <S : Scalar> Cube <S> {
  /// Create a new cube with the absolute value of the given half-extent.
  ///
  /// Panics if half-extent is zero:
  ///
  /// ```should_panic
  /// # use geometry_utils::shape::Cube;
  /// let s = Cube::noisy (0.0);
  /// ```
  #[inline]
  pub fn noisy (half_extent : S) -> Self {
    assert_ne!(half_extent, S::zero());
    let half_extent = math::Positive::unchecked (half_extent.abs());
    Cube { half_extent }
  }
  #[inline]
  pub fn extent (&self) -> math::Positive <S> {
    self.half_extent * math::Positive::unchecked (S::two())
  }
}
impl <S : Scalar> Stereometric <S> for Cube <S> {
  fn volume (&self) -> math::Positive <S> {
    let extent = self.extent();
    extent * extent * extent
  }
}
impl <S : Scalar> Aabb <S> for Cube <S> {
  fn aabb (&self) -> Aabb3 <S> {
    Aabb3::with_minmax (
      [-*self.half_extent; 3].into(),
      [ *self.half_extent; 3].into())
  }
}

//
//  impl Cuboid
//
impl <S : Scalar> Shape <S> for Cuboid <S> { }
impl <S : Scalar> Cuboid <S> {
  /// Create a new cuboid with the absolute values of the given half-extents.
  ///
  /// Panics if any half-extent is zero:
  ///
  /// ```should_panic
  /// # use geometry_utils::shape::Cuboid;
  /// let s = Cuboid::noisy ([0.0, 0.0, 0.0]);
  /// ```
  #[inline]
  pub fn noisy (half_extents : [S; 3]) -> Self {
    assert_ne!(half_extents[0], S::zero());
    assert_ne!(half_extents[1], S::zero());
    assert_ne!(half_extents[2], S::zero());
    let half_extent_x = math::Positive::unchecked (half_extents[0].abs());
    let half_extent_y = math::Positive::unchecked (half_extents[1].abs());
    let half_extent_z = math::Positive::unchecked (half_extents[2].abs());
    Cuboid { half_extent_x, half_extent_y, half_extent_z }
  }
  #[inline]
  pub fn extents (&self) -> [math::Positive <S>; 3] {
    let two = math::Positive::unchecked (S::two());
    [
      self.half_extent_x * two,
      self.half_extent_y * two,
      self.half_extent_z * two
    ]
  }
  #[inline]
  pub fn half_extents_vec (&self) -> cgmath::Vector3 <S> {
    [ *self.half_extent_x,
      *self.half_extent_y,
      *self.half_extent_z
    ].into()
  }
  #[inline]
  pub fn max (&self) -> cgmath::Point3 <S> {
    cgmath::Point3 {
      x: *self.half_extent_x,
      y: *self.half_extent_y,
      z: *self.half_extent_z
    }
  }
  #[inline]
  pub fn min (&self) -> cgmath::Point3 <S> {
    cgmath::Point3 {
      x: -*self.half_extent_x,
      y: -*self.half_extent_y,
      z: -*self.half_extent_z
    }
  }
}
impl <S : Scalar> Stereometric <S> for Cuboid <S> {
  fn volume (&self) -> math::Positive <S> {
    let [x, y, z] = self.extents();
    x * y * z
  }
}
impl <S : Scalar> Aabb <S> for Cuboid <S> {
  fn aabb (&self) -> Aabb3 <S> {
    Aabb3::with_minmax (self.min(), self.max())
  }
}

//
//  impl Orthant
//
impl <S : Scalar> Shape <S> for Orthant { }
impl Orthant {
  pub fn try_from <S : Scalar> (halfspace : &Halfspace <S>) -> Option <Self> {
    if let Some (normal_axis) = math::SignedAxis3::try_from (&halfspace.normal_vector) {
      Some (Orthant { normal_axis })
    } else {
      None
    }
  }
}
impl <S : Scalar> Aabb <S> for Orthant {
  fn aabb (&self) -> Aabb3 <S> {
    use cgmath::num_traits::Float;
    let surface = cgmath::vec3 (S::one(), S::one(), S::one()) -
      self.normal_axis.to_vec().map (Float::abs);

    let min = [
      if surface.x == S::one() { S::neg_infinity() } else { S::zero() },
      if surface.y == S::one() { S::neg_infinity() } else { S::zero() },
      if surface.z == S::one() { S::neg_infinity() } else { S::zero() }
    ].into();
    let max = [
      if surface.x == S::one() { S::infinity() } else { S::zero() },
      if surface.y == S::one() { S::infinity() } else { S::zero() },
      if surface.z == S::one() { S::infinity() } else { S::zero() }
    ].into();

    Aabb3::with_minmax (min, max)
  }
}
impl <S : Scalar> Stereometric <S> for Orthant {
  /// Volume of an unbounded solid is always infinite
  fn volume (&self) -> math::Positive <S> {
    math::Positive::infinity()
  }
}
impl From <math::SignedAxis3> for Orthant {
  fn from (normal_axis : math::SignedAxis3) -> Self {
    Orthant { normal_axis }
  }
}

//
//  impl Halfspace
//
impl <S : Scalar> Shape <S> for Halfspace <S> { }
impl <S : Scalar> Aabb <S> for Halfspace <S> {
  fn aabb (&self) -> Aabb3 <S> {
    if let Some (orthant) = Orthant::try_from (&self) {
      orthant.aabb()
    } else {
      Aabb3::with_minmax (
        [S::infinity(); 3].into(),
        [S::neg_infinity(); 3].into())
    }
  }
}
impl <S : Scalar> Stereometric <S> for Halfspace <S> {
  /// Volume of an unbounded solid is always infinite
  fn volume (&self) -> math::Positive <S> {
    math::Positive::infinity()
  }
}
impl <S : Scalar> From <math::Unit3 <S>> for Halfspace <S> {
  fn from (normal_vector : math::Unit3 <S>) -> Self {
    Halfspace { normal_vector }
  }
}
