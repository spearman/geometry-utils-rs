//! Geometry utilities

#![warn(unused_extern_crates)]

pub extern crate math_utils as math;

pub mod intersect;
pub mod primitive;
pub mod shape;

pub use self::primitive::*;
pub use self::shape::Shape;
