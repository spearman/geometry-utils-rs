//! Affine, convex, and combinatorial spaces

use rand;
use cgmath::{self, relative_eq};

use crate::{math::{self, Scalar}, intersect, shape};

#[cfg(feature = "derive_serdes")]
use serde::{Deserialize, Serialize};

/// Primitives over signed integers
pub mod integer;

mod simplex;
pub use self::simplex::{simplex2, simplex3};
pub use simplex2::Simplex2;
pub use simplex3::Simplex3;
pub use simplex2::Point       as Point2;
pub use simplex3::Point       as Point3;
pub use simplex2::Segment     as Segment2;
pub use simplex3::Segment     as Segment3;
pub use simplex2::Triangle    as Triangle2;
pub use simplex3::Triangle    as Triangle3;
pub use simplex3::Tetrahedron as Tetrahedron3;

////////////////////////////////////////////////////////////////////////////////
//  structs                                                                   //
////////////////////////////////////////////////////////////////////////////////

/// 1D axis-aligned bounding box (interval)
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Debug,PartialEq)]
pub struct Aabb1 <S : Scalar> {
  min : cgmath::Point1 <S>,
  max : cgmath::Point1 <S>
}

/// 2D axis-aligned bounding box
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Debug,PartialEq)]
pub struct Aabb2 <S : Scalar> {
  min : cgmath::Point2 <S>,
  max : cgmath::Point2 <S>
}

/// 3D axis-aligned bounding box.
///
/// See also `shape::Aabb` trait for primitive and shape types that can compute
/// a 3D AABB.
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Debug,PartialEq)]
pub struct Aabb3 <S : Scalar> {
  min : cgmath::Point3 <S>,
  max : cgmath::Point3 <S>
}

/// 3D Z-axis-aligned cylinder
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Debug,PartialEq)]
pub struct Cylinder3 <S : Scalar> {
  pub center      : cgmath::Point3 <S>,
  pub half_height : math::Positive <S>,
  pub radius      : math::Positive <S>
}

/// 3D Z-axis-aligned capsule
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Debug,PartialEq)]
pub struct Capsule3 <S : Scalar> {
  pub center      : cgmath::Point3    <S>,
  pub half_height : math::NonNegative <S>,
  pub radius      : math::Positive    <S>
}

/// An infinitely extended line in 2D space defined by a base point and
/// normalized direction
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Debug,PartialEq)]
pub struct Line2 <S : Scalar> {
  pub base      : cgmath::Point2 <S>,
  pub direction : math::Unit2 <S>
}

/// An infinitely extended line in 3D space defined by a base point and
/// normalized direction
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Debug,PartialEq)]
pub struct Line3 <S : Scalar> {
  pub base      : cgmath::Point3 <S>,
  pub direction : math::Unit3 <S>
}

/// A plane in 3D space defined by a base point and (unit) normal vector
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Debug,PartialEq)]
pub struct Plane3 <S : Scalar> {
  pub base   : cgmath::Point3 <S>,
  pub normal : math::Unit3 <S>
}

/// Sphere in 2D space (a circle)
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Debug,PartialEq)]
pub struct Sphere2 <S : Scalar> {
  pub center : cgmath::Point2 <S>,
  pub radius : math::Positive <S>
}

/// Sphere in 3D space
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone,Debug,PartialEq)]
pub struct Sphere3 <S : Scalar> {
  pub center : cgmath::Point3 <S>,
  pub radius : math::Positive <S>
}

////////////////////////////////////////////////////////////////////////////////
//  functions                                                                 //
////////////////////////////////////////////////////////////////////////////////

/// Returns true when three points lie on the same line in 2D space.
///
/// ```
/// # use geometry_utils::colinear_2d;
/// assert!(colinear_2d (
///   &[-1.0, -1.0].into(),
///   &[ 0.0,  0.0].into(),
///   &[ 1.0,  1.0].into())
/// );
/// assert!(!colinear_2d (
///   &[-1.0, -1.0].into(),
///   &[ 0.0,  1.0].into(),
///   &[ 1.0, -1.0].into())
/// );
/// ```
pub fn colinear_2d <S : Scalar> (
  a : &cgmath::Point2 <S>,
  b : &cgmath::Point2 <S>,
  c : &cgmath::Point2 <S>
) -> bool {
  // early exit: a pair of points are equal
  if a == b || a == c || b == c {
    true
  } else {
    let a = cgmath::Point3::new (a.x, a.y, S::one());
    let b = cgmath::Point3::new (b.x, b.y, S::one());
    let c = cgmath::Point3::new (c.x, c.y, S::one());
    let determinant = determinant_3d (&a, &b, &c);
    // a zero determinant indicates that the points are colinear
    relative_eq!(S::zero(), determinant)
  }
}

/// Returns true when three points lie on the same line in 3D space.
///
/// ```
/// # use geometry_utils::colinear_3d;
/// assert!(colinear_3d (
///   &[-1.0, -1.0, -1.0].into(),
///   &[ 0.0,  0.0,  0.0].into(),
///   &[ 1.0,  1.0,  1.0].into())
/// );
/// ```
pub fn colinear_3d <S : Scalar> (
  a : &cgmath::Point3 <S>,
  b : &cgmath::Point3 <S>,
  c : &cgmath::Point3 <S>
) -> bool {
  // early exit: a pair of points are equal
  if a == b || a == c || b == c {
    true
  } else {
    let determinant = determinant_3d (a, b, c);
    // a zero determinant indicates that the points are coplanar
    if relative_eq!(S::zero(), determinant) {
      relative_eq!(S::zero(), triangle_3d_area2 (a, b, c))
    } else {
      false
    }
  }
}

/// Returns true when four points lie on the same plane in 3D space.
///
/// ```
/// # use geometry_utils::coplanar_3d;
/// assert!(coplanar_3d (
///   &[-1.0, -1.0, -1.0].into(),
///   &[ 1.0,  1.0,  1.0].into(),
///   &[-1.0,  1.0,  0.0].into(),
///   &[ 1.0, -1.0,  0.0].into()
/// ));
/// ```
pub fn coplanar_3d <S : Scalar> (
  a : &cgmath::Point3 <S>,
  b : &cgmath::Point3 <S>,
  c : &cgmath::Point3 <S>,
  d : &cgmath::Point3 <S>
) -> bool {
  // early exit: a pair of points are equal
  if a == b || a == c || a == d || b == c || b == d || c == d {
    true
  } else {
    use cgmath::InnerSpace;
    let ab_cross_ac_dot_ad = (b-a).cross (c-a).dot (d-a);
    relative_eq!(S::zero(), ab_cross_ac_dot_ad)
  }
}

/// Square area of three points in 3D space.
///
/// Uses a numerically stable Heron's formula:
/// <https://en.wikipedia.org/wiki/Heron%27s_formula#Numerical_stability>
///
/// ```
/// # #[macro_use] extern crate cgmath;
/// # use cgmath::assert_relative_eq;
/// # use geometry_utils::triangle_3d_area2;
/// assert_relative_eq!(
///   3.0/4.0,
///   triangle_3d_area2 (
///     &[-1.0,  0.0,  0.0].into(),
///     &[ 0.0,  0.0,  1.0].into(),
///     &[ 0.0,  1.0,  0.0].into())
/// );
/// ```
///
/// If the area squared is zero then the points are colinear:
///
/// ```
/// # use geometry_utils::triangle_3d_area2;
/// assert_eq!(
///   0.0,
///   triangle_3d_area2 (
///     &[-1.0, -1.0, -1.0].into(),
///     &[ 0.0,  0.0,  0.0].into(),
///     &[ 1.0,  1.0,  1.0].into())
/// );
/// ```
pub fn triangle_3d_area2 <S : Scalar> (
  a : &cgmath::Point3 <S>,
  b : &cgmath::Point3 <S>,
  c : &cgmath::Point3 <S>
) -> S {
  use cgmath::{InnerSpace};
  if a == b || a == c || b == c {
    return S::zero()
  }
  // compute the length of each side
  let ab_mag = (b-a).magnitude();
  let ac_mag = (c-a).magnitude();
  let bc_mag = (c-b).magnitude();
  // order as max >= mid >= min
  let max = S::max (S::max (ab_mag, ac_mag), bc_mag);
  let min = S::min (S::min (ab_mag, ac_mag), bc_mag);
  let mid = if min <= ab_mag && ab_mag <= max {
    ab_mag
  } else if min <= ac_mag && ac_mag <= max {
    ac_mag
  } else {
    bc_mag
  };
  (S::one() + S::one()).powi(-4)  // 1.0/16.0
    * (max + (mid + min))
    * (min - (max - mid))
    * (min + (max - mid))
    * (max + (mid - min))
}

/// Computes the determinant of a matrix formed by the three points as columns
#[inline]
pub fn determinant_3d <S : Scalar> (
  a : &cgmath::Point3 <S>,
  b : &cgmath::Point3 <S>,
  c : &cgmath::Point3 <S>
) -> S {
  use cgmath::{EuclideanSpace, SquareMatrix};
  cgmath::Matrix3::from_cols (a.to_vec(), b.to_vec(), c.to_vec()).determinant()
}

/// Coordinate-wise min
pub fn point2_min <S : Scalar> (a : &cgmath::Point2 <S>, b : &cgmath::Point2 <S>)
  -> cgmath::Point2 <S>
{
  [ S::min (a.x, b.x),
    S::min (a.y, b.y)
  ].into()
}

/// Coordinate-wise max
pub fn point2_max <S : Scalar> (a : &cgmath::Point2 <S>, b : &cgmath::Point2 <S>)
  -> cgmath::Point2 <S>
{
  [ S::max (a.x, b.x),
    S::max (a.y, b.y)
  ].into()
}

/// Coordinate-wise min
pub fn point3_min <S : Scalar> (a : &cgmath::Point3 <S>, b : &cgmath::Point3 <S>)
  -> cgmath::Point3 <S>
{
  [ S::min (a.x, b.x),
    S::min (a.y, b.y),
    S::min (a.z, b.z)
  ].into()
}

/// Coordinate-wise max
pub fn point3_max <S : Scalar> (a : &cgmath::Point3 <S>, b : &cgmath::Point3 <S>)
  -> cgmath::Point3 <S>
{
  [ S::max (a.x, b.x),
    S::max (a.y, b.y),
    S::max (a.z, b.z)
  ].into()
}

/// Given a 2D point and a 2D line, returns the nearest point on the line to
/// the given point
pub fn project_2d_point_on_line <S : Scalar>
  (point : &cgmath::Point2 <S>, line : &Line2 <S>) -> cgmath::Point2 <S>
{
  use cgmath::{EuclideanSpace, InnerSpace};
  let dot_dir = line.direction.dot (point.to_vec() - line.base.to_vec());
  line.point (dot_dir)
}

/// Given a 3D point and a 3D line, returns the nearest point on the line to
/// the given point
pub fn project_3d_point_on_line <S : Scalar>
  (point : &cgmath::Point3 <S>, line : &Line3 <S>) -> cgmath::Point3 <S>
{
  use cgmath::{EuclideanSpace, InnerSpace};
  let dot_dir = line.direction.dot (point.to_vec() - line.base.to_vec());
  line.point (dot_dir)
}

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

impl <S : Scalar> Aabb1 <S> {
  /// Construct a new AABB with given min and max points.
  ///
  /// Debug panic if points are not min/max:
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb1::with_minmax ([1.0].into(), [0.0].into());  // panic!
  /// ```
  ///
  /// Debug panic if points are identical:
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb1::with_minmax ([0.0].into(), [0.0].into());  // panic!
  /// ```
  #[inline]
  pub fn with_minmax (min : cgmath::Point1 <S>, max : cgmath::Point1 <S>) -> Self {
    debug_assert_ne!(min, max);
    debug_assert_eq!(min.x, S::min (min.x, max.x));
    debug_assert_eq!(max.x, S::max (min.x, max.x));
    Aabb1 { min, max }
  }
  /// Construct a new AABB using the two given points to determine min/max.
  ///
  /// Panic if points are identical:
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb1::from_points ([0.0].into(), [0.0].into());  // panic!
  /// ```
  #[inline]
  pub fn from_points (a : cgmath::Point1 <S>, b : cgmath::Point1 <S>) -> Self {
    debug_assert_ne!(a, b);
    let min = [S::min (a.x, b.x)].into();
    let max = [S::max (a.x, b.x)].into();
    Aabb1 { min, max }
  }
  /// Construct the minimum AABB containing the given set of points.
  ///
  /// Debug panic if fewer than 2 points are given:
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb1::<f32>::containing (&[]);  // panic!
  /// ```
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb1::containing (&[[0.0].into()]);  // panic!
  /// ```
  #[inline]
  pub fn containing (points : &[cgmath::Point1 <S>]) -> Self {
    debug_assert!(points.len() >= 2);
    let mut min = cgmath::Point1::from ([S::min (points[0].x, points[1].x)]);
    let mut max = cgmath::Point1::from ([S::max (points[0].x, points[1].x)]);
    for point in points.iter().skip (2) {
      min.x = S::min (min.x, point.x);
      max.x = S::max (max.x, point.x);
    }
    Aabb1::with_minmax (min, max)
  }
  /// Create a new AABB that is the union of the two input AABBs
  #[inline]
  pub fn union (a : &Aabb1 <S>, b : &Aabb1 <S>) -> Self {
    Aabb1::with_minmax (
      [S::min (a.min().x, b.min().x)].into(),
      [S::max (a.max().x, b.max().x)].into())
  }
  #[inline]
  pub fn min (&self) -> &cgmath::Point1 <S> {
    &self.min
  }
  #[inline]
  pub fn max (&self) -> &cgmath::Point1 <S> {
    &self.max
  }
  #[inline]
  pub fn width (&self) -> S {
    self.max.x - self.min.x
  }
  #[inline]
  pub fn contains (&self, point : &cgmath::Point1 <S>) -> bool {
    self.min.x < point.x && point.x < self.max.x
  }
  /// Clamp a given point to the AABB interval.
  ///
  /// ```
  /// # use geometry_utils::*;
  /// let b = Aabb1::from_points ([-1.0].into(), [1.0].into());
  /// assert_eq!(b.clamp (&[-2.0].into()), [-1.0].into());
  /// assert_eq!(b.clamp (&[ 2.0].into()), [ 1.0].into());
  /// assert_eq!(b.clamp (&[ 0.0].into()), [ 0.0].into());
  /// ```
  pub fn clamp (&self, point : &cgmath::Point1 <S>) -> cgmath::Point1 <S> {
    [S::max (S::min (self.max.x, point.x), self.min.x)].into()
  }
  /// Generate a random point contained in the AABB
  ///
  /// ```
  /// # extern crate cgmath;
  /// # extern crate rand;
  /// # extern crate rand_xorshift;
  /// # extern crate geometry_utils;
  /// # use geometry_utils::*;
  /// # fn main () {
  /// use rand_xorshift;
  /// use rand::SeedableRng;
  /// // random sequence will be the same each time this is run
  /// let mut rng = rand_xorshift::XorShiftRng::from_seed ([
  ///   0x19, 0x3a, 0x67, 0x54,
  ///   0xa8, 0xa7, 0xd4, 0x69,
  ///   0x97, 0x83, 0x0e, 0x05,
  ///   0x11, 0x3b, 0xa7, 0xbb
  /// ]);
  /// let aabb = Aabb1::<f32>::with_minmax ([-10.0].into(), [ 10.0].into());
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// # }
  /// ```
  #[inline]
  pub fn rand_point <R> (&self, rng : &mut R) -> cgmath::Point1 <S> where
    S : rand::distributions::uniform::SampleUniform,
    R : rand::Rng
  {
    [ rng.gen_range (self.min.x..self.max.x) ].into()
  }
  #[inline]
  pub fn intersects (&self, other : &Aabb1 <S>) -> bool {
    intersect::discrete_aabb1_aabb1 (self, other)
  }
}

impl <S : Scalar> Aabb2 <S> {
  /// Construct a new AABB with given min and max points.
  ///
  /// Debug panic if points are not min/max:
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb2::with_minmax ([1.0, 1.0].into(), [0.0, 0.0].into());  // panic!
  /// ```
  ///
  /// Debug panic if points are identical:
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb2::with_minmax ([0.0, 0.0].into(), [0.0, 0.0].into());  // panic!
  /// ```
  #[inline]
  pub fn with_minmax (min : cgmath::Point2 <S>, max : cgmath::Point2 <S>) -> Self {
    debug_assert_ne!(min, max);
    debug_assert_eq!(min, point2_min (&min, &max));
    debug_assert_eq!(max, point2_max (&min, &max));
    Aabb2 { min, max }
  }
  /// Construct a new AABB using the two given points to determine min/max.
  ///
  /// Debug panic if points are identical:
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb2::from_points ([0.0, 0.0].into(), [0.0, 0.0].into());  // panic!
  /// ```
  #[inline]
  pub fn from_points (a : cgmath::Point2 <S>, b : cgmath::Point2 <S>) -> Self {
    debug_assert_ne!(a, b);
    let min = point2_min (&a, &b);
    let max = point2_max (&a, &b);
    Aabb2 { min, max }
  }
  /// Construct the minimum AABB containing the given set of points.
  ///
  /// Debug panic if fewer than 2 points are given:
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb2::<f32>::containing (&[]);  // panic!
  /// ```
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb2::containing (&[[0.0, 0.0].into()]);  // panic!
  /// ```
  #[inline]
  pub fn containing (points : &[cgmath::Point2 <S>]) -> Self {
    debug_assert!(points.len() >= 2);
    let mut min = point2_min (&points[0], &points[1]);
    let mut max = point2_max (&points[0], &points[1]);
    for point in points.iter().skip (2) {
      min = point2_min (&min, point);
      max = point2_max (&max, point);
    }
    Aabb2::with_minmax (min, max)
  }
  /// Create a new AABB that is the union of the two input AABBs
  #[inline]
  pub fn union (a : &Aabb2 <S>, b : &Aabb2 <S>) -> Self {
    Aabb2::with_minmax (
      point2_min (a.min(), b.min()),
      point2_max (a.max(), b.max())
    )
  }
  #[inline]
  pub fn min (&self) -> &cgmath::Point2 <S> {
    &self.min
  }
  #[inline]
  pub fn max (&self) -> &cgmath::Point2 <S> {
    &self.max
  }
  #[inline]
  pub fn width (&self) -> S {
    self.max.x - self.min.x
  }
  #[inline]
  pub fn height (&self) -> S {
    self.max.y - self.min.y
  }
  #[inline]
  pub fn contains (&self, point : &cgmath::Point2 <S>) -> bool {
    self.min.x < point.x && point.x < self.max.x &&
    self.min.y < point.y && point.y < self.max.y
  }
  /// Clamp a given point to the AABB.
  ///
  /// ```
  /// # use geometry_utils::*;
  /// let b = Aabb2::from_points ([-1.0, -1.0].into(), [1.0, 1.0].into());
  /// assert_eq!(b.clamp (&[-2.0, 0.0].into()), [-1.0, 0.0].into());
  /// assert_eq!(b.clamp (&[ 2.0, 2.0].into()), [ 1.0, 1.0].into());
  /// assert_eq!(b.clamp (&[-0.5, 0.5].into()), [-0.5, 0.5].into());
  /// ```
  pub fn clamp (&self, point : &cgmath::Point2 <S>) -> cgmath::Point2 <S> {
    [ S::max (S::min (self.max.x, point.x), self.min.x),
      S::max (S::min (self.max.y, point.y), self.min.y),
    ].into()
  }
  /// Generate a random point contained in the AABB
  ///
  /// ```
  /// # extern crate cgmath;
  /// # extern crate rand;
  /// # extern crate rand_xorshift;
  /// # extern crate geometry_utils;
  /// # use geometry_utils::*;
  /// # fn main () {
  /// use rand_xorshift;
  /// use rand::SeedableRng;
  /// // random sequence will be the same each time this is run
  /// let mut rng = rand_xorshift::XorShiftRng::from_seed ([
  ///   0x19, 0x3a, 0x67, 0x54,
  ///   0xa8, 0xa7, 0xd4, 0x69,
  ///   0x97, 0x83, 0x0e, 0x05,
  ///   0x11, 0x3b, 0xa7, 0xbb
  /// ]);
  /// let aabb = Aabb2::<f32>::with_minmax (
  ///   [-10.0, -10.0].into(),
  ///   [ 10.0,  10.0].into());
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// # }
  /// ```
  #[inline]
  pub fn rand_point <R> (&self, rng : &mut R) -> cgmath::Point2 <S> where
    S : rand::distributions::uniform::SampleUniform,
    R : rand::Rng
  {
    [ rng.gen_range (self.min.x..self.max.x),
      rng.gen_range (self.min.y..self.max.y)
    ].into()
  }
  #[inline]
  pub fn intersects (&self, other : &Aabb2 <S>) -> bool {
    intersect::discrete_aabb2_aabb2 (self, other)
  }
}

impl <S : Scalar> Aabb3 <S> {
  /// Construct a new AABB with given min and max points.
  ///
  /// Debug panic if points are not min/max:
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb3::with_minmax ([1.0, 1.0, 1.0].into(), [0.0, 0.0, 0.0].into());  // panic!
  /// ```
  ///
  /// Debug panic if points are identical:
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb3::with_minmax ([0.0, 0.0, 0.0].into(), [0.0, 0.0, 0.0].into());  // panic!
  /// ```
  #[inline]
  pub fn with_minmax (min : cgmath::Point3 <S>, max : cgmath::Point3 <S>) -> Self {
    debug_assert_ne!(min, max);
    debug_assert_eq!(min, point3_min (&min, &max));
    debug_assert_eq!(max, point3_max (&min, &max));
    Aabb3 { min, max }
  }
  /// Construct a new AABB using the two given points to determine min/max.
  ///
  /// Panic if points are identical:
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb3::from_points (
  ///   [0.0, 0.0, 0.0].into(),
  ///   [0.0, 0.0, 0.0].into());  // panic!
  /// ```
  #[inline]
  pub fn from_points (a : cgmath::Point3 <S>, b : cgmath::Point3 <S>) -> Self {
    debug_assert_ne!(a, b);
    let min = point3_min (&a, &b);
    let max = point3_max (&a, &b);
    Aabb3 { min, max }
  }
  /// Construct the minimum AABB containing the given set of points.
  ///
  /// Debug panic if fewer than 2 points are given:
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb3::<f32>::containing (&[]);  // panic!
  /// ```
  ///
  /// ```should_panic
  /// # use geometry_utils::*;
  /// let b = Aabb3::containing (&[[0.0, 0.0, 0.0].into()]);  // panic!
  /// ```
  #[inline]
  pub fn containing (points : &[cgmath::Point3 <S>]) -> Self {
    debug_assert!(points.len() >= 2);
    let mut min = point3_min (&points[0], &points[1]);
    let mut max = point3_max (&points[0], &points[1]);
    for point in points.iter().skip (2) {
      min = point3_min (&min, point);
      max = point3_max (&max, point);
    }
    Aabb3::with_minmax (min, max)
  }
  /// Create a new AABB that is the union of the two input AABBs
  #[inline]
  pub fn union (a : &Aabb3 <S>, b : &Aabb3 <S>) -> Self {
    Aabb3::with_minmax (
      point3_min (a.min(), b.min()),
      point3_max (a.max(), b.max())
    )
  }
  #[inline]
  pub fn min (&self) -> &cgmath::Point3 <S> {
    &self.min
  }
  #[inline]
  pub fn max (&self) -> &cgmath::Point3 <S> {
    &self.max
  }
  #[inline]
  pub fn width (&self) -> S {
    self.max.x - self.min.x
  }
  #[inline]
  pub fn height (&self) -> S {
    self.max.y - self.min.y
  }
  #[inline]
  pub fn depth (&self) -> S {
    self.max.z - self.min.z
  }
  #[inline]
  pub fn contains (&self, point : &cgmath::Point3 <S>) -> bool {
    self.min.x < point.x && point.x < self.max.x &&
    self.min.y < point.y && point.y < self.max.y &&
    self.min.z < point.z && point.z < self.max.z
  }
  /// Clamp a given point to the AABB.
  ///
  /// ```
  /// # use geometry_utils::*;
  /// let b = Aabb3::with_minmax ([-1.0, -1.0, -1.0].into(), [1.0, 1.0, 1.0].into());
  /// assert_eq!(b.clamp (&[-2.0, 0.0, 0.0].into()), [-1.0, 0.0, 0.0].into());
  /// assert_eq!(b.clamp (&[ 2.0, 2.0, 0.0].into()), [ 1.0, 1.0, 0.0].into());
  /// assert_eq!(b.clamp (&[-1.0, 2.0, 3.0].into()), [-1.0, 1.0, 1.0].into());
  /// assert_eq!(b.clamp (&[-0.5, 0.5, 0.0].into()), [-0.5, 0.5, 0.0].into());
  /// ```
  pub fn clamp (&self, point : &cgmath::Point3 <S>) -> cgmath::Point3 <S> {
    [ S::max (S::min (self.max.x, point.x), self.min.x),
      S::max (S::min (self.max.y, point.y), self.min.y),
      S::max (S::min (self.max.z, point.z), self.min.z)
    ].into()
  }
  /// Generate a random point contained in the AABB
  ///
  /// ```
  /// # extern crate cgmath;
  /// # extern crate rand;
  /// # extern crate rand_xorshift;
  /// # extern crate geometry_utils;
  /// # use geometry_utils::*;
  /// # fn main () {
  /// use rand_xorshift;
  /// use rand::SeedableRng;
  /// // random sequence will be the same each time this is run
  /// let mut rng = rand_xorshift::XorShiftRng::from_seed ([
  ///   0x19, 0x3a, 0x67, 0x54,
  ///   0xa8, 0xa7, 0xd4, 0x69,
  ///   0x97, 0x83, 0x0e, 0x05,
  ///   0x11, 0x3b, 0xa7, 0xbb
  /// ]);
  /// let aabb = Aabb3::<f32>::with_minmax (
  ///   [-10.0, -10.0, -10.0].into(),
  ///   [ 10.0,  10.0,  10.0].into());
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// # }
  /// ```
  #[inline]
  pub fn rand_point <R> (&self, rng : &mut R) -> cgmath::Point3 <S> where
    S : rand::distributions::uniform::SampleUniform,
    R : rand::Rng
  {
    [ rng.gen_range (self.min.x..self.max.x),
      rng.gen_range (self.min.y..self.max.y),
      rng.gen_range (self.min.z..self.max.z)
    ].into()
  }
  #[inline]
  pub fn intersects (&self, other : &Aabb3 <S>) -> bool {
    intersect::discrete_aabb3_aabb3 (self, other)
  }
}

impl <S : Scalar> Capsule3 <S> {
  pub fn aabb3 (&self) -> Aabb3 <S> {
    use cgmath::EuclideanSpace;
    use shape::Aabb;
    let shape_aabb = shape::Capsule {
      radius:      self.radius,
      half_height: self.half_height
    }.aabb();
    let center_vec = self.center.to_vec();
    let min        = shape_aabb.min() + center_vec;
    let max        = shape_aabb.max() + center_vec;
    Aabb3::with_minmax (min, max)
  }
  /// Return the (upper sphere, cylinder, lower sphere) making up this capsule
  pub fn decompose (&self)
    -> (Sphere3 <S>, Option <Cylinder3 <S>>, Sphere3 <S>)
  {
    let cylinder = if *self.half_height > S::zero() {
      Some (Cylinder3 {
        center:      self.center,
        radius:      self.radius,
        half_height: math::Positive::unchecked (*self.half_height)
      })
    } else {
      None
    };
    let upper_sphere = Sphere3 {
      center: self.center + (cgmath::Vector3::unit_z() * *self.half_height),
      radius: self.radius
    };
    let lower_sphere = Sphere3 {
      center: self.center - (cgmath::Vector3::unit_z() * *self.half_height),
      radius: self.radius
    };
    (upper_sphere, cylinder, lower_sphere)
  }
}

impl <S : Scalar> Cylinder3 <S> {
  pub fn aabb3 (&self) -> Aabb3 <S> {
    use cgmath::EuclideanSpace;
    use shape::Aabb;
    let shape_aabb = shape::Cylinder::unchecked (*self.radius, *self.half_height)
      .aabb();
    let center_vec = self.center.to_vec();
    let min        = shape_aabb.min() + center_vec;
    let max        = shape_aabb.max() + center_vec;
    Aabb3::with_minmax (min, max)
  }
}

impl <S : Scalar> Line2 <S> {
  /// Construct a new 2D line
  #[inline]
  pub fn new (base : cgmath::Point2 <S>, direction : math::Unit2 <S>) -> Self {
    Line2 { base, direction }
  }
  #[inline]
  pub fn point (&self, t : S) -> cgmath::Point2 <S> {
    self.base + *self.direction * t
  }
  #[inline]
  pub fn intersect_aabb (&self, aabb : &Aabb2 <S>)
    -> Option <((S, cgmath::Point2 <S>), (S, cgmath::Point2 <S>))>
  {
    intersect::continuous_line2_aabb2 (self, aabb)
  }
  #[inline]
  pub fn intersect_sphere (&self, sphere : &Sphere2 <S>)
    -> Option <((S, cgmath::Point2 <S>), (S, cgmath::Point2 <S>))>
  {
    intersect::continuous_line2_sphere2 (self, sphere)
  }
}
impl <S : Scalar> Default for Line2 <S> {
  fn default() -> Self {
    use cgmath::EuclideanSpace;
    Line2 {
      base:      cgmath::Point2::origin(),
      direction: math::Unit2::axis_y()
    }
  }
}

impl <S : Scalar> Line3 <S> {
  /// Consructs a new 3D line with given base point and direction vector
  #[inline]
  pub fn new (base : cgmath::Point3 <S>, direction : math::Unit3 <S>) -> Self {
    Line3 { base, direction }
  }
  #[inline]
  pub fn point (&self, t : S) -> cgmath::Point3 <S> {
    self.base + *self.direction * t
  }
  #[inline]
  pub fn intersect_plane (&self, plane : &Plane3 <S>)
    -> Option <(S, cgmath::Point3 <S>)>
  {
    intersect::continuous_line3_plane3 (self, plane)
  }
  #[inline]
  pub fn intersect_aabb (&self, aabb : &Aabb3 <S>)
    -> Option <((S, cgmath::Point3 <S>), (S, cgmath::Point3 <S>))>
  {
    intersect::continuous_line3_aabb3 (self, aabb)
  }
  #[inline]
  pub fn intersect_sphere (&self, sphere : &Sphere3 <S>)
    -> Option <((S, cgmath::Point3 <S>), (S, cgmath::Point3 <S>))>
  {
    intersect::continuous_line3_sphere3 (self, sphere)
  }
}
impl <S : Scalar> Default for Line3 <S> {
  fn default() -> Self {
    use cgmath::EuclideanSpace;
    Line3 {
      base:      cgmath::Point3::origin(),
      direction: math::Unit3::axis_z()
    }
  }
}

impl <S : Scalar> Plane3 <S> {
  /// Consructs a new 3D plane with given base point and normal vector
  #[inline]
  pub fn new (base : cgmath::Point3 <S>, normal : math::Unit3 <S>) -> Self {
    Plane3 { base, normal }
  }
}
impl <S : Scalar> Default for Plane3 <S> {
  fn default() -> Self {
    use cgmath::EuclideanSpace;
    Plane3 {
      base:   cgmath::Point3::origin(),
      normal: math::Unit3::axis_z()
    }
  }
}

impl <S : Scalar> Sphere2 <S> {
  /// Unit circle
  #[inline]
  pub fn unit() -> Self {
    use cgmath::EuclideanSpace;
    Sphere2 {
      center: cgmath::Point2::origin(),
      radius: math::Positive::one()
    }
  }
  /// Discrete intersection test with another sphere
  #[inline]
  pub fn intersects (&self, other : &Self) -> bool {
    intersect::discrete_sphere2_sphere2 (self, other)
  }
}

impl <S : Scalar> Sphere3 <S> {
  /// Unit sphere
  #[inline]
  pub fn unit() -> Self {
    use cgmath::EuclideanSpace;
    Sphere3 {
      center: cgmath::Point3::origin(),
      radius: math::Positive::one()
    }
  }
  /// Discrete intersection test with another sphere
  #[inline]
  pub fn intersects (&self, other : &Self) -> bool {
    intersect::discrete_sphere3_sphere3 (self, other)
  }
}

////////////////////////////////////////////////////////////////////////////////
//  tests                                                                     //
////////////////////////////////////////////////////////////////////////////////

#[cfg(test)]
mod tests {
  use cgmath::{assert_relative_eq, assert_ulps_eq};
  use super::*;

  #[test]
  fn test_project_2d_point_on_line() {
    use math::Unit2;
    let point : cgmath::Point2 <f64> = [2.0, 2.0].into();
    let line = Line2::<f64>::new ([0.0, 0.0].into(), Unit2::axis_x());
    assert_eq!(project_2d_point_on_line (&point, &line), [2.0, 0.0].into());
    let line = Line2::<f64>::new ([0.0, 0.0].into(), Unit2::axis_y());
    assert_eq!(project_2d_point_on_line (&point, &line), [0.0, 2.0].into());
    let point : cgmath::Point2 <f64> = [0.0, 1.0].into();
    let line = Line2::<f64>::new (
      [0.0, -1.0].into(), Unit2::normalize ([1.0, 1.0].into()));
    assert_relative_eq!(
      project_2d_point_on_line (&point, &line), [1.0, 0.0].into());
    // the answer should be the same for lines with equivalent definitions
    let point : cgmath::Point2 <f64> = [1.0, 3.0].into();
    let line_a = Line2::<f64>::new (
      [0.0, -1.0].into(), Unit2::normalize ([2.0, 1.0].into()));
    let line_b = Line2::<f64>::new (
      [2.0, 0.0].into(),  Unit2::normalize ([2.0, 1.0].into()));
    assert_relative_eq!(
      project_2d_point_on_line (&point, &line_a),
      project_2d_point_on_line (&point, &line_b));
    let line_a = Line2::<f64>::new (
      [0.0, 0.0].into(),   Unit2::normalize ([1.0, 1.0].into()));
    let line_b = Line2::<f64>::new (
      [-2.0, -2.0].into(), Unit2::normalize ([1.0, 1.0].into()));
    assert_ulps_eq!(
      project_2d_point_on_line (&point, &line_a),
      project_2d_point_on_line (&point, &line_b)
    );
    assert_relative_eq!(
      project_2d_point_on_line (&point, &line_a),
      [2.0, 2.0].into());
  }

  #[test]
  fn test_project_3d_point_on_line() {
    use math::Unit3;
    // all the tests from 2d projection with 0.0 for the Z component
    let point : cgmath::Point3 <f64> = [2.0, 2.0, 0.0].into();
    let line = Line3::<f64>::new ([0.0, 0.0, 0.0].into(), Unit3::axis_x());
    assert_eq!(project_3d_point_on_line (&point, &line), [2.0, 0.0, 0.0].into());
    let line = Line3::<f64>::new ([0.0, 0.0, 0.0].into(), Unit3::axis_y());
    assert_eq!(project_3d_point_on_line (&point, &line), [0.0, 2.0, 0.0].into());
    let point : cgmath::Point3 <f64> = [0.0, 1.0, 0.0].into();
    let line = Line3::<f64>::new (
      [0.0, -1.0, 0.0].into(), Unit3::normalize ([1.0, 1.0, 0.0].into()));
    assert_relative_eq!(
      project_3d_point_on_line (&point, &line), [1.0, 0.0, 0.0].into());
    // the answer should be the same for lines with equivalent definitions
    let point : cgmath::Point3 <f64> = [1.0, 3.0, 0.0].into();
    let line_a = Line3::<f64>::new (
      [0.0, -1.0, 0.0].into(), Unit3::normalize ([2.0, 1.0, 0.0].into()));
    let line_b = Line3::<f64>::new (
      [2.0, 0.0, 0.0].into(),  Unit3::normalize ([2.0, 1.0, 0.0].into()));
    assert_relative_eq!(
      project_3d_point_on_line (&point, &line_a),
      project_3d_point_on_line (&point, &line_b));
    let line_a = Line3::<f64>::new (
      [0.0, 0.0, 0.0].into(), Unit3::normalize ([1.0, 1.0, 0.0].into()));
    let line_b = Line3::<f64>::new (
      [2.0, 2.0, 0.0].into(), Unit3::normalize ([1.0, 1.0, 0.0].into()));
    assert_relative_eq!(
      project_3d_point_on_line (&point, &line_a),
      project_3d_point_on_line (&point, &line_b));
    assert_relative_eq!(
      project_3d_point_on_line (&point, &line_a),
      [2.0, 2.0, 0.0].into());
    // more tests
    let point : cgmath::Point3 <f64> = [0.0, 0.0, 2.0].into();
    let line_a = Line3::<f64>::new (
      [-4.0, -4.0, -4.0].into(), Unit3::normalize ([1.0, 1.0, 1.0].into()));
    let line_b = Line3::<f64>::new (
      [4.0, 4.0, 4.0].into(),    Unit3::normalize ([1.0, 1.0, 1.0].into()));
    assert_relative_eq!(
      project_3d_point_on_line (&point, &line_a),
      project_3d_point_on_line (&point, &line_b),
      epsilon = 0.00000000000001
    );
    assert_relative_eq!(
      project_3d_point_on_line (&point, &line_a),
      [2.0/3.0, 2.0/3.0, 2.0/3.0].into(),
      epsilon = 0.00000000000001
    );
  }

} // end tests
