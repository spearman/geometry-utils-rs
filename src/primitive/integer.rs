//! Integer Aabb primitives.
//!
//! Note that unlike Aabbs over Scalars, Aabbs over Integers are *closed* (they
//! include their boundary points). This means that there can be degenerate
//! Aabbs containing a single point (when `min = max`), or line or plane in
//! higher dimensions.
// TODO: more types? ortho lines, planes?

use cgmath;
#[cfg(feature = "derive_serdes")]
use serde::{Deserialize, Serialize};

use crate::intersect;
use crate::math::Integer;

/// 1D axis-aligned bounding box (interval)
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Aabb1 <I : Integer> {
  min : cgmath::Point1 <I>,
  max : cgmath::Point1 <I>
}

/// 2D axis-aligned bounding box
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Aabb2 <I : Integer> {
  min : cgmath::Point2 <I>,
  max : cgmath::Point2 <I>
}

/// 3D axis-aligned bounding box.
///
/// See also `shape::Aabb` trait for primitive and shape types that can compute
/// a 3D AABB.
#[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Aabb3 <I : Integer> {
  min : cgmath::Point3 <I>,
  max : cgmath::Point3 <I>
}

/// Coordinate-wise min
pub fn point2_min <I : Integer> (a : &cgmath::Point2 <I>, b : &cgmath::Point2 <I>)
  -> cgmath::Point2 <I>
{
  [ I::min (a.x, b.x),
    I::min (a.y, b.y)
  ].into()
}
/// Coordinate-wise max
pub fn point2_max <I : Integer> (a : &cgmath::Point2 <I>, b : &cgmath::Point2 <I>)
  -> cgmath::Point2 <I>
{
  [ I::max (a.x, b.x),
    I::max (a.y, b.y)
  ].into()
}
/// Minimum representable point
pub fn point2_min_value <I : Integer> () -> cgmath::Point2 <I> {
  [ I::min_value(), I::min_value() ].into()
}
/// Maximum representable point
pub fn point2_max_value <I : Integer> () -> cgmath::Point2 <I> {
  [ I::max_value(), I::max_value() ].into()
}

/// Coordinate-wise min
pub fn point3_min <I : Integer> (a : &cgmath::Point3 <I>, b : &cgmath::Point3 <I>)
  -> cgmath::Point3 <I>
{
  [ I::min (a.x, b.x),
    I::min (a.y, b.y),
    I::min (a.z, b.z)
  ].into()
}
/// Coordinate-wise max
pub fn point3_max <I : Integer> (a : &cgmath::Point3 <I>, b : &cgmath::Point3 <I>)
  -> cgmath::Point3 <I>
{
  [ I::max (a.x, b.x),
    I::max (a.y, b.y),
    I::max (a.z, b.z)
  ].into()
}
/// Minimum representable point
pub fn point3_min_value <I : Integer> () -> cgmath::Point3 <I> {
  [ I::min_value(), I::min_value(), I::min_value() ].into()
}
/// Maximum representable point
pub fn point3_max_value <I : Integer> () -> cgmath::Point3 <I> {
  [ I::max_value(), I::max_value(), I::max_value() ].into()
}

impl <I : Integer> Aabb1 <I> {
  /// Construct a new AABB with given min and max points.
  ///
  /// Debug panic if points are not min/max:
  ///
  /// ```should_panic
  /// # use geometry_utils::integer::*;
  /// let b = Aabb1::with_minmax ([1].into(), [0].into());  // panic!
  /// ```
  ///
  /// Identical points are allowed:
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb1::with_minmax ([0].into(), [0].into());  // ok
  /// ```
  #[inline]
  pub fn with_minmax (min : cgmath::Point1 <I>, max : cgmath::Point1 <I>) -> Self {
    debug_assert_eq!(min.x, I::min (min.x, max.x));
    debug_assert_eq!(max.x, I::max (min.x, max.x));
    Aabb1 { min, max }
  }
  /// Construct a new AABB using the two given points to determine min/max.
  ///
  /// Identical points are allowed:
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb1::from_points ([0].into(), [0].into());  // ok!
  /// ```
  #[inline]
  pub fn from_points (a : cgmath::Point1 <I>, b : cgmath::Point1 <I>) -> Self {
    let min = [I::min (a.x, b.x)].into();
    let max = [I::max (a.x, b.x)].into();
    Aabb1 { min, max }
  }
  /// Construct the minimum AABB containing the given set of points.
  ///
  /// Identical points are allowed:
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb1::containing (&[[0].into(), [0].into()]);  // ok!
  /// ```
  ///
  /// A single point is allowed:
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb1::containing (&[[0].into()]);  // ok!
  /// ```
  ///
  /// Debug panic if no points are given:
  ///
  /// ```should_panic
  /// # use geometry_utils::integer::*;
  /// let b = Aabb1::<i32>::containing (&[]);  // panic!
  /// ```
  #[inline]
  pub fn containing (points : &[cgmath::Point1 <I>]) -> Self {
    debug_assert!(points.len() > 0);
    let mut min = cgmath::Point1::from ([I::max_value()]);
    let mut max = cgmath::Point1::from ([I::min_value()]);
    for point in points {
      if point.x < min.x {
        min.x = point.x;
      }
      if point.x > max.x {
        max.x = point.x;
      }
    }
    Aabb1::with_minmax (min, max)
  }
  /// Create a new AABB that is the union of the two input AABBs
  #[inline]
  pub fn union (a : &Aabb1 <I>, b : &Aabb1 <I>) -> Self {
    Aabb1::with_minmax (
      [I::min (a.min().x, b.min().x)].into(),
      [I::max (a.max().x, b.max().x)].into())
  }
  #[inline]
  pub fn min (&self) -> &cgmath::Point1 <I> {
    &self.min
  }
  #[inline]
  pub fn max (&self) -> &cgmath::Point1 <I> {
    &self.max
  }
  #[inline]
  pub fn width (&self) -> I {
    self.max.x - self.min.x
  }
  #[inline]
  pub fn contains (&self, point : &cgmath::Point1 <I>) -> bool {
    self.min.x <= point.x && point.x <= self.max.x
  }
  /// Clamp a given point to the AABB interval.
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb1::from_points ([-1].into(), [1].into());
  /// assert_eq!(b.clamp (&[-2].into()), [-1].into());
  /// assert_eq!(b.clamp (&[ 2].into()), [ 1].into());
  /// assert_eq!(b.clamp (&[ 0].into()), [ 0].into());
  /// ```
  pub fn clamp (&self, point : &cgmath::Point1 <I>) -> cgmath::Point1 <I> {
    [I::max (I::min (self.max.x, point.x), self.min.x)].into()
  }
  /// Generate a random point contained in the AABB
  ///
  /// ```
  /// # extern crate cgmath;
  /// # extern crate rand;
  /// # extern crate rand_xorshift;
  /// # extern crate geometry_utils;
  /// # use geometry_utils::integer::*;
  /// # fn main () {
  /// use rand_xorshift;
  /// use rand::SeedableRng;
  /// // random sequence will be the same each time this is run
  /// let mut rng = rand_xorshift::XorShiftRng::from_seed ([
  ///   0x19, 0x3a, 0x67, 0x54,
  ///   0xa8, 0xa7, 0xd4, 0x69,
  ///   0x97, 0x83, 0x0e, 0x05,
  ///   0x11, 0x3b, 0xa7, 0xbb
  /// ]);
  /// let aabb = Aabb1::<i32>::with_minmax ([-10].into(), [ 10].into());
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// # }
  /// ```
  #[inline]
  pub fn rand_point <R> (&self, rng : &mut R) -> cgmath::Point1 <I> where
    I : rand::distributions::uniform::SampleUniform,
    R : rand::Rng
  {
    // NB: integer aabbs include their max points so we increase the max by 1
    [ rng.gen_range (self.min.x..self.max.x+I::one()) ].into()
  }
  #[inline]
  pub fn intersects (&self, other : &Aabb1 <I>) -> bool {
    intersect::integer::discrete_aabb1_aabb1 (self, other)
  }
}

impl <I : Integer> Aabb2 <I> {
  /// Construct a new AABB with given min and max points.
  ///
  /// Debug panic if points are not min/max:
  ///
  /// ```should_panic
  /// # use geometry_utils::integer::*;
  /// let b = Aabb2::with_minmax ([1, 1].into(), [0, 0].into());  // panic!
  /// ```
  ///
  /// Identical points are allowed:
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb2::with_minmax ([0, 0].into(), [0, 0].into());  // ok
  /// ```
  #[inline]
  pub fn with_minmax (min : cgmath::Point2 <I>, max : cgmath::Point2 <I>) -> Self {
    debug_assert_eq!(min, point2_min (&min, &max));
    debug_assert_eq!(max, point2_max (&min, &max));
    Aabb2 { min, max }
  }
  /// Construct a new AABB using the two given points to determine min/max.
  ///
  /// Identical points are allowed:
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb2::from_points ([0, 0].into(), [0, 0].into());  // ok
  /// ```
  #[inline]
  pub fn from_points (a : cgmath::Point2 <I>, b : cgmath::Point2 <I>) -> Self {
    let min = point2_min (&a, &b);
    let max = point2_max (&a, &b);
    Aabb2 { min, max }
  }
  /// Construct the minimum AABB containing the given set of points.
  ///
  /// Identical points are allowed:
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb2::containing (&[[0, 0].into(), [0, 0].into()]);  // ok!
  /// ```
  ///
  /// A single point is allowed:
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb2::containing (&[[0, 0].into()]);  // ok!
  /// ```
  ///
  /// Debug panic if no points are given:
  ///
  /// ```should_panic
  /// # use geometry_utils::integer::*;
  /// let b = Aabb2::<i32>::containing (&[]);  // panic!
  /// ```
  #[inline]
  pub fn containing (points : &[cgmath::Point2 <I>]) -> Self {
    debug_assert!(points.len() > 0);
    let mut min = point2_max_value();
    let mut max = point2_min_value();
    for point in points {
      min = point2_min (&min, point);
      max = point2_max (&max, point);
    }
    Aabb2::with_minmax (min, max)
  }
  /// Create a new AABB that is the union of the two input AABBs
  #[inline]
  pub fn union (a : &Aabb2 <I>, b : &Aabb2 <I>) -> Self {
    Aabb2::with_minmax (
      point2_min (a.min(), b.min()),
      point2_max (a.max(), b.max())
    )
  }
  #[inline]
  pub fn min (&self) -> &cgmath::Point2 <I> {
    &self.min
  }
  #[inline]
  pub fn max (&self) -> &cgmath::Point2 <I> {
    &self.max
  }
  #[inline]
  pub fn width (&self) -> I {
    self.max.x - self.min.x
  }
  #[inline]
  pub fn height (&self) -> I {
    self.max.y - self.min.y
  }
  #[inline]
  pub fn contains (&self, point : &cgmath::Point2 <I>) -> bool {
    self.min.x <= point.x && point.x <= self.max.x &&
    self.min.y <= point.y && point.y <= self.max.y
  }
  /// Clamp a given point to the AABB.
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb2::from_points ([-1, -1].into(), [1, 1].into());
  /// assert_eq!(b.clamp (&[-2, 0].into()), [-1, 0].into());
  /// assert_eq!(b.clamp (&[ 2, 2].into()), [ 1, 1].into());
  /// assert_eq!(b.clamp (&[ 0, 0].into()), [ 0, 0].into());
  /// ```
  pub fn clamp (&self, point : &cgmath::Point2 <I>) -> cgmath::Point2 <I> {
    [ I::max (I::min (self.max.x, point.x), self.min.x),
      I::max (I::min (self.max.y, point.y), self.min.y),
    ].into()
  }
  /// Generate a random point contained in the AABB
  ///
  /// ```
  /// # extern crate cgmath;
  /// # extern crate rand;
  /// # extern crate rand_xorshift;
  /// # extern crate geometry_utils;
  /// # use geometry_utils::integer::*;
  /// # fn main () {
  /// use rand_xorshift;
  /// use rand::SeedableRng;
  /// // random sequence will be the same each time this is run
  /// let mut rng = rand_xorshift::XorShiftRng::from_seed ([
  ///   0x19, 0x3a, 0x67, 0x54,
  ///   0xa8, 0xa7, 0xd4, 0x69,
  ///   0x97, 0x83, 0x0e, 0x05,
  ///   0x11, 0x3b, 0xa7, 0xbb
  /// ]);
  /// let aabb = Aabb2::<i32>::with_minmax (
  ///   [-10, -10].into(),
  ///   [ 10,  10].into());
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// # }
  /// ```
  #[inline]
  pub fn rand_point <R> (&self, rng : &mut R) -> cgmath::Point2 <I> where
    I : rand::distributions::uniform::SampleUniform,
    R : rand::Rng
  {
    // NB: integer aabbs include their max points so we increase the max by 1
    [ rng.gen_range (self.min.x..self.max.x+I::one()),
      rng.gen_range (self.min.y..self.max.y+I::one())
    ].into()
  }
  #[inline]
  pub fn intersects (&self, other : &Aabb2 <I>) -> bool {
    intersect::integer::discrete_aabb2_aabb2 (self, other)
  }
}

impl <I : Integer> Aabb3 <I> {
  /// Construct a new AABB with given min and max points.
  ///
  /// Debug panic if points are not min/max:
  ///
  /// ```should_panic
  /// # use geometry_utils::integer::*;
  /// let b = Aabb3::with_minmax ([1, 1, 1].into(), [0, 0, 0].into());  // panic!
  /// ```
  ///
  /// Identical points are allowed:
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb3::with_minmax ([0, 0, 0].into(), [0, 0, 0].into());  // ok
  /// ```
  #[inline]
  pub fn with_minmax (min : cgmath::Point3 <I>, max : cgmath::Point3 <I>) -> Self {
    debug_assert_eq!(min, point3_min (&min, &max));
    debug_assert_eq!(max, point3_max (&min, &max));
    Aabb3 { min, max }
  }
  /// Construct a new AABB using the two given points to determine min/max.
  ///
  /// Identical points are allowed:
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb3::from_points ([0, 0, 0].into(), [0, 0, 0].into());  // ok
  /// ```
  #[inline]
  pub fn from_points (a : cgmath::Point3 <I>, b : cgmath::Point3 <I>) -> Self {
    let min = point3_min (&a, &b);
    let max = point3_max (&a, &b);
    Aabb3 { min, max }
  }
  /// Construct the minimum AABB containing the given set of points.
  ///
  /// Identical points are allowed:
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb3::containing (&[[0, 0, 0].into(), [0, 0, 0].into()]);  // ok!
  /// ```
  ///
  /// A single point is allowed:
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb3::containing (&[[0, 0, 0].into()]);  // ok!
  /// ```
  ///
  /// Debug panic if no points are given:
  ///
  /// ```should_panic
  /// # use geometry_utils::integer::*;
  /// let b = Aabb3::<i32>::containing (&[]);  // panic!
  /// ```
  #[inline]
  pub fn containing (points : &[cgmath::Point3 <I>]) -> Self {
    debug_assert!(points.len() > 0);
    let mut min = point3_max_value();
    let mut max = point3_min_value();
    for point in points {
      min = point3_min (&min, point);
      max = point3_max (&max, point);
    }
    Aabb3::with_minmax (min, max)
  }
  /// Create a new AABB that is the union of the two input AABBs
  #[inline]
  pub fn union (a : &Aabb3 <I>, b : &Aabb3 <I>) -> Self {
    Aabb3::with_minmax (
      point3_min (a.min(), b.min()),
      point3_max (a.max(), b.max())
    )
  }
  #[inline]
  pub fn min (&self) -> &cgmath::Point3 <I> {
    &self.min
  }
  #[inline]
  pub fn max (&self) -> &cgmath::Point3 <I> {
    &self.max
  }
  #[inline]
  pub fn width (&self) -> I {
    self.max.x - self.min.x
  }
  #[inline]
  pub fn height (&self) -> I {
    self.max.y - self.min.y
  }
  #[inline]
  pub fn depth (&self) -> I {
    self.max.z - self.min.z
  }
  #[inline]
  pub fn contains (&self, point : &cgmath::Point3 <I>) -> bool {
    self.min.x <= point.x && point.x <= self.max.x &&
    self.min.y <= point.y && point.y <= self.max.y &&
    self.min.z <= point.z && point.z <= self.max.z
  }
  /// Clamp a given point to the AABB.
  ///
  /// ```
  /// # use geometry_utils::integer::*;
  /// let b = Aabb3::with_minmax ([-1, -1, -1].into(), [1, 1, 1].into());
  /// assert_eq!(b.clamp (&[-2, 0, 0].into()), [-1, 0, 0].into());
  /// assert_eq!(b.clamp (&[ 2, 2, 0].into()), [ 1, 1, 0].into());
  /// assert_eq!(b.clamp (&[-1, 2, 3].into()), [-1, 1, 1].into());
  /// assert_eq!(b.clamp (&[ 0, 0, 0].into()), [ 0, 0, 0].into());
  /// ```
  pub fn clamp (&self, point : &cgmath::Point3 <I>) -> cgmath::Point3 <I> {
    [ I::max (I::min (self.max.x, point.x), self.min.x),
      I::max (I::min (self.max.y, point.y), self.min.y),
      I::max (I::min (self.max.z, point.z), self.min.z)
    ].into()
  }
  /// Generate a random point contained in the AABB
  ///
  /// ```
  /// # extern crate cgmath;
  /// # extern crate rand;
  /// # extern crate rand_xorshift;
  /// # extern crate geometry_utils;
  /// # use geometry_utils::integer::*;
  /// # fn main () {
  /// use rand::SeedableRng;
  /// // random sequence will be the same each time this is run
  /// let mut rng = rand_xorshift::XorShiftRng::from_seed ([
  ///   0x19, 0x3a, 0x67, 0x54,
  ///   0xa8, 0xa7, 0xd4, 0x69,
  ///   0x97, 0x83, 0x0e, 0x05,
  ///   0x11, 0x3b, 0xa7, 0xbb
  /// ]);
  /// let aabb = Aabb3::<i32>::with_minmax (
  ///   [-10, -10, -10].into(),
  ///   [ 10,  10,  10].into());
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// let point = aabb.rand_point (&mut rng);
  /// assert!(aabb.contains (&point));
  /// # }
  /// ```
  #[inline]
  pub fn rand_point <R> (&self, rng : &mut R) -> cgmath::Point3 <I> where
    I : rand::distributions::uniform::SampleUniform,
    R : rand::Rng
  {
    // NB: integer aabbs include their max points so we increase the max by 1
    [ rng.gen_range (self.min.x..self.max.x+I::one()),
      rng.gen_range (self.min.y..self.max.y+I::one()),
      rng.gen_range (self.min.z..self.max.z+I::one())
    ].into()
  }
  #[inline]
  pub fn intersects (&self, other : &Aabb3 <I>) -> bool {
    intersect::integer::discrete_aabb3_aabb3 (self, other)
  }
}
