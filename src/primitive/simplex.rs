pub use self::simplex2::Simplex2;
pub use self::simplex3::Simplex3;

pub mod simplex2 {
  use cgmath;
  use crate::primitive::*;
  /// A $n<3$-simplex in 2-dimensional space.
  ///
  /// Individual simplex variants should fail to construct in debug builds when
  /// points are degenerate.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone,Debug,PartialEq)]
  pub enum Simplex2 <S : Scalar> {
    Point    (Point    <S>),
    Segment  (Segment  <S>),
    Triangle (Triangle <S>)
  }

  /// A 0-simplex or point in 2D space
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone,Debug,PartialEq)]
  pub struct Point <S : Scalar> {
    a : cgmath::Point2 <S>
  }

  /// A 1-simplex or line segment in 2D space.
  ///
  /// Creation methods should fail with a debug assertion if the points are
  /// identical.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone,Debug,PartialEq)]
  pub struct Segment <S : Scalar> {
    a : cgmath::Point2 <S>,
    b : cgmath::Point2 <S>
  }

  /// A 2-simplex or triangle in 2D space
  ///
  /// Creation methods should fail with a debug assertion if the points are
  /// colinear.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone,Debug,PartialEq)]
  pub struct Triangle <S : Scalar> {
    a : cgmath::Point2 <S>,
    b : cgmath::Point2 <S>,
    c : cgmath::Point2 <S>
  }

  impl <S : Scalar> Point <S> {
    pub fn new (a : cgmath::Point2 <S>) -> Self {
      Point { a }
    }
  }
  impl <S : Scalar> Segment <S> {
    /// Panics if the points are identical:
    ///
    /// ```should_panic
    /// # use geometry_utils::simplex2::Segment;
    /// let s = Segment::new ([0.0, 0.0].into(), [0.0, 0.0].into());
    /// ```
    pub fn new (
      a : cgmath::Point2 <S>,
      b : cgmath::Point2 <S>
    ) -> Self {
      assert_ne!(a, b);
      Segment { a, b }
    }
    /// Debug panic if the points are identical:
    ///
    /// ```should_panic
    /// # use geometry_utils::simplex2::Segment;
    /// let s = Segment::unchecked ([0.0, 0.0].into(), [0.0, 0.0].into());
    /// ```
    pub fn unchecked (
      a : cgmath::Point2 <S>,
      b : cgmath::Point2 <S>
    ) -> Self {
      debug_assert_ne!(a, b);
      Segment { a, b }
    }
    #[inline]
    pub fn point_a (&self) -> &cgmath::Point2 <S> {
      &self.a
    }
    #[inline]
    pub fn point_b (&self) -> &cgmath::Point2 <S> {
      &self.b
    }
    #[inline]
    pub fn length (&self) -> S {
      use cgmath::InnerSpace;
      self.vector().magnitude()
    }
    #[inline]
    pub fn length2 (&self) -> S {
      use cgmath::InnerSpace;
      self.vector().magnitude2()
    }
    /// Returns `point_b() - point_a()`
    #[inline]
    pub fn vector (&self) -> cgmath::Vector2 <S> {
      self.b - self.a
    }
    #[inline]
    pub fn aabb2 (&self) -> Aabb2 <S> {
      Aabb2::from_points (self.a, self.b)
    }
    #[inline]
    pub fn line2 (&self) -> Line2 <S> {
      Line2::new (self.a, math::Unit2::normalize (self.vector()))
    }
    #[inline]
    pub fn intersect_aabb (&self, aabb : &Aabb2 <S>)
      -> Option <((S, cgmath::Point2 <S>), (S, cgmath::Point2 <S>))>
    {
      intersect::continuous_segment2_aabb2 (self, aabb)
    }
    #[inline]
    pub fn intersect_sphere (&self, sphere : &Sphere2 <S>)
      -> Option <((S, cgmath::Point2 <S>), (S, cgmath::Point2 <S>))>
    {
      intersect::continuous_segment2_sphere2 (self, sphere)
    }
  }
  impl <S : Scalar> Default for Segment <S> {
    /// A default simplex is arbitrarily chosen to be the simplex from -1.0 to 1.0
    /// lying on the X axis:
    ///
    /// ```
    /// # use geometry_utils::simplex2::Segment;
    /// assert_eq!(
    ///   Segment::default(),
    ///   Segment::new ([-1.0, 0.0].into(), [1.0, 0.0].into())
    /// );
    /// ```
    fn default() -> Self {
      Segment {
        a: [-S::one(), S::zero()].into(),
        b: [ S::one(), S::zero()].into()
      }
    }
  }

  impl <S : Scalar> Triangle <S> {
    /// Panics if the points are colinear:
    ///
    /// ```should_panic
    /// # use geometry_utils::simplex2::Triangle;
    /// let s = Triangle::new (
    ///   [-1.0, -1.0].into(),
    ///   [ 0.0,  0.0].into(),
    ///   [ 1.0,  1.0].into());
    /// ```
    pub fn new (
      a : cgmath::Point2 <S>,
      b : cgmath::Point2 <S>,
      c : cgmath::Point2 <S>
    ) -> Self {
      debug_assert_ne!(a, b);
      debug_assert_ne!(a, c);
      debug_assert_ne!(b, c);
      assert!(!colinear_2d (&a, &b, &c));
      Triangle { a, b, c }
    }
    /// Debug panic if the points are colinear:
    ///
    /// ```should_panic
    /// # use geometry_utils::simplex2::Triangle;
    /// let s = Triangle::unchecked (
    ///   [-1.0, -1.0].into(),
    ///   [ 0.0,  0.0].into(),
    ///   [ 1.0,  1.0].into());
    /// ```
    pub fn unchecked (
      a : cgmath::Point2 <S>,
      b : cgmath::Point2 <S>,
      c : cgmath::Point2 <S>
    ) -> Self {
      debug_assert_ne!(a, b);
      debug_assert_ne!(a, c);
      debug_assert_ne!(b, c);
      debug_assert!(!colinear_2d (&a, &b, &c));
      Triangle { a, b, c }
    }
    #[inline]
    pub fn point_a (&self) -> &cgmath::Point2 <S> {
      &self.a
    }
    #[inline]
    pub fn point_b (&self) -> &cgmath::Point2 <S> {
      &self.b
    }
    #[inline]
    pub fn point_c (&self) -> &cgmath::Point2 <S> {
      &self.c
    }
  }
  impl <S : Scalar> Default for Triangle <S> {
    /// A default triangle is arbitrarily chosen to be the equilateral triangle
    /// with point at 1.0 on the Y axis:
    ///
    /// ```
    /// # #[macro_use] extern crate cgmath;
    /// # extern crate geometry_utils;
    /// # use geometry_utils::simplex2::Triangle;
    /// use std::f64::consts::FRAC_1_SQRT_2;
    /// use cgmath::EuclideanSpace;
    /// let s = Triangle::default();
    /// let t = Triangle::new (
    ///   [           0.0,            1.0].into(),
    ///   [-FRAC_1_SQRT_2, -FRAC_1_SQRT_2].into(),
    ///   [ FRAC_1_SQRT_2, -FRAC_1_SQRT_2].into()
    /// );
    /// assert_relative_eq!(s.point_a().to_vec(), t.point_a().to_vec());
    /// assert_relative_eq!(s.point_b().to_vec(), t.point_b().to_vec());
    /// assert_relative_eq!(s.point_c().to_vec(), t.point_c().to_vec());
    /// ```
    fn default() -> Self {
      let frac_1_sqrt_2 = S::one() / (S::one() + S::one()).sqrt();
      Triangle {
        a: [     S::zero(),       S::one()].into(),
        b: [-frac_1_sqrt_2, -frac_1_sqrt_2].into(),
        c: [ frac_1_sqrt_2, -frac_1_sqrt_2].into()
      }
    }
  }
}

pub mod simplex3 {
  use cgmath;
  use crate::primitive::*;
  /// A $n<4$-simplex in 3-dimensional space.
  ///
  /// Individual simplex variants should fail to construct in debug builds when
  /// points are degenerate.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone,Debug,PartialEq)]
  pub enum Simplex3 <S : Scalar> {
    Point       (Point       <S>),
    Segment     (Segment     <S>),
    Triangle    (Triangle    <S>),
    Tetrahedron (Tetrahedron <S>)
  }

  /// A 0-simplex or point in 3D space
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone,Debug,PartialEq)]
  pub struct Point <S : Scalar> {
    a : cgmath::Point3 <S>
  }

  /// A 1-simplex or line segment in 3D space.
  ///
  /// Creation methods should fail with a debug assertion if the points are
  /// identical.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone,Debug,PartialEq)]
  pub struct Segment <S : Scalar> {
    a : cgmath::Point3 <S>,
    b : cgmath::Point3 <S>
  }

  /// A 2-simplex or triangle in 3D space
  ///
  /// Creation methods should fail with a debug assertion if the points are
  /// colinear.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone,Debug,PartialEq)]
  pub struct Triangle <S : Scalar> {
    a : cgmath::Point3 <S>,
    b : cgmath::Point3 <S>,
    c : cgmath::Point3 <S>
  }

  /// A 3-simplex or tetrahedron in 3D space
  ///
  /// Creation methods will fail with a debug assertion if the points are
  /// coplanar.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone,Debug,PartialEq)]
  pub struct Tetrahedron <S : Scalar> {
    a : cgmath::Point3 <S>,
    b : cgmath::Point3 <S>,
    c : cgmath::Point3 <S>,
    d : cgmath::Point3 <S>
  }
  impl <S : Scalar> Point <S> {
    pub fn new (a : cgmath::Point3 <S>) -> Self {
      Point { a }
    }
  }
  impl <S : Scalar> Segment <S> {
    /// Panics if the points are identical:
    ///
    /// ```should_panic
    /// # use geometry_utils::simplex3::Segment;
    /// let s = Segment::new ([0.0, 0.0, 1.0].into(), [0.0, 0.0, 1.0].into());
    /// ```
    pub fn new (
      a : cgmath::Point3 <S>,
      b : cgmath::Point3 <S>
    ) -> Self {
      assert_ne!(a, b);
      Segment { a, b }
    }
    /// Debug panic if the points are identical:
    ///
    /// ```should_panic
    /// # use geometry_utils::simplex3::Segment;
    /// let s = Segment::unchecked ([0.0, 0.0, 1.0].into(), [0.0, 0.0, 1.0].into());
    /// ```
    pub fn unchecked (
      a : cgmath::Point3 <S>,
      b : cgmath::Point3 <S>
    ) -> Self {
      debug_assert_ne!(a, b);
      Segment { a, b }
    }
    #[inline]
    pub fn point_a (&self) -> &cgmath::Point3 <S> {
      &self.a
    }
    #[inline]
    pub fn point_b (&self) -> &cgmath::Point3 <S> {
      &self.b
    }
    #[inline]
    pub fn length (&self) -> S {
      use cgmath::InnerSpace;
      self.vector().magnitude()
    }
    #[inline]
    pub fn length2 (&self) -> S {
      use cgmath::InnerSpace;
      self.vector().magnitude2()
    }
    /// Returns `point_b() - point_a()`
    #[inline]
    pub fn vector (&self) -> cgmath::Vector3 <S> {
      self.b - self.a
    }
    #[inline]
    pub fn aabb3 (&self) -> Aabb3 <S> {
      Aabb3::from_points (self.a, self.b)
    }
    #[inline]
    pub fn line3 (&self) -> Line3 <S> {
      Line3::new (self.a, math::Unit3::normalize (self.vector()))
    }
    #[inline]
    pub fn intersect_aabb (&self, aabb : &Aabb3 <S>)
      -> Option <((S, cgmath::Point3 <S>), (S, cgmath::Point3 <S>))>
    {
      intersect::continuous_segment3_aabb3 (self, aabb)
    }
    #[inline]
    pub fn intersect_sphere (&self, sphere : &Sphere3 <S>)
      -> Option <((S, cgmath::Point3 <S>), (S, cgmath::Point3 <S>))>
    {
      intersect::continuous_segment3_sphere3 (self, sphere)
    }
    #[inline]
    pub fn intersect_cylinder (&self, sphere : &Cylinder3 <S>)
      -> Option <((S, cgmath::Point3 <S>), (S, cgmath::Point3 <S>))>
    {
      intersect::continuous_segment3_cylinder3 (self, sphere)
    }
    #[inline]
    pub fn intersect_capsule (&self, capsule : &Capsule3 <S>)
      -> Option <((S, cgmath::Point3 <S>), (S, cgmath::Point3 <S>))>
    {
      intersect::continuous_segment3_capsule3 (self, capsule)
    }
  }
  impl <S : Scalar> Default for Segment <S> {
    /// A default simplex is arbitrarily chosen to be the simplex from -1.0 to 1.0
    /// lying on the X axis:
    ///
    /// ```
    /// # use geometry_utils::simplex3::Segment;
    /// assert_eq!(
    ///   Segment::default(),
    ///   Segment::new ([-1.0, 0.0, 0.0].into(), [1.0, 0.0, 0.0].into())
    /// );
    /// ```
    fn default() -> Self {
      Segment {
        a: [-S::one(), S::zero(), S::zero()].into(),
        b: [ S::one(), S::zero(), S::zero()].into()
      }
    }
  }

  impl <S : Scalar> Triangle <S> {
    /// Panics if the points are colinear:
    ///
    /// ```should_panic
    /// # use geometry_utils::simplex3::Triangle;
    /// let s = Triangle::new (
    ///   [-1.0, -1.0, -1.0].into(),
    ///   [ 0.0,  0.0,  0.0].into(),
    ///   [ 1.0,  1.0,  1.0].into());
    /// ```
    pub fn new (
      a : cgmath::Point3 <S>,
      b : cgmath::Point3 <S>,
      c : cgmath::Point3 <S>
    ) -> Self {
      debug_assert_ne!(a, b);
      debug_assert_ne!(a, c);
      debug_assert_ne!(b, c);
      assert!(!colinear_3d (&a, &b, &c));
      Triangle { a, b, c }
    }
    /// Debug panic if the points are colinear:
    ///
    /// ```should_panic
    /// # use geometry_utils::simplex3::Triangle;
    /// let s = Triangle::unchecked (
    ///   [-1.0, -1.0, -1.0].into(),
    ///   [ 0.0,  0.0,  0.0].into(),
    ///   [ 1.0,  1.0,  1.0].into());
    /// ```
    pub fn unchecked (
      a : cgmath::Point3 <S>,
      b : cgmath::Point3 <S>,
      c : cgmath::Point3 <S>
    ) -> Self {
      debug_assert_ne!(a, b);
      debug_assert_ne!(a, c);
      debug_assert_ne!(b, c);
      debug_assert!(!colinear_3d (&a, &b, &c));
      Triangle { a, b, c }
    }
    #[inline]
    pub fn point_a (&self) -> &cgmath::Point3 <S> {
      &self.a
    }
    #[inline]
    pub fn point_b (&self) -> &cgmath::Point3 <S> {
      &self.b
    }
    #[inline]
    pub fn point_c (&self) -> &cgmath::Point3 <S> {
      &self.c
    }
  }
  impl <S : Scalar> Default for Triangle <S> {
    /// A default simplex is arbitrarily chosen to be the simplex on the XY plane
    /// formed by an equilateral triangle with point at 1.0 on the Y axis:
    ///
    /// ```
    /// # #[macro_use] extern crate cgmath;
    /// # extern crate geometry_utils;
    /// # use geometry_utils::simplex3::Triangle;
    /// use std::f64::consts::FRAC_1_SQRT_2;
    /// use cgmath::EuclideanSpace;
    /// let s = Triangle::default();
    /// let t = Triangle::new (
    ///   [           0.0,            1.0, 0.0].into(),
    ///   [-FRAC_1_SQRT_2, -FRAC_1_SQRT_2, 0.0].into(),
    ///   [ FRAC_1_SQRT_2, -FRAC_1_SQRT_2, 0.0].into()
    /// );
    /// assert_relative_eq!(
    ///   cgmath::Matrix3::from_cols (
    ///     s.point_a().to_vec(), s.point_b().to_vec(), s.point_c().to_vec()),
    ///   cgmath::Matrix3::from_cols (
    ///     t.point_a().to_vec(), t.point_b().to_vec(), t.point_c().to_vec()));
    /// ```
    fn default() -> Self {
      let frac_1_sqrt_2 = S::one() / (S::one() + S::one()).sqrt();
      Triangle {
        a: [     S::zero(),       S::one(), S::zero()].into(),
        b: [-frac_1_sqrt_2, -frac_1_sqrt_2, S::zero()].into(),
        c: [ frac_1_sqrt_2, -frac_1_sqrt_2, S::zero()].into()
      }
    }
  }

  impl <S : Scalar> Tetrahedron <S> {
    /// Panics if the points are coplanar:
    ///
    /// ```should_panic
    /// # use geometry_utils::simplex3::Tetrahedron;
    /// let s = Tetrahedron::new (
    ///   [-1.0, -1.0, -1.0].into(),
    ///   [ 1.0,  1.0,  1.0].into(),
    ///   [-1.0,  1.0,  0.0].into(),
    ///   [ 1.0, -1.0,  0.0].into());
    /// ```
    pub fn new (
      a : cgmath::Point3 <S>,
      b : cgmath::Point3 <S>,
      c : cgmath::Point3 <S>,
      d : cgmath::Point3 <S>
    ) -> Self {
      assert!(!coplanar_3d (&a, &b, &c, &d));
      Tetrahedron { a, b, c, d }
    }
    /// Debug panic if the points are coplanar:
    ///
    /// ```should_panic
    /// # use geometry_utils::simplex3::Tetrahedron;
    /// let s = Tetrahedron::unchecked (
    ///   [-1.0, -1.0, -1.0].into(),
    ///   [ 1.0,  1.0,  1.0].into(),
    ///   [-1.0,  1.0,  0.0].into(),
    ///   [ 1.0, -1.0,  0.0].into());
    /// ```
    pub fn unchecked (
      a : cgmath::Point3 <S>,
      b : cgmath::Point3 <S>,
      c : cgmath::Point3 <S>,
      d : cgmath::Point3 <S>
    ) -> Self {
      debug_assert!(!coplanar_3d (&a, &b, &c, &d));
      Tetrahedron { a, b, c, d }
    }
    #[inline]
    pub fn point_a (&self) -> &cgmath::Point3 <S> {
      &self.a
    }
    #[inline]
    pub fn point_b (&self) -> &cgmath::Point3 <S> {
      &self.b
    }
    #[inline]
    pub fn point_c (&self) -> &cgmath::Point3 <S> {
      &self.c
    }
    #[inline]
    pub fn point_d (&self) -> &cgmath::Point3 <S> {
      &self.d
    }
  }
  impl <S : Scalar> Default for Tetrahedron <S> {
    /// A default simplex is arbitrarily chosen to be the simplex with vertices at
    /// unit distance from the origin with A at `[0.0, 0.0, 1.0]` and B at
    /// `[0.0, sqrt(8.0/9.0), -1.0/3.0]`, and points C and D at
    /// `[ sqrt(2.0/3.0), -sqrt(2.0/9.0), -1.0/3.0]` and
    /// `[-sqrt(2.0/3.0), -sqrt(2.0/9.0), -1.0/3.0]`.
    ///
    /// ```
    /// # #[macro_use] extern crate cgmath;
    /// # extern crate geometry_utils;
    /// # use geometry_utils::simplex3::Tetrahedron;
    /// use cgmath::EuclideanSpace;
    /// let s = Tetrahedron::default();
    /// let t = Tetrahedron::new (
    ///   [                0.0,                 0.0,      1.0].into(),
    ///   [                0.0,  f64::sqrt(8.0/9.0), -1.0/3.0].into(),
    ///   [ f64::sqrt(2.0/3.0), -f64::sqrt(2.0/9.0), -1.0/3.0].into(),
    ///   [-f64::sqrt(2.0/3.0), -f64::sqrt(2.0/9.0), -1.0/3.0].into());
    /// assert_relative_eq!(
    ///   cgmath::Matrix4::from_cols (
    ///     s.point_a().to_vec().extend (0.0),
    ///     s.point_b().to_vec().extend (0.0),
    ///     s.point_c().to_vec().extend (0.0),
    ///     s.point_d().to_vec().extend (0.0)),
    ///   cgmath::Matrix4::from_cols (
    ///     t.point_a().to_vec().extend (0.0),
    ///     t.point_b().to_vec().extend (0.0),
    ///     t.point_c().to_vec().extend (0.0),
    ///     t.point_d().to_vec().extend (0.0)));
    /// ```
    fn default() -> Self {
      let frac_1_3 = S::one()    / S::three();
      let sqrt_2_3 = (S::two()   / S::three()).sqrt();
      let sqrt_8_9 = (S::eight() / S::nine()).sqrt();
      let sqrt_2_9 = (S::two()   / S::nine()).sqrt();
      Tetrahedron {
        a: [S::zero(), S::zero(),  S::one()].into(),
        b: [S::zero(),  sqrt_8_9, -frac_1_3].into(),
        c: [ sqrt_2_3, -sqrt_2_9, -frac_1_3].into(),
        d: [-sqrt_2_3, -sqrt_2_9, -frac_1_3].into()
      }
    }
  }

}

