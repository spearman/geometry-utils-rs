//! Primitive intersection routines

use {cgmath, cgmath::relative_eq};
use crate::*;
use math::Scalar;

pub mod integer;

/// Discrete intersection test of 1D axis-aligned bounding boxes (intervals).
///
/// AABBs that are merely touching are not counted as intersecting:
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let a = Aabb1::from_points ([ 0.0].into(), [1.0].into());
/// let b = Aabb1::from_points ([-1.0].into(), [0.0].into());
/// assert!(!discrete_aabb1_aabb1 (&a, &b));
/// ```
#[inline]
pub fn discrete_aabb1_aabb1 <S : Scalar> (a : &Aabb1 <S>, b : &Aabb1 <S>)
  -> bool
{
  let (min_a, max_a) = (a.min(), a.max());
  let (min_b, max_b) = (b.min(), b.max());
  // intersection if intervals overlap
  max_a.x > min_b.x && min_a.x < max_b.x
}

/// Continuous intersection test of 1D axis-aligned bounding boxes (intervals).
///
/// AABBs that are merely touching return no intersection:
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let a = Aabb1::from_points ([ 0.0].into(), [1.0].into());
/// let b = Aabb1::from_points ([-1.0].into(), [0.0].into());
/// assert!(continuous_aabb1_aabb1 (&a, &b).is_none());
/// ```
#[inline]
pub fn continuous_aabb1_aabb1 <S : Scalar> (a : &Aabb1 <S>, b : &Aabb1 <S>)
  -> Option <Aabb1 <S>>
{
  if discrete_aabb1_aabb1 (a, b) {
    Some (Aabb1::with_minmax (
      [S::max (a.min().x, b.min().x)].into(),
      [S::min (a.max().x, b.max().x)].into()
    ))
  } else {
    None
  }
}

/// Discrete intersection test of 2D axis-aligned bounding boxes.
///
/// AABBs that are merely touching are not counted as intersecting:
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let a = Aabb2::from_points ([ 0.0, 0.0].into(), [1.0, 1.0].into());
/// let b = Aabb2::from_points ([-1.0, 0.0].into(), [0.0, 1.0].into());
/// assert!(!discrete_aabb2_aabb2 (&a, &b));
/// ```
#[inline]
pub fn discrete_aabb2_aabb2 <S : Scalar> (a : &Aabb2 <S>, b : &Aabb2 <S>)
  -> bool
{
  let (min_a, max_a) = (a.min(), a.max());
  let (min_b, max_b) = (b.min(), b.max());
  // intersection if overlap exists on both axes
  max_a.x > min_b.x && min_a.x < max_b.x &&
  max_a.y > min_b.y && min_a.y < max_b.y
}

/// Continuous intersection test of 2D axis-aligned bounding boxes.
///
/// AABBs that are merely touching return no intersection:
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let a = Aabb2::from_points ([ 0.0, 0.0].into(), [1.0, 1.0].into());
/// let b = Aabb2::from_points ([-1.0, 0.0].into(), [0.0, 1.0].into());
/// assert!(continuous_aabb2_aabb2 (&a, &b).is_none());
/// ```
#[inline]
pub fn continuous_aabb2_aabb2 <S : Scalar> (a : &Aabb2 <S>, b : &Aabb2 <S>)
  -> Option <Aabb2 <S>>
{
  if discrete_aabb2_aabb2 (a, b) {
    Some (Aabb2::with_minmax (
      point2_max (a.min(), b.min()),
      point2_min (a.max(), b.max())
    ))
  } else {
    None
  }
}

/// Discrete intersection test of 3D axis-aligned bounding boxes.
///
/// AABBs that are merely touching are not counted as intersecting:
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let a = Aabb3::from_points ([ 0.0, 0.0,  0.0].into(), [1.0, 1.0, 1.0].into());
/// let b = Aabb3::from_points ([-1.0, 0.0,  0.0].into(), [0.0, 1.0, 1.0].into());
/// assert!(!discrete_aabb3_aabb3 (&a, &b));
/// ```
#[inline]
pub fn discrete_aabb3_aabb3 <S : Scalar> (a : &Aabb3 <S>, b : &Aabb3 <S>)
  -> bool
{
  let (min_a, max_a) = (a.min(), a.max());
  let (min_b, max_b) = (b.min(), b.max());
  // intersection if overlap exists on all three axes
  max_a.x > min_b.x && min_a.x < max_b.x &&
  max_a.y > min_b.y && min_a.y < max_b.y &&
  max_a.z > min_b.z && min_a.z < max_b.z
}

/// Continuous intersection test of 3D axis-aligned bounding boxes.
///
/// AABBs that are merely touching return no intersection:
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let a = Aabb3::from_points ([ 0.0, 0.0,  0.0].into(), [1.0, 1.0, 1.0].into());
/// let b = Aabb3::from_points ([-1.0, 0.0,  0.0].into(), [0.0, 1.0, 1.0].into());
/// assert!(continuous_aabb3_aabb3 (&a, &b).is_none());
/// ```
#[inline]
pub fn continuous_aabb3_aabb3 <S : Scalar> (a : &Aabb3 <S>, b : &Aabb3 <S>)
  -> Option <Aabb3 <S>>
{
  if discrete_aabb3_aabb3 (a, b) {
    Some (Aabb3::with_minmax (
      point3_max (a.min(), b.min()),
      point3_min (a.max(), b.max())
    ))
  } else {
    None
  }
}

/// Compute the intersection of a 2D line with a 2D AABB (rectangle).
///
/// If the line and AABB intersect, the two intersection points are returned
/// with the scalar parameter corresponding to that point along the line.
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let aabb = Aabb2::with_minmax ([-1.0, -1.0].into(), [1.0, 1.0].into());
/// let line = Line2::new  ([0.0, 0.0].into(), math::Unit2::axis_x());
/// assert_eq!(
///   continuous_line2_aabb2 (&line, &aabb).unwrap(),
///   ( (-1.0, [-1.0, 0.0].into()),
///     ( 1.0, [ 1.0, 0.0].into())
///   )
/// );
/// ```
///
/// Returns `None` if the line and AABB are tangent:
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let aabb = Aabb2::with_minmax ([-1.0, -1.0].into(), [1.0, 1.0].into());
/// let line = Line2::new  ([0.0, 1.0].into(), math::Unit2::axis_x());
/// assert_eq!(continuous_line2_aabb2 (&line, &aabb), None);
/// ```
pub fn continuous_line2_aabb2 <S : Scalar> (
  line : &Line2 <S>, aabb : &Aabb2 <S>
) -> Option <((S, cgmath::Point2 <S>), (S, cgmath::Point2 <S>))> {
  let aabb_min = aabb.min();
  let aabb_max = aabb.max();
  if line.direction.x == S::zero() {
    // parallel x-axis
    if aabb_min.x < line.base.x && line.base.x < aabb_max.x {
      let out = if line.direction.y > S::zero() {
        let (t0, t1) = (aabb_min.y - line.base.y, aabb_max.y - line.base.y);
        ( (t0, (line.base.x, aabb_min.y).into()),
          (t1, (line.base.x, aabb_max.y).into())
        )
      } else {
        let (t0, t1) = (line.base.y - aabb_max.y, line.base.y - aabb_min.y);
        ( (t0, (line.base.x, aabb_max.y).into()),
          (t1, (line.base.x, aabb_min.y).into())
        )
      };
      Some (out)
    } else {
      None
    }
  } else if line.direction.y == S::zero() {
    // parallel y-axis
    if aabb_min.y < line.base.y && line.base.y < aabb_max.y {
      let out = if line.direction.x > S::zero() {
        let (t0, t1) = (aabb_min.x - line.base.x, aabb_max.x - line.base.x);
        ( (t0, (aabb_min.x, line.base.y).into()),
          (t1, (aabb_max.x, line.base.y).into())
        )
      } else {
        let (t0, t1) = (line.base.x - aabb_max.x, line.base.x - aabb_min.x);
        ( (t0, (aabb_max.x, line.base.y).into()),
          (t1, (aabb_min.x, line.base.y).into())
        )
      };
      Some (out)
    } else {
      None
    }
  } else {
    let dir_reciprocal = line.direction.map (|s| S::one() / s);
    let (t0_x, t1_x)   = {
      let (near_x, far_x) = if line.direction.x.is_positive() {
        (aabb_min.x, aabb_max.x)
      } else {
        (aabb_max.x, aabb_min.x)
      };
      ( (near_x - line.base.x) * dir_reciprocal.x,
        (far_x  - line.base.x) * dir_reciprocal.x
      )
    };
    let (t0_y, t1_y) = {
      let (near_y, far_y) = if line.direction.y.is_positive() {
        (aabb_min.y, aabb_max.y)
      } else {
        (aabb_max.y, aabb_min.y)
      };
      ( (near_y - line.base.y) * dir_reciprocal.y,
        (far_y  - line.base.y) * dir_reciprocal.y
      )
    };
    let interval_x = Aabb1::with_minmax ([t0_x].into(), [t1_x].into());
    let interval_y = Aabb1::with_minmax ([t0_y].into(), [t1_y].into());
    continuous_aabb1_aabb1 (&interval_x, &interval_y).map (|interval|{
      let start = line.point (interval.min().x);
      let end   = line.point (interval.max().x);
      ( (interval.min().x, start), (interval.max().x, end) )
    })
  }
}

/// Compute the intersection of a 2D line with a 2D sphere (circle).
///
/// If the line and circle intersect, the two intersection points are returned
/// with the scalar parameter corresponding to that point along the line.
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let sphere = Sphere2::unit();
/// let line   = Line2::new  ([0.0, 0.0].into(), math::Unit2::axis_x());
/// assert_eq!(
///   continuous_line2_sphere2 (&line, &sphere).unwrap(),
///   ( (-1.0, [-1.0, 0.0].into()),
///     ( 1.0, [ 1.0, 0.0].into())
///   )
/// );
/// ```
///
/// Returns `None` if the line and circle are tangent:
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let sphere = Sphere2::unit();
/// let line   = Line2::new  ([0.0, 1.0].into(), math::Unit2::axis_x());
/// assert_eq!(continuous_line2_sphere2 (&line, &sphere), None);
/// ```
pub fn continuous_line2_sphere2 <S : Scalar> (
  line : &Line2 <S>, sphere : &Sphere2 <S>
) -> Option <((S, cgmath::Point2 <S>), (S, cgmath::Point2 <S>))> {
  use cgmath::InnerSpace;
  // intersect the line with the cylinder circle in the XY plane
  let two  = S::two();
  let four = S::four();
  // defining intersection points by the equation at^2 + bt + c = 0,
  // solve for t using quadratic formula:
  //
  //  t = (-b +- sqrt (b^2 -4ac)) / 2a
  //
  // where a, b, and c are defined in terms of points p1, p2 of the line
  // segment and the sphere center p3, and sphere radius r
  let p1   = line.base;
  let _p2   = line.base + *line.direction;
  let p3   = sphere.center;
  let r    = *sphere.radius;
  let p1p2 = &line.direction;
  let p3p1 = p1 - p3;
  let a    = S::one();
  let b    = two * p1p2.dot (p3p1);
  let c    = p3p1.magnitude2() - r * r;
  // this is the portion of the quadratic equation inside the square root
  // that determines whether the intersection is none, a tangent point, or
  // a segment
  let discriminant = b * b - four * a * c;
  if discriminant <= S::zero() {
    None
  } else {
    let discriminant_sqrt = discriminant.sqrt();
    let frac_2a           = S::one() / (two * a);
    let t1                = (-b - discriminant_sqrt) * frac_2a;
    let t2                = (-b + discriminant_sqrt) * frac_2a;
    let first  = p1 + (**p1p2) * t1;
    let second = p1 + (**p1p2) * t2;
    Some (((t1, first), (t2, second)))
  }
}

/// Compute the continuous intersection of a 3D line with a 3D plane.
///
/// Returns `None` if the line and plane are parallel:
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let plane = Plane3::new ([0.0, 0.0,  0.0].into(), math::Unit3::axis_z());
/// let line  = Line3::new  ([0.0, 0.0,  0.0].into(),
///   math::Unit3::normalize ([1.0, 1.0, 0.0].into()));
/// assert_eq!(continuous_line3_plane3 (&line, &plane), None);
/// ```
pub fn continuous_line3_plane3 <S : Scalar> (
  line : &Line3 <S>, plane : &Plane3 <S>
) -> Option <(S, cgmath::Point3 <S>)> {
  use cgmath::{InnerSpace};
  let normal_dot_direction = plane.normal.dot (*line.direction);
  if relative_eq!(normal_dot_direction, S::zero()) {
    None
  } else {
    let plane_to_line = line.base - plane.base;
    let t     = -plane.normal.dot (plane_to_line) / normal_dot_direction;
    let point = line.point (t);
    Some ((t, point))
  }
}

/// Compute the intersection of a 3D line with a 3D AABB.
///
/// If the line and AABB intersect, the two intersection points are returned
/// with the scalar parameter corresponding to that point along the line.
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let aabb = Aabb3::with_minmax (
///   [-1.0, -1.0, -1.0].into(), [1.0, 1.0, 1.0].into());
/// let line = Line3::new  ([0.0, 0.0, 0.0].into(), math::Unit3::axis_x());
/// assert_eq!(
///   continuous_line3_aabb3 (&line, &aabb).unwrap(),
///   ( (-1.0, [-1.0, 0.0, 0.0].into()),
///     ( 1.0, [ 1.0, 0.0, 0.0].into())
///   )
/// );
/// ```
///
/// Returns `None` if the line and AABB are tangent:
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let aabb = Aabb3::with_minmax (
///   [-1.0, -1.0, -1.0].into(), [1.0, 1.0, 1.0].into());
/// let line = Line3::new  ([0.0, 1.0, 0.0].into(), math::Unit3::axis_x());
/// assert_eq!(continuous_line3_aabb3 (&line, &aabb), None);
/// ```
pub fn continuous_line3_aabb3 <S : Scalar> (
  line : &Line3 <S>, aabb : &Aabb3 <S>
) -> Option <((S, cgmath::Point3 <S>), (S, cgmath::Point3 <S>))> {
  let aabb_min = aabb.min();
  let aabb_max = aabb.max();
  if line.direction.x == S::zero() {
    if aabb_min.x < line.base.x && line.base.x < aabb_max.x {
      let line2 = Line2::new ([line.base.y, line.base.z].into(),
        math::Unit2::unchecked ([line.direction.y, line.direction.z].into()));
      let aabb2 = Aabb2::with_minmax (
        [aabb_min.y, aabb_min.z].into(),
        [aabb_max.y, aabb_max.z].into());
      continuous_line2_aabb2 (&line2, &aabb2).map (|((t0, p0), (t1, p1))|
        ( (t0, [line.base.x, p0.x, p0.y].into()),
          (t1, [line.base.x, p1.x, p1.y].into())
        )
      )
    } else {
      None
    }
  } else if line.direction.y == S::zero() {
    if aabb_min.y < line.base.y && line.base.y < aabb_max.y {
      let line2 = Line2::new ([line.base.x, line.base.z].into(),
        math::Unit2::unchecked ([line.direction.x, line.direction.z].into()));
      let aabb2 = Aabb2::with_minmax (
        [aabb_min.x, aabb_min.z].into(),
        [aabb_max.x, aabb_max.z].into());
      continuous_line2_aabb2 (&line2, &aabb2).map (|((t0, p0), (t1, p1))|
        ( (t0, [p0.x, line.base.y, p0.y].into()),
          (t1, [p1.x, line.base.y, p1.y].into())
        )
      )
    } else {
      None
    }
  } else if line.direction.z == S::zero() {
    if aabb_min.z < line.base.z && line.base.z < aabb_max.z {
      let line2 = Line2::new ([line.base.x, line.base.y].into(),
        math::Unit2::unchecked ([line.direction.x, line.direction.y].into()));
      let aabb2 = Aabb2::with_minmax (
        [aabb_min.x, aabb_min.y].into(),
        [aabb_max.x, aabb_max.y].into());
      continuous_line2_aabb2 (&line2, &aabb2).map (|((t0, p0), (t1, p1))|
        ( (t0, [p0.x, p0.y, line.base.z].into()),
          (t1, [p1.x, p1.y, line.base.z].into())
        )
      )
    } else {
      None
    }
  } else {
    let dir_reciprocal = line.direction.map (|s| S::one() / s);
    let (t0_x, t1_x)   = {
      let (near_x, far_x) = if line.direction.x.is_positive() {
        (aabb_min.x, aabb_max.x)
      } else {
        (aabb_max.x, aabb_min.x)
      };
      ( (near_x - line.base.x) * dir_reciprocal.x,
        (far_x  - line.base.x) * dir_reciprocal.x
      )
    };
    let (t0_y, t1_y) = {
      let (near_y, far_y) = if line.direction.y.is_positive() {
        (aabb_min.y, aabb_max.y)
      } else {
        (aabb_max.y, aabb_min.y)
      };
      ( (near_y - line.base.y) * dir_reciprocal.y,
        (far_y  - line.base.y) * dir_reciprocal.y
      )
    };
    let (t0_z, t1_z) = {
      let (near_z, far_z) = if line.direction.z.is_positive() {
        (aabb_min.z, aabb_max.z)
      } else {
        (aabb_max.z, aabb_min.z)
      };
      ( (near_z - line.base.z) * dir_reciprocal.z,
        (far_z  - line.base.z) * dir_reciprocal.z
      )
    };
    let interval_x = Aabb1::with_minmax ([t0_x].into(), [t1_x].into());
    let interval_y = Aabb1::with_minmax ([t0_y].into(), [t1_y].into());
    continuous_aabb1_aabb1 (&interval_x, &interval_y).and_then (|interval|{
      let interval_z = Aabb1::with_minmax ([t0_z].into(), [t1_z].into());
      continuous_aabb1_aabb1 (&interval, &interval_z).map (|interval|{
        let start = line.point (interval.min().x);
        let end   = line.point (interval.max().x);
        ( (interval.min().x, start), (interval.max().x, end) )
      })
    })
  }
}

/// Compute the continuous intersection of a 3D line with a 3D sphere.
///
/// If the line and sphere intersect, the two intersection points are returned
/// with the scalar parameter corresponding to that point along the line.
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let sphere = Sphere3::unit();
/// let line   = Line3::new  ([0.0, 0.0, 0.0].into(), math::Unit3::axis_x());
/// assert_eq!(
///   continuous_line3_sphere3 (&line, &sphere).unwrap(),
///   ( (-1.0, [-1.0, 0.0, 0.0].into()),
///     ( 1.0, [ 1.0, 0.0, 0.0].into())
///   )
/// );
/// ```
///
/// Returns `None` if the line and sphere are tangent:
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let sphere = Sphere3::unit();
/// let line   = Line3::new  ([0.0, 0.0, 1.0].into(), math::Unit3::axis_x());
/// assert_eq!(continuous_line3_sphere3 (&line, &sphere), None);
/// ```
pub fn continuous_line3_sphere3 <S : Scalar> (
  line : &Line3 <S>, sphere : &Sphere3 <S>
) -> Option <((S, cgmath::Point3 <S>), (S, cgmath::Point3 <S>))> {
  use cgmath::InnerSpace;
  let two  = S::two();
  let four = S::four();
  // defining intersection points by the equation at^2 + bt + c = 0,
  // solve for t using quadratic formula:
  //
  //  t = (-b +- sqrt (b^2 -4ac)) / 2a
  //
  // where a, b, and c are defined in terms of points p1, p2 of the line
  // segment and the sphere center p3, and sphere radius r
  let p1   = line.base;
  let _p2   = line.base + *line.direction;
  let p3   = sphere.center;
  let r    = *sphere.radius;
  let p1p2 = &line.direction;
  let p3p1 = p1 - p3;
  let a    = S::one();
  let b    = two * p1p2.dot (p3p1);
  let c    = p3p1.magnitude2() - r * r;
  // this is the portion of the quadratic equation inside the square root
  // that determines whether the intersection is none, a tangent point, or
  // a segment
  let discriminant = b * b - four * a * c;
  if discriminant <= S::zero() {
    None
  } else {
    let discriminant_sqrt = discriminant.sqrt();
    let frac_2a           = S::one() / (two * a);
    let t1                = (-b - discriminant_sqrt) * frac_2a;
    let t2                = (-b + discriminant_sqrt) * frac_2a;
    let first  = p1 + (**p1p2) * t1;
    let second = p1 + (**p1p2) * t2;
    Some (((t1, first), (t2, second)))
  }
}

/// Compute continuous intersection of a 2D line segment with a 2D AABB
/// (rectangle).
///
/// The points are returned in the order given by the ordering of the points
/// in the segment.
///
/// Note that a segment that is tangent to the AABB returns no intersection.
pub fn continuous_segment2_aabb2 <S : Scalar> (
  segment : &Segment2 <S>, aabb : &Aabb2 <S>
) -> Option <((S, cgmath::Point2 <S>), (S, cgmath::Point2 <S>))> {
  use cgmath::InnerSpace;
  let vector = segment.point_b() - segment.point_a();
  let length = vector.magnitude();
  let line   = Line2::new (
    *segment.point_a(), math::Unit2::unchecked (vector / length));
  if let Some (((t0, _p0), (t1, _p1))) = continuous_line2_aabb2 (&line, aabb) {
    let interval = Aabb1::with_minmax ([S::zero()].into(), [length].into());
    continuous_aabb1_aabb1 (&interval,
      &Aabb1::with_minmax ([t0].into(), [t1].into())
    ).map (|interval|
      ( (interval.min().x, line.point (interval.min().x)),
        (interval.max().x, line.point (interval.max().x))
      )
    )
  } else {
    None
  }
}

/// Compute continuous intersection of a 2D line segment with a 2D sphere
/// (circle).
///
/// The points are returned in the order given by the ordering of the points
/// in the segment.
///
/// Note that a segment that is tangent to the surface of the circle returns no
/// intersection.
pub fn continuous_segment2_sphere2 <S : Scalar> (
  segment : &Segment2 <S>, sphere : &Sphere2  <S>
) -> Option <((S, cgmath::Point2 <S>), (S, cgmath::Point2<S>))> {
  use cgmath::InnerSpace;
  let vector = segment.point_b() - segment.point_a();
  let length = vector.magnitude();
  let line   = Line2::new (
    *segment.point_a(), math::Unit2::unchecked (vector / length));
  if let Some (((t0, _p0), (t1, _p1))) = continuous_line2_sphere2 (&line, sphere) {
    let interval = Aabb1::with_minmax ([S::zero()].into(), [length].into());
    continuous_aabb1_aabb1 (&interval,
      &Aabb1::with_minmax ([t0].into(), [t1].into())
    ).map (|interval|
      ( (interval.min().x, line.point (interval.min().x)),
        (interval.max().x, line.point (interval.max().x))
      )
    )
  } else {
    None
  }
}

/// Compute continuous intersection of a 3D line segment with an AABB.
///
/// The points are returned in the order given by the ordering of the points
/// in the segment.
///
/// Note that a segment that is tangent to the AABB returns no intersection.
pub fn continuous_segment3_aabb3 <S : Scalar> (
  segment : &Segment3 <S>, aabb : &Aabb3  <S>
) -> Option <((S, cgmath::Point3 <S>), (S, cgmath::Point3<S>))> {
  use cgmath::InnerSpace;
  let vector = segment.point_b() - segment.point_a();
  let length = vector.magnitude();
  let line   = Line3::new (
    *segment.point_a(), math::Unit3::unchecked (vector / length));
  if let Some (((t0, _p0), (t1, _p1))) = continuous_line3_aabb3 (&line, aabb) {
    let interval = Aabb1::with_minmax ([S::zero()].into(), [length].into());
    continuous_aabb1_aabb1 (&interval,
      &Aabb1::with_minmax ([t0].into(), [t1].into())
    ).map (|interval|
      ( (interval.min().x, line.point (interval.min().x)),
        (interval.max().x, line.point (interval.max().x))
      )
    )
  } else {
    None
  }
}

/// Compute continuous intersection of a 3D line segment with a sphere.
///
/// The points are returned in the order given by the ordering of the points
/// in the segment.
///
/// Note that a segment that is tangent to the surface of the sphere returns no
/// intersection.
pub fn continuous_segment3_sphere3 <S : Scalar> (
  segment : &Segment3 <S>, sphere  : &Sphere3  <S>
) -> Option <((S, cgmath::Point3 <S>), (S, cgmath::Point3<S>))> {
  use cgmath::InnerSpace;
  let two  = S::two();
  let four = S::four();
  // defining intersection points by the equation at^2 + bt + c = 0,
  // solve for t using quadratic formula:
  //
  //  t = (-b +- sqrt (b^2 -4ac)) / 2a
  //
  // where a, b, and c are defined in terms of points p1, p2 of the line
  // segment and the sphere center p3, and sphere radius r
  let p1   = segment.point_a();
  let p2   = segment.point_b();
  let p3   = sphere.center;
  let r    = *sphere.radius;
  let p1p2 = p2-p1;
  let p3p1 = p1-p3;
  let a    = p1p2.magnitude2();
  let b    = two * p1p2.dot (p3p1);
  let c    = p3p1.magnitude2() - r * r;
  // this is the portion of the quadratic equation inside the square root
  // that determines whether the intersection is none, a tangent point, or
  // a segment
  let discriminant = b * b - four * a * c;
  if discriminant <= S::zero() {
    None
  } else {
    let discriminant_sqrt = discriminant.sqrt();
    let frac_2a        = S::one() / (two * a);
    let t1             = S::max (
      (-b - discriminant_sqrt) * frac_2a,
      S::zero());
    let t2             = S::min (
      (-b + discriminant_sqrt) * frac_2a,
      S::one());
    if t2 <= S::zero() || S::one() <= t1 {
      None
    } else {
      let first  = p1 + p1p2 * t1;
      let second = p1 + p1p2 * t2;
      Some (((t1, first), (t2, second)))
    }
  }
}

/// Compute continuous intersection of a 3D line segment with an axis-aligned
/// cylinder.
///
/// The points are returned in the order given by the ordering of the points
/// in the segment.
///
/// Note that a segment that is tangent to the surface of the cylinder returns
/// no intersection.
pub fn continuous_segment3_cylinder3 <S : Scalar> (
  segment  : &Segment3 <S>, cylinder : &Cylinder3 <S>
) -> Option <((S, cgmath::Point3 <S>), (S, cgmath::Point3<S>))> {
  let segment_aabb  = segment.aabb3();
  let cylinder_aabb = cylinder.aabb3();
  if !discrete_aabb3_aabb3 (&segment_aabb, &cylinder_aabb) {
    None
  } else {
    use cgmath::InnerSpace;
    let p1       = segment.point_a();
    let p2       = segment.point_b();
    let p3       = cylinder.center;
    let r        = *cylinder.radius;
    let r2       = r * r;
    let p1p2     = p2 - p1;
    let p1_xy    = cgmath::Point2::new (p1.x, p1.y);
    let p2_xy    = cgmath::Point2::new (p2.x, p2.y);
    let p3_xy    = cgmath::Point2::new (p3.x, p3.y);
    let p3_z_max = cylinder_aabb.max().z;
    let p3_z_min = cylinder_aabb.min().z;
    if p1_xy == p2_xy {   // segment is aligned vertically (Z axis)
      let d2 = (p1_xy - p3_xy).magnitude2();
      if d2 >= r2 {
        None
      } else {
        let (t1, begin_z) = if p1.z >= p3_z_max {
          ((p3_z_max - p1.z) / p1p2.z, p3_z_max)
        } else if p1.z <= p3_z_min {
          ((p3_z_min - p1.z) / p1p2.z, p3_z_min)
        } else {
          (S::zero(), p1.z)
        };
        let (t2, end_z)   = if p2.z >= p3_z_max {
          ((p3_z_max - p1.z) / p1p2.z, p3_z_max)
        } else if p2.z <= p3_z_min {
          ((p3_z_min - p1.z) / p1p2.z, p3_z_min)
        } else {
          (S::one(), p2.z)
        };
        let begin = [p1_xy.x, p1_xy.y, begin_z].into();
        let end   = [p1_xy.x, p1_xy.y, end_z  ].into();
        Some (((t1, begin), (t2, end)))
      }
    } else {    // segment is not aligned vertically
      // intersect the line with the cylinder circle in the XY plane
      let two     = S::two();
      let four    = S::four();
      let p1p2_xy = p1p2.truncate();
      let p3p1_xy = p1_xy - p3_xy;
      let a       = p1p2_xy.magnitude2();
      let b       = two * p1p2_xy.dot (p3p1_xy);
      let c       = p3p1_xy.magnitude2() - r * r;
      // this is the portion of the quadratic equation inside the square root
      // that determines whether the intersection is none, a tangent point, or
      // a segment
      let discriminant = b * b - four * a * c;
      if discriminant <= S::zero() {
        None
      } else {
        let discriminant_sqrt = discriminant.sqrt();
        let frac_2a           = S::one() / (two * a);
        let t1_xy             = S::max (
          (-b - discriminant_sqrt) * frac_2a,
          S::zero());
        let t2_xy             = S::min (
          (-b + discriminant_sqrt) * frac_2a,
          S::one());
        if t2_xy <= S::zero() || S::one() <= t1_xy {
          None
        } else {
          if let Some ((t1, t2)) = if p1.z == p2.z { // segment aligned horizontally
            Some ((t1_xy, t2_xy))
          } else {                         // segment not aligned horizontally
            // intersect the line with the top and bottom of the cylinder
            let p1p3_z_max = p3_z_max - p1.z;
            let p1p3_z_min = p3_z_min - p1.z;
            let t_z_max    = S::max (
              S::min (p1p3_z_max / p1p2.z, S::one()),
              S::zero());
            let t_z_min    = S::max (
              S::min (p1p3_z_min / p1p2.z, S::one()),
              S::zero());
            let t1_z       = S::min (t_z_max, t_z_min);
            let t2_z       = S::max (t_z_max, t_z_min);
            let aabb_xy    = Aabb1::with_minmax ([t1_xy].into(), [t2_xy].into());
            let aabb_z     = Aabb1::with_minmax ([ t1_z].into(), [ t2_z].into());
            if !aabb_xy.intersects (&aabb_z) {
              None
            } else {
              Some ((S::max (t1_xy, t1_z), S::min (t2_xy, t2_z)))
            }
          } /*then*/ {
            debug_assert!(t1 < t2);
            debug_assert!(t1 >= S::zero());
            debug_assert!(t1 <  S::one());
            debug_assert!(t2 >  S::zero());
            debug_assert!(t2 <= S::one());
            let first  = p1 + p1p2 * t1;
            let second = p1 + p1p2 * t2;
            Some (((t1, first), (t2, second)))
          } else {
            None
          }
        }
      }
    }
  }
}

/// Compute continuous intersection of a line segment with an axis-aligned
/// capsule.
///
/// The points are returned in the order given by the ordering of the points
/// in the segment.
///
/// Note that a line that is tangent to the surface of the capsule returns no
/// intersection.
pub fn continuous_segment3_capsule3 <S : Scalar> (
  segment : &Segment3 <S>, capsule : &Capsule3 <S>
) -> Option <((S, cgmath::Point3 <S>), (S, cgmath::Point3 <S>))> {
  let segment_aabb = segment.aabb3();
  let capsule_aabb = capsule.aabb3();
  if !discrete_aabb3_aabb3 (&segment_aabb, &capsule_aabb) {
    None
  } else {
    // decompose the capsule into spheres and cylinder
    let (upper_sphere, cylinder, lower_sphere) = capsule.decompose();
    let cylinder_result = cylinder.and_then (
      |cylinder| segment.intersect_cylinder (&cylinder));
    let upper_result    = segment.intersect_sphere   (&upper_sphere);
    let lower_result    = segment.intersect_sphere   (&lower_sphere);
    match (upper_result, cylinder_result, lower_result) {
      (None, None, None) => None,
      (one,  None, None) |
      (None,  one, None) |
      (None, None,  one) => one,
      (Some (((t1,p1), (t2,p2))), Some (((u1,q1), (u2,q2))), None) |
      (Some (((t1,p1), (t2,p2))), None, Some (((u1,q1), (u2,q2)))) |
      (None, Some (((t1,p1), (t2,p2))), Some (((u1,q1), (u2,q2)))) => {
        let first = if t1 < u1 {
          (t1,p1)
        } else {
          (u1,q1)
        };
        let second = if t2 > u2 {
          (t2,p2)
        } else {
          (u2,q2)
        };
        Some ((first, second))
      }
      ( Some (((t1,p1), (t2,p2))),
        Some (((u1,q1), (u2,q2))),
        Some (((v1,r1), (v2,r2)))
      ) => {
        let min1 = S::min (S::min (t1, u1), v1);
        let max2 = S::max (S::max (t2, u2), v2);
        let first = if min1 == t1 {
          (t1,p1)
        } else if min1 == u1 {
          (u1,q1)
        } else {
          debug_assert_eq!(min1, v1);
          (v1,r1)
        };
        let second = if max2 == t2 {
          (t2,p2)
        } else if max2 == u2 {
          (u2,q2)
        } else {
          debug_assert_eq!(max2, v2);
          (v2,r2)
        };
        Some ((first, second))
      }
    }
  }
}

/// Discrete intersection test of 2D spheres.
///
/// Spheres that are merely touching are not counted as intersecting:
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let r = math::Positive::one();
/// let a = Sphere2 { center: [ 1.0, 0.0].into(), radius: r };
/// let b = Sphere2 { center: [-1.0, 0.0].into(), radius: r };
/// assert!(!discrete_sphere2_sphere2 (&a, &b));
/// ```
#[inline]
pub fn discrete_sphere2_sphere2 <S : Scalar> (
  a : &Sphere2 <S>, b : &Sphere2 <S>
) -> bool {
  use cgmath::InnerSpace;
  let r_ab = *(a.radius + b.radius);
  (b.center - a.center).magnitude2() < r_ab * r_ab
}

/// Discrete intersection test of 3D spheres.
///
/// Spheres that are merely touching are not counted as intersecting:
///
/// ```
/// # use geometry_utils::{intersect::*, *};
/// let r = math::Positive::one();
/// let a = Sphere3 { center: [ 1.0, 0.0,  0.0].into(), radius: r };
/// let b = Sphere3 { center: [-1.0, 0.0,  0.0].into(), radius: r };
/// assert!(!discrete_sphere3_sphere3 (&a, &b));
/// ```
#[inline]
pub fn discrete_sphere3_sphere3 <S : Scalar> (
  a : &Sphere3 <S>, b : &Sphere3 <S>
) -> bool {
  use cgmath::InnerSpace;
  let r_ab = *(a.radius + b.radius);
  (b.center - a.center).magnitude2() < r_ab * r_ab
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_line2_aabb2() {
    use std::f64::consts::SQRT_2;
    let aabb = Aabb2::with_minmax ([-1.0, -1.0].into(), [1.0, 1.0].into());
    let line = Line2::new ([ 0.0, 0.0].into(), math::Unit2::axis_y());
    assert_eq!(
      continuous_line2_aabb2 (&line, &aabb).unwrap(),
      ((-1.0, [ 0.0, -1.0].into()), (1.0, [ 0.0, 1.0].into())));
    let line = Line2::new ([ 0.0, 0.0].into(), math::Unit2::axis_x());
    assert_eq!(
      continuous_line2_aabb2 (&line, &aabb).unwrap(),
      ((-1.0, [-1.0,  0.0].into()), (1.0, [ 1.0, 0.0].into())));
    let line = Line2::new ([ 0.0, 0.0].into(),
      math::Unit2::normalize ([1.0, 1.0].into()));
    assert_eq!(
      continuous_line2_aabb2 (&line, &aabb).unwrap(),
      ((-SQRT_2, [-1.0, -1.0].into()), (SQRT_2, [ 1.0,  1.0].into())));
    let line = Line2::new ([ 0.0, 0.0].into(),
      math::Unit2::normalize ([-1.0, -1.0].into()));
    assert_eq!(
      continuous_line2_aabb2 (&line, &aabb).unwrap(),
      ((-SQRT_2, [ 1.0,  1.0].into()), (SQRT_2, [-1.0, -1.0].into())));
    let line = Line2::new ([ 0.0, 3.0].into(),
      math::Unit2::normalize ([-1.0, -1.0].into()));
    assert!(continuous_line2_aabb2 (&line, &aabb).is_none());
    let line = Line2::new ([ 0.0, -3.0].into(),
      math::Unit2::normalize ([ 1.0,  1.0].into()));
    assert!(continuous_line2_aabb2 (&line, &aabb).is_none());
  }

  #[test]
  fn test_line3_aabb3() {
    use cgmath::assert_ulps_eq;
    use math::consts::f64::SQRT_3;
    let aabb = Aabb3::with_minmax (
      [-1.0, -1.0, -1.0].into(), [1.0, 1.0, 1.0].into());
    let line = Line3::new ([ 0.0, 0.0, 0.0].into(), math::Unit3::axis_z());
    assert_eq!(
      continuous_line3_aabb3 (&line, &aabb).unwrap(),
      ((-1.0, [ 0.0, 0.0, -1.0].into()), (1.0, [ 0.0, 0.0, 1.0].into())));
    let line = Line3::new ([ 0.0, 0.0, 0.0].into(), math::Unit3::axis_y());
    assert_eq!(
      continuous_line3_aabb3 (&line, &aabb).unwrap(),
      ((-1.0, [0.0, -1.0,  0.0].into()), (1.0, [ 0.0, 1.0, 0.0].into())));
    {
      let line = Line3::new ([ 0.0, 0.0, 0.0].into(),
        math::Unit3::normalize ([1.0, 1.0, 1.0].into()));
      let result = continuous_line3_aabb3 (&line, &aabb).unwrap();
      assert_ulps_eq!((result.0).0, -SQRT_3);
      assert_ulps_eq!((result.1).0, SQRT_3);
      assert_eq!((result.0).1, [-1.0, -1.0, -1.0].into());
      assert_eq!((result.1).1, [ 1.0,  1.0,  1.0].into());
    }
    {
      let line = Line3::new ([ 0.0, 0.0, 0.0].into(),
        math::Unit3::normalize ([-1.0, -1.0, -1.0].into()));
      let result = continuous_line3_aabb3 (&line, &aabb).unwrap();
      assert_ulps_eq!((result.0).0, -SQRT_3);
      assert_ulps_eq!((result.1).0, SQRT_3);
      assert_eq!((result.0).1, [ 1.0,  1.0,  1.0].into());
      assert_eq!((result.1).1, [-1.0, -1.0, -1.0].into());
    }
    let line = Line3::new ([ 0.0, 0.0, 3.0].into(),
      math::Unit3::normalize ([-1.0, -1.0, -1.0].into()));
    assert!(continuous_line3_aabb3 (&line, &aabb).is_none());
    let line = Line3::new ([0.0, 0.0, -3.0].into(),
      math::Unit3::normalize ([1.0, 1.0, 1.0].into()));
    assert!(continuous_line3_aabb3 (&line, &aabb).is_none());
  }

  #[test]
  fn test_segment3_sphere3() {
    use cgmath::EuclideanSpace;
    let sphere  = shape::Sphere::unit().sphere3 (cgmath::Point3::origin());
    let segment = Segment3::new (
      [-2.0, 0.0, 0.0].into(), [ 2.0, 0.0, 0.0].into());
    assert_eq!(
      continuous_segment3_sphere3 (&segment, &sphere).unwrap(),
      ((0.25, [-1.0, 0.0, 0.0].into()), (0.75, [ 1.0, 0.0, 0.0].into())));
    let segment = Segment3::new (
      [ 2.0, 0.0, 0.0].into(), [-2.0, 0.0, 0.0].into());
    assert_eq!(
      continuous_segment3_sphere3 (&segment, &sphere).unwrap(),
      ((0.25, [ 1.0, 0.0, 0.0].into()), (0.75, [-1.0, 0.0, 0.0].into())));
    let segment = Segment3::new (
      [ 0.0, 0.0, 0.0].into(), [ 0.0, 0.0, 2.0].into());
    assert_eq!(
      continuous_segment3_sphere3 (&segment, &sphere).unwrap(),
      ((0.0, [ 0.0, 0.0, 0.0].into()), (0.5, [ 0.0, 0.0, 1.0].into())));
    let segment = Segment3::new (
      [ 0.0, 0.0, -2.0].into(), [ 0.0, 0.0, 0.0].into());
    assert_eq!(
      continuous_segment3_sphere3 (&segment, &sphere).unwrap(),
      ((0.5, [ 0.0, 0.0, -1.0].into()), (1.0, [ 0.0, 0.0, 0.0].into())));
  }

  #[test]
  fn test_segment3_cylinder3() {
    use cgmath::EuclideanSpace;
    let cylinder = shape::Cylinder::unit().cylinder3 (cgmath::Point3::origin());
    let segment = Segment3::new (
      [-2.0, 0.0, 0.0].into(), [ 2.0, 0.0, 0.0].into());
    assert_eq!(
      continuous_segment3_cylinder3 (&segment, &cylinder).unwrap(),
      ((0.25, [-1.0, 0.0, 0.0].into()), (0.75, [ 1.0, 0.0, 0.0].into())));
    let segment = Segment3::new (
      [ 2.0, 0.0, 0.0].into(), [-2.0, 0.0, 0.0].into());
    assert_eq!(
      continuous_segment3_cylinder3 (&segment, &cylinder).unwrap(),
      ((0.25, [ 1.0, 0.0, 0.0].into()), (0.75, [-1.0, 0.0, 0.0].into())));
    let segment = Segment3::new (
      [ 0.0, 0.0, 0.0].into(), [ 0.0, 0.0, 2.0].into());
    assert_eq!(
      continuous_segment3_cylinder3 (&segment, &cylinder).unwrap(),
      ((0.0, [ 0.0, 0.0, 0.0].into()), (0.5, [ 0.0, 0.0, 1.0].into())));
    let segment = Segment3::new (
      [ 0.0, 0.0, -2.0].into(), [ 0.0, 0.0, 0.0].into());
    assert_eq!(
      continuous_segment3_cylinder3 (&segment, &cylinder).unwrap(),
      ((0.5, [ 0.0, 0.0, -1.0].into()), (1.0, [ 0.0, 0.0, 0.0].into())));
  }

  #[test]
  fn test_segment3_capsule3() {
    use cgmath::EuclideanSpace;
    let capsule = shape::Capsule::noisy (1.0, 1.0)
      .capsule3 (cgmath::Point3::origin());
    let segment = Segment3::new (
      [-2.0, 0.0, 0.0].into(), [ 2.0, 0.0, 0.0].into());
    assert_eq!(
      continuous_segment3_capsule3 (&segment, &capsule).unwrap(),
      ((0.25, [-1.0, 0.0, 0.0].into()), (0.75, [ 1.0, 0.0, 0.0].into())));
    let segment = Segment3::new (
      [ 2.0, 0.0, 0.0].into(), [-2.0, 0.0, 0.0].into());
    assert_eq!(
      continuous_segment3_capsule3 (&segment, &capsule).unwrap(),
      ((0.25, [ 1.0, 0.0, 0.0].into()), (0.75, [-1.0, 0.0, 0.0].into())));
    let segment = Segment3::new (
      [ 0.0, 0.0, 0.0].into(), [ 0.0, 0.0, 4.0].into());
    assert_eq!(
      continuous_segment3_capsule3 (&segment, &capsule).unwrap(),
      ((0.0, [ 0.0, 0.0, 0.0].into()), (0.5, [ 0.0, 0.0, 2.0].into())));
    let segment = Segment3::new (
      [ 0.0, 0.0, -4.0].into(), [ 0.0, 0.0, 0.0].into());
    assert_eq!(
      continuous_segment3_capsule3 (&segment, &capsule).unwrap(),
      ((0.5, [ 0.0, 0.0, -2.0].into()), (1.0, [ 0.0, 0.0, 0.0].into())));
  }

} // end tests
