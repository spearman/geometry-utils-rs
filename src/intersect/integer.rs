//! Intersection of Aabbs over Integers.
//!
//! Note that unlike Aabbs over Scalars, Aabbs over Integers are *closed* (they
//! include their boundaries).

use crate::math::Integer;
use crate::integer::*;

/// Discrete intersection test of 1D axis-aligned bounding boxes (intervals).
///
/// Shared boundary points will return intersection:
///
/// ```
/// # use geometry_utils::{intersect::integer::*, integer::*};
/// let a = Aabb1::from_points ([ 0].into(), [1].into());
/// let b = Aabb1::from_points ([-1].into(), [0].into());
/// assert!(discrete_aabb1_aabb1 (&a, &b));
/// ```
#[inline]
pub fn discrete_aabb1_aabb1 <I : Integer> (a : &Aabb1 <I>, b : &Aabb1 <I>)
  -> bool
{
  let (min_a, max_a) = (a.min(), a.max());
  let (min_b, max_b) = (b.min(), b.max());
  // intersection if intervals overlap
  max_a.x >= min_b.x && min_a.x <= max_b.x
}

/// Continuous intersection test of 1D axis-aligned bounding boxes (intervals).
///
/// Shared boundary points will return intersection:
///
/// ```
/// # use geometry_utils::{intersect::integer::*, integer::*};
/// let a = Aabb1::from_points ([ 0].into(), [1].into());
/// let b = Aabb1::from_points ([-1].into(), [0].into());
/// assert_eq!(
///   continuous_aabb1_aabb1 (&a, &b).unwrap(),
///   Aabb1::from_points ([0].into(), [0].into()));
/// ```
#[inline]
pub fn continuous_aabb1_aabb1 <I : Integer> (a : &Aabb1 <I>, b : &Aabb1 <I>)
  -> Option <Aabb1 <I>>
{
  if discrete_aabb1_aabb1 (a, b) {
    Some (Aabb1::with_minmax (
      [I::max (a.min().x, b.min().x)].into(),
      [I::min (a.max().x, b.max().x)].into()
    ))
  } else {
    None
  }
}

/// Discrete intersection test of 2D axis-aligned bounding boxes.
///
/// Shared boundary points will return intersection:
///
/// ```
/// # use geometry_utils::{intersect::integer::*, integer::*};
/// let a = Aabb2::from_points ([ 0, 0].into(), [1, 1].into());
/// let b = Aabb2::from_points ([-1, 0].into(), [0, 1].into());
/// assert!(discrete_aabb2_aabb2 (&a, &b));
/// ```
#[inline]
pub fn discrete_aabb2_aabb2 <I : Integer> (a : &Aabb2 <I>, b : &Aabb2 <I>)
  -> bool
{
  let (min_a, max_a) = (a.min(), a.max());
  let (min_b, max_b) = (b.min(), b.max());
  // intersection if overlap exists on both axes
  max_a.x >= min_b.x && min_a.x <= max_b.x &&
  max_a.y >= min_b.y && min_a.y <= max_b.y
}

/// Continuous intersection test of 2D axis-aligned bounding boxes.
///
/// Shared boundary points will return intersection:
///
/// ```
/// # use geometry_utils::{intersect::integer::*, integer::*};
/// let a = Aabb2::from_points ([ 0, 0].into(), [1, 1].into());
/// let b = Aabb2::from_points ([-1, 0].into(), [0, 1].into());
/// assert_eq!(
///   continuous_aabb2_aabb2 (&a, &b).unwrap(),
///   Aabb2::from_points ([0,0].into(), [0, 1].into()));
/// ```
#[inline]
pub fn continuous_aabb2_aabb2 <I : Integer> (a : &Aabb2 <I>, b : &Aabb2 <I>)
  -> Option <Aabb2 <I>>
{
  if discrete_aabb2_aabb2 (a, b) {
    Some (Aabb2::with_minmax (
      point2_max (a.min(), b.min()),
      point2_min (a.max(), b.max())
    ))
  } else {
    None
  }
}

/// Discrete intersection test of 3D axis-aligned bounding boxes.
///
/// Shared boundary points will return intersection:
///
/// ```
/// # use geometry_utils::{intersect::integer::*, integer::*};
/// let a = Aabb3::from_points ([ 0, 0,  0].into(), [1, 1, 1].into());
/// let b = Aabb3::from_points ([-1, 0,  0].into(), [0, 1, 1].into());
/// assert!(discrete_aabb3_aabb3 (&a, &b));
/// ```
#[inline]
pub fn discrete_aabb3_aabb3 <I : Integer> (a : &Aabb3 <I>, b : &Aabb3 <I>)
  -> bool
{
  let (min_a, max_a) = (a.min(), a.max());
  let (min_b, max_b) = (b.min(), b.max());
  // intersection if overlap exists on all three axes
  max_a.x >= min_b.x && min_a.x <= max_b.x &&
  max_a.y >= min_b.y && min_a.y <= max_b.y &&
  max_a.z >= min_b.z && min_a.z <= max_b.z
}

/// Continuous intersection test of 3D axis-aligned bounding boxes.
///
/// Shared boundary points will return intersection:
///
/// ```
/// # use geometry_utils::{intersect::integer::*, integer::*};
/// let a = Aabb3::from_points ([ 0, 0,  0].into(), [1, 1, 1].into());
/// let b = Aabb3::from_points ([-1, 0,  0].into(), [0, 1, 1].into());
/// assert_eq!(
///   continuous_aabb3_aabb3 (&a, &b).unwrap(),
///   Aabb3::from_points ([0, 0, 0].into(), [0, 1, 1].into()));
/// ```
#[inline]
pub fn continuous_aabb3_aabb3 <I : Integer> (a : &Aabb3 <I>, b : &Aabb3 <I>)
  -> Option <Aabb3 <I>>
{
  if discrete_aabb3_aabb3 (a, b) {
    Some (Aabb3::with_minmax (
      point3_max (a.min(), b.min()),
      point3_min (a.max(), b.max())
    ))
  } else {
    None
  }
}

