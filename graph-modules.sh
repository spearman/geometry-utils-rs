#!/usr/bin/env bash
set -x

cargo modules graph > modules.dot && make -f MakefileDot modules \
  && feh modules.png

exit
